// @ts-check
const
  { assign, get, noop, now, isNil } = require('lodash'),
  stubSetup = require('./stub-setup'),
  debug = require('debug')('app:db'),
  express = require('express'),
  bodyParser = require('body-parser'),
  KnexServer = require('@cern/mrest');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 */

/** @type {mrest.DatabaseDescription} */
const schema = require('./schema');
/** @type {mrest.KnexRESTSwagger.Info} */
const doc = require('./doc');

class Database extends KnexServer {
  /**
   *
   * @param {string} basePath
   * @param {mrest.KnexServer.Options} config
   */
  constructor(basePath, config) {
    super(config, schema, assign(doc, { basePath }));
  }

  router() {
    const router = express.Router();
    super.register(router);

    router.post('/comments', bodyParser.json(), this.postComment.bind(this));
    return router;
  }

  /**
   * @param {Request} req
   * @returns {Promise<string>}
   */
  async getUserId(req) {
    const login = get(req, 'user.sub', 'ntofdev');

    const ret = await this.tables['users'].search(
      // @ts-ignore fake Request
      { get: noop, query: { max: 1, filter: 'login:' + login } }, { set: noop })
    // @ts-ignore
    .then((ret) => get(ret, [ 0, 'id' ]));

    if (isNil(ret)) {
      throw `user not found in database: ${login}`;
    }
    return ret;
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  async postComment(req, res) {
    try {
      const userId = await this.getUserId(req);
      assign(req.body, { userId, date: now() });

      await this.tables['comments'].insert(req, res)
      .then((ret) => res.json(ret));
    }
    catch (ret) {
      debug('postComment failed:', ret);
      res.status(get(ret, 'status', 500))
      .send(get(ret, 'message', ret));
    }
  }

  async createStub() {
    // @ts-ignore
    return stubSetup(this.knex);
  }
}

module.exports = Database;
