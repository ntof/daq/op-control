// @ts-check

module.exports = {
  basePath: '',
  paths: {
    '/db/comments': {
      post: {
        description: "post a comment, date and userId are automatically added",
        parameters: [ {
          description: "comment to post",
          example: '{ "comments": "example", "runNumber": 920001 }',
          in: "body",
          name: "comment to post",
          required: true
        } ],
        responses: [ 200 ],
        summary: "insert a comment",
        tags: [ "comments" ]
      }
    }
  }
};
