#!/usr/bin/env node
// @ts-check

const
  debug = require('debug')('app:stub'),
  readline = require('readline'),
  fs = require('fs'),
  path = require('path'),
  knex = require('knex'),
  { omit } = require('lodash');

/**
 * @typedef {import('knex').TableBuilder} TableBuilder
 */

/**
 * @param {knex} db
 * @param {string} table
 * @param {function(TableBuilder): any} fun
 */
async function createTable(db, table, fun) {
  if (!await db.schema.hasTable(table)) {
    await db.schema.createTable(table, fun);
  }
}

/**
 * @param {knex} db
 */
async function createSchema(db) {
  debug('Building schema');
  await createTable(db, 'MATERIALS_SETUPS', function(table) {
    table.increments('MAT_SETUP_ID');
    table.string('MAT_SETUP_NAME', 255).unique();
    table.string('MAT_SETUP_DESCRIPTION', 255);
    table.integer('MAT_SETUP_EAR_NUMBER').notNullable();
    table.integer('MAT_SETUP_OPER_YEAR').notNullable();
    table.index([ 'MAT_SETUP_EAR_NUMBER', 'MAT_SETUP_OPER_YEAR' ],
      'MAT_EAR_YEAR_IDX');
  });

  await createTable(db, 'DETECTORS_SETUPS', function(table) {
    table.increments('DET_SETUP_ID');
    table.string('DET_SETUP_NAME', 255).unique();
    table.string('DET_SETUP_DESCRIPTION', 255);
    table.integer('DET_SETUP_EAR_NUMBER').notNullable();
    table.integer('DET_SETUP_OPER_YEAR').notNullable();
    table.index(
      [ 'DET_SETUP_EAR_NUMBER', 'DET_SETUP_OPER_YEAR', 'DET_SETUP_NAME' ],
      'DET_SETUP_EAR_YEAR_NAME_IDX');
  });

  await createTable(db, 'USERS', function(table) {
    table.increments('USER_ID');
    table.string('USER_LOGIN', 20).unique()
    .comment('may be null for expired users');
    table.string('USER_FIRST_NAME', 50).notNullable();
    table.string('USER_LAST_NAME', 50).notNullable();
    table.string('USER_EMAIL', 50).notNullable();
    table.integer('USER_STATUS').notNullable();
    table.integer('USER_PRIVILEGES').notNullable();

    table.index([ 'USER_LAST_NAME', 'USER_FIRST_NAME' ], 'USER_NAME_IDX');
    table.index([ 'USER_LOGIN' ], 'USER_LOGIN_IDX');
  });

  await createTable(db, 'COMMENTS', function(table) {
    table.increments('COM_ID');
    table.integer('COM_DATE').notNullable();
    table.string('COM_COMMENTS', 4000);
    table.integer('COM_RUN_NUMBER').notNullable();
    table.integer('COM_USER_ID').notNullable()
    // @ts-ignore: of course it works
    .references('USERS.USER_ID').withKeyName('COMMENTS_USER_ID_FK');

    table.index([ 'COM_RUN_NUMBER', 'COM_DATE' ], 'COM_RUN_DATE_IDX');
  });

  await createTable(db, 'MATERIALS', function(table) {
    table.increments('MAT_ID');
    table.string('MAT_TYPE', 20).notNullable();
    table.string('MAT_TITLE', 100).notNullable();
    table.string('MAT_COMPOUND', 25).notNullable();
    table.integer('MAT_MASS_NUMBER').notNullable();
    table.float('MAT_MASS_MG', 126);
    table.integer('MAT_ATOMIC_NUMBER');
    table.float('MAT_THICKNESS_UM', 126);
    table.float('MAT_DIAMETER_MM', 126);
    table.integer('MAT_STATUS').notNullable();
    table.float('MAT_AREA_DENSITY_UM_CM2', 126);
    table.string('MAT_DESCRIPTION', 4000).notNullable();
    table.index([ 'MAT_TYPE', 'MAT_COMPOUND' ], 'MAT_TYPE_COMPOUND_IDX');
  });

  await createTable(db, 'DETECTORS', function(table) {
    table.increments('DET_ID');
    table.string('DET_NAME', 50).notNullable().unique();
    table.string('DET_DESCRIPTION', 255).notNullable();
    table.string('DET_DAQNAME', 255);
    table.string('DET_TRECNUMBER', 255);
    table.index([ 'DET_DAQNAME' ], 'DET_DAQNAME_IDX');
  });

  await createTable(db, 'CONTAINERS', function(table) {
    table.increments('CONT_ID');
    table.string('CONT_NAME', 255).notNullable();
    table.string('CONT_DESCRIPTION', 255);
    table.string('CONT_TRECNUMBER', 255);
    table.index([ 'CONT_NAME' ], 'CONT_NAME_IDX');
  });

  await createTable(db, 'REL_DETECTORS_SETUPS', function(table) {
    table.integer('DET_ID').notNullable();
    table.integer('DET_SETUP_ID').notNullable();
    table.integer('DET_CONT_ID');
    table.primary([ 'DET_SETUP_ID', 'DET_ID' ]);
  });

  await createTable(db, 'REL_MATERIALS_SETUPS', function(table) {
    table.integer('MAT_ID').notNullable();
    table.integer('MAT_SETUP_ID').notNullable();
    table.integer('MAT_STATUS').notNullable();
    table.integer('MAT_POSITION');
    table.primary([ 'MAT_SETUP_ID', 'MAT_ID' ]);
  });
}

/**
 * @param {knex} db
 * @param {string} file
 */
async function insertData(db, file) {
  var trx = await db.transaction();
  const reader = readline.createInterface({
    input: fs.createReadStream(file), terminal: false
  });
  var i = 0;
  var stmt = '';

  for await (const line of reader) {
    if (line.length <= 0) {
      continue;
    }
    else if (line[line.length - 1] === ';') {
      stmt = stmt + line.slice(0, -1);
    }
    else {
      stmt += line.replace(/; $/g, ';') + '\n';
      continue;
    }
    await trx.raw(stmt);
    stmt = '';

    if ((++i % 100) === 0) {
      debug('rows inserted:', i);
      await trx.commit();
      trx = await db.transaction();
    }
  }
  debug('rows inserted:', i);
  await trx.commit();
}

/**
 * @param {knex} db
 */
async function updateData(db) {
  /* update stubs to always work properly :) */
  await db('MATERIALS_SETUPS')
  .update({ MAT_SETUP_OPER_YEAR: (new Date()).getFullYear() })
  .where('MAT_SETUP_OPER_YEAR', 2018);

  await db('DETECTORS_SETUPS')
  .update({ DET_SETUP_OPER_YEAR: (new Date()).getFullYear() })
  .where('DET_SETUP_OPER_YEAR', 2018);

  await db('COMMENTS')
  .update({ COM_RUN_NUMBER: 920001 });

  await db.raw(db('USERS').insert({
    USER_EMAIL: 'nowhere@nowhere.com', USER_FIRST_NAME: 'ntofdev',
    USER_ID: 4242, USER_LAST_NAME: 'APC', USER_LOGIN: 'ntofdev',
    USER_PRIVILEGES: 0, USER_STATUS: 1
  }).toString().replace(/^insert/, 'insert or replace'));

  // Add some containers + detectors
  await db.raw(db('CONTAINERS').insert({
    CONT_ID: 1, CONT_NAME: 'test container',
    CONT_DESCRIPTION: 'Sample container with some description\nblah\nblah',
    CONT_TRECNUMBER: 1234
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('DETECTORS').insert({
    DET_ID: 1, DET_NAME: 'test detector',
    DET_DESCRIPTION: 'Sample detector with some description\nblah\nblah',
    DET_DAQNAME: 'DAQNAME', DET_TRECNUMBER: 12345
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('REL_DETECTORS_SETUPS').insert({
    DET_ID: 1, DET_CONT_ID: 1, DET_SETUP_ID: 22
  }).toString().replace(/^insert/, 'insert or replace'));

  debug('stub data udpated');
}

/**
 * @param {knex} db
 */
async function stubSetup(db) {
  await createSchema(db);
  await insertData(db, path.join(__dirname, 'sample.sql.dump'));
  await updateData(db);
}

// @ts-ignore
if (!module.parent) {
  const config = require('../config-stub'); // eslint-disable-line global-require
  const db = knex(omit(config.db, 'definition'));

  stubSetup(db)
  .catch((err) => console.log('Error:', err))
  .finally(() => db.destroy());
}
else {
  /* export our server */
  module.exports = stubSetup;
}
