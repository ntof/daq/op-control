// @ts-check
const
  _ = require('lodash'),
  // @ts-ignore
  config = require('../../config'),
  knex = require('knex');

/* do not use node-oracledb LOB objects, dump content as a Buffer */
const oracledb = require('oracledb');
oracledb.fetchAsBuffer = [ oracledb.BLOB ];

/**
 * @typedef {{
 *  db: knex
 *  out: knex,
 *  history: { [tableName: string]: Set<string> }
 * }} Env
 */

/**
 *
 * @param {knex} k
 * @param {string} tableName
 * @param {any} row
 */
function sqlInsert(k, tableName, row) {
  row = _.omitBy(row, (value, key) => (key[0] === '_'));
  row = _.mapValues(row,
    (value) => (_.isString(value) ? value.replace(/;\r?\n/g, '; \n') : value));
  return k(tableName).insert(row)
  .toString().replace(/^insert/, 'insert or replace');
}

/**
 *
 * @param {Env} env
 * @param {string} tableName
 * @param {any[]} rows
 * @param {string} rowIdName
 */
function dumpRow(env, tableName, rows, rowIdName) {
  if (!_.get(env, [ 'history', tableName ])) {
    _.set(env, [ 'history', tableName ], new Set());
  }
  const hist = env.history[tableName];

  if (_.isArray(rows)) {
    rows = rows.filter((row) => !hist.has(row[rowIdName]));
    _.forEach(rows, (row) => {
      hist.add(row[rowIdName]);
      console.log(sqlInsert(env.out, tableName, row) + ';');
    });
  }
  else if (_.isObject(rows) && !hist.has(rows[rowIdName])) {
    hist.add(rows[rowIdName]);
    console.log(sqlInsert(env.out, tableName, rows) + ';');
  }
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpMaterialsSetups(env, ids) {
  await env.db('REL_MATERIALS_SETUPS').select('*')
  .whereIn('MAT_SETUP_ID', ids)
  .then(async (rows) => {
    for (const row of rows) {
      row._UUID = row.MAT_SETUP_ID + ':' + row.MAT_ID;

      const ret = await env.db('MATERIALS').select('*')
      .where('MAT_ID', row.MAT_ID);
      dumpRow(env, 'MATERIALS', ret, 'MAT_ID');
    }
    dumpRow(env, 'REL_MATERIALS_SETUPS', rows, '_UUID');
  });
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpDetectorsSetups(env, ids) {
  await env.db('REL_DETECTORS_SETUPS').select('*')
  .whereIn('DET_SETUP_ID', ids)
  .then(async (rows) => {
    for (const row of rows) {
      row._UUID = row.DET_SETUP_ID + ':' + row.DET_ID;

      let ret = await env.db('DETECTORS').select('*')
      .where('DET_ID', row.DET_ID);
      dumpRow(env, 'DETECTORS', ret, 'DET_ID');

      ret = await env.db('CONTAINERS').select('*')
      .where('DET_CONT_ID', row.DET_ID);
      dumpRow(env, 'CONTAINERS', ret, 'CONT_ID');
    }
    dumpRow(env, 'REL_DETECTORS_SETUPS', rows, '_UUID');
  });
}

/** @type {Env} */
const env = {
  db: knex(config.db),
  out: knex({ client: 'sqlite3', useNullAsDefault: true }),
  history: {}
};

(async () => {
  await env.db('MATERIALS_SETUPS').select('*')
  .where('MAT_SETUP_OPER_YEAR', 2017).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'MATERIALS_SETUPS', rows, 'MAT_SETUP_ID');
    await dumpMaterialsSetups(env, _.map(rows, 'MAT_SETUP_ID'));
  });

  await env.db('MATERIALS_SETUPS').select('*')
  .where('MAT_SETUP_OPER_YEAR', 2018).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'MATERIALS_SETUPS', rows, 'MAT_SETUP_ID');
    await dumpMaterialsSetups(env, _.map(rows, 'MAT_SETUP_ID'));
  });

  await env.db('DETECTORS_SETUPS').select('*')
  .where('DET_SETUP_OPER_YEAR', 2017).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'DETECTORS_SETUPS', rows, 'DET_SETUP_ID');
    await dumpDetectorsSetups(env, _.map(rows, 'DET_SETUP_ID'));
  });

  await env.db('DETECTORS_SETUPS').select('*')
  .where('DET_SETUP_OPER_YEAR', 2018).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'DETECTORS_SETUPS', rows, 'DET_SETUP_ID');
    await dumpDetectorsSetups(env, _.map(rows, 'DET_SETUP_ID'));
  });

  await env.db('COMMENTS').select('*')
  .where('COM_ID', '>=', 3355)
  .where('COM_ID', '<=', 3465).limit(50)
  .then(async (comments) => {
    const users = _.uniq(_.map(comments, 'COM_USER_ID'));
    await env.db('USERS').select('*').whereIn('USER_ID', users)
    .then((rows) => dumpRow(env, 'USERS', rows, 'USER_ID'));

    dumpRow(env, 'COMMENTS', comments, 'COM_ID');
  });
})()
.finally(() => {
  env.db.destroy();
});
