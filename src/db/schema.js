// @ts-check
/*::
import type { mrest$config, mrest$dbInfo } from '@cern/mrest'

(module.exports: mrest$dbInfo)
*/

module.exports = {
  materials: {
    table: "MATERIALS",
    key: "MAT_ID",
    mapping: {
      MAT_ID: "id",
      MAT_TYPE: "type",
      MAT_TITLE: "title",
      MAT_COMPOUND: "compound",
      MAT_MASS_NUMBER: "massNumber",
      MAT_MASS_MG: "mass",
      MAT_ATOMIC_NUMBER: "atomicNumber",
      MAT_THICKNESS_UM: "thickness",
      MAT_DIAMETER_MM: "diameter",
      MAT_STATUS: "status",
      MAT_AREA_DENSITY_UM_CM2: "areaDensity",
      MAT_DESCRIPTION: "description"
    },
    strict: true
  },
  materialsSetups: {
    table: "MATERIALS_SETUPS",
    key: "MAT_SETUP_ID",
    mapping: {
      MAT_SETUP_ID: "id",
      MAT_SETUP_NAME: "name",
      MAT_SETUP_DESCRIPTION: "description",
      MAT_SETUP_EAR_NUMBER: "earNumber",
      MAT_SETUP_OPER_YEAR: "operYear"
    },
    strict: true,
    related: {
      materials: {
        path: "materials",
        join: {
          table: "REL_MATERIALS_SETUPS",
          key: "MAT_SETUP_ID",
          relKey: "MAT_ID"
        }
      },
      materialsSetupInfo: {
        path: "materialsSetupInfo",
        key: "materialSetupId"
      }
    }
  },
  materialsSetupInfo: {
    table: "REL_MATERIALS_SETUPS",
    mapping: {
      MAT_ID: "materialId",
      MAT_SETUP_ID: "materialSetupId",
      MAT_POSITION: "position",
      MAT_STATUS: "status"
    },
    strict: true
  },
  detectorsSetups: {
    table: "DETECTORS_SETUPS",
    key: "DET_SETUP_ID",
    mapping: {
      DET_SETUP_ID: "id",
      DET_SETUP_NAME: "name",
      DET_SETUP_DESCRIPTION: "description",
      DET_SETUP_EAR_NUMBER: "earNumber",
      DET_SETUP_OPER_YEAR: "operYear"
    },
    strict: true,
    related: {
      detectorsSetupInfo: {
        path: "detectorsSetupInfo",
        key: 'detectorSetupId'
      }
    }
  },
  detectorsSetupInfo: {
    table: "REL_DETECTORS_SETUPS",
    mapping: {
      DET_ID: "detectorId",
      DET_SETUP_ID: "detectorSetupId",
      DET_CONT_ID: "containerId"
    },
    strict: true
  },
  comments: {
    table: "COMMENTS",
    key: "COM_ID",
    mapping: {
      COM_ID: "id",
      COM_DATE: "date",
      COM_COMMENTS: "comments",
      COM_RUN_NUMBER: "runNumber",
      COM_USER_ID: "userId"
    },
    strict: true
  },
  users: {
    table: "USERS",
    key: "USER_ID",
    mapping: {
      USER_ID: "id",
      USER_LOGIN: "login",
      USER_FIRST_NAME: "firstName",
      USER_LAST_NAME: "lastName",
      USER_EMAIL: "email"
    },
    strict: true,
    related: {
      comments: {
        path: "comments",
        key: "userId"
      }
    }
  },
  containers: {
    table: "CONTAINERS",
    key: "CONT_ID",
    mapping: {
      CONT_ID: "id",
      CONT_NAME: "name",
      CONT_DESCRIPTION: "description",
      CONT_TRECNUMBER: "trecNumber"
    },
    strict: true
  },
  detectors: {
    table: "DETECTORS",
    key: "DET_ID",
    mapping: {
      DET_ID: "id",
      DET_NAME: "name",
      DET_DESCRIPTION: "description",
      DET_DAQNAME: "daqName",
      DET_TRECNUMBER: "trecNumber"
    },
    strict: true
  }
};
