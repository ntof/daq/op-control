// @ts-check
import { keyBy } from 'lodash';

/*::
export type Consts$Area = {
  name: string, prefix: number, dns: string
}
*/

/* this one is ordered on purpose */
export const AreaList /*: Array<Consts$Area> */ = [
  { name: 'EAR1', prefix: 1, dns: 'ntofproxy-1.cern.ch' },
  { name: 'EAR2', prefix: 2, dns: 'ntofproxy-2.cern.ch' },
  { name: 'EAR3', prefix: 3, dns: 'ntofdaq-m5.cern.ch' },
  { name: 'LAB', prefix: 9, dns: 'ntofdaq-m12.cern.ch' }
];

export const AreaListDnsMap = {
  'ntofproxy-1.cern.ch': 'EAR1',
  'ntofproxy-2.cern.ch': 'EAR2',
  'ntofdaq-m5.cern.ch': 'EAR3',
  'ntofdaq-m12.cern.ch': 'LAB'
};

export const CardLimits = {
  A240: {
    name: 'DC240',
    ImpedanceLimits: { values: [ 50, 1E6 ] },
    SampleRateLimits: {
      values: [ 1, 10, 50, 100, 200, 250, 400, 500, 1000, 2000 ] },
    FullScaleLimits: { values: [ 50, 100, 200, 500, 1000, 2000, 5000 ] },
    SampleSizeLimits: { max: 16000, min: 1 },
    /**
     * @param {number} fullScale
     */
    OffsetLimit: (fullScale) => ((fullScale >= 500) ? 20000 : 2000)
  },
  A270: {
    name: 'DC270',
    ImpedanceLimits: { values: [ 50, 1E6 ] },
    SampleRateLimits: { values: [ 10, 50, 100, 200, 250, 400, 500, 1000 ] },
    FullScaleLimits: { values: [ 50, 100, 200, 500, 1000, 2000, 5000 ] },
    SampleSizeLimits: { max: 8000, min: 1 },
    /**
     * @param {number} fullScale
     */
    OffsetLimit: (fullScale) => ((fullScale >= 500) ? 20000 : 2000)
  },
  A282: {
    name: 'DC282',
    ImpedanceLimits: { values: [ 50 ] },
    SampleRateLimits: {
      values: [ 10, 50, 100, 200, 250, 400, 500, 1000, 2000 ] },
    FullScaleLimits: { values: [ 50, 100, 200, 500, 1000, 2000, 5000 ] },
    SampleSizeLimits: { max: 32000, min: 1 },
    /**
     * @param {number} fullScale
     */
    OffsetLimit: (fullScale) => ((fullScale >= 1000) ? 5000 : 2000)
  },
  S014: {
    name: 'ADQ14',
    ImpedanceLimits: { values: [ 50 ] },
    SampleRateLimits: {
      values: [ 14.0625, 28.125, 56.25, 112.5, 125, 250, 500, 1000 ] },
    FullScaleLimits: { values: [ 50, 100, 200, 500, 1000, 2000, 5000 ] },
    SampleSizeLimits: { max: 256E3, min: 1 },
    OffsetLimit: 2500
  },
  S412: {
    name: 'ADQ412',
    ImpedanceLimits: { values: [ 50 ] },
    SampleRateLimits: {
      values: [ 14.0625, 28.125, 56.25, 112.5, 225, 450, 900, 1800 ] },
    FullScaleLimits: { values: [ 100, 200, 500, 1000, 2000, 5000 ] },
    SampleSizeLimits: { max: 175000, min: 1 },
    OffsetLimit: 2500
  }
};

export const CardInfo = {
  A240: { bits: 8 },
  A270: { bits: 8 },
  A282: { bits: 16 },
  S014: { bits: 16 },
  S412: { bits: 12 }
};

/* for convenience access to are metadata */
export const Area /*: { [string]: Consts$Area } */ = keyBy(AreaList, 'name');

/*::
var VERSION: string = ''
*/

/** @type {string} */
// @ts-ignore
const VER = (typeof VERSION === 'undefined') ? 'unknown' : VERSION; // jshint ignore:line
export { VER as VERSION };
