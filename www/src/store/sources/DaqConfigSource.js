// @ts-check
import { forEach, transform } from 'lodash';
import DIMSource from './DIMSource';
import DaqZSPSource from './DaqZSPSource';
import DaqChannelSource from './DaqChannelSource';
import { DaqConfigStore } from '../modules/daq';

/**
 * @typedef {import('../../interfaces/types').EACS.DaqListInfo} DaqListInfo
 * @typedef {import('../../interfaces/types').EACS.DaqListCardInfo} DaqListCardInfo
 * @typedef {import('../../interfaces/types').DAQ.DaqSourceConfig} DaqSourceConfig
 */

export default class DaqConfigSource extends DIMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  /**
   * @param {boolean} isEACS
   * @param {DaqListInfo} daqInfo
   * @param {V.Store<AppStore.State>} store
   */
  constructor(isEACS, daqInfo, store) {
    super(store);
    /** @type Array<string> */
    this.services = [];
    this.config = /** @type DaqSourceConfig */({
      path: (isEACS ? 'eacs/daq/' : 'daq/') + daqInfo.name,
      service: (isEACS ? 'EACS/Daq/' : '') + daqInfo.name,
      daqInfo
    });
    if (!store.hasModule(this.config.path.split('/'))) {
      store.registerModule(this.config.path.split('/'), DaqConfigStore);
    }

    this.zspSource = new DaqZSPSource(this.config, store);
    // this.zspSource.unwatch();
    this.chanSources = /** @type {{[name: string]: DaqChannelSource}} */(transform(daqInfo.cards,
      /**
       * @param {{[key: string]: DaqChannelSource}} ret
       * @param {DaqListCardInfo} card
       */
      (ret, card) => {
        for (let i = 0; i < card.channelsCount; ++i) {
          const source = new DaqChannelSource(this.config, card, i, store);
          // source.unwatch();
          ret[source.id] = source;
        }
      }, /** [key: string]: DaqChannelSource */({})));
    this.connect();
  }

  connect() {
    this.zspSource.connect();
    // @ts-ignore
    forEach(this.chanSources, (s) => s.connect());
  }

  onDnsUpdate() {
    this.close();
    this.store.commit(this.config.path + '/clear');
    this.store.commit(this.config.path + '/zsp/clear');
    this.connect();
  }

  close(destroy = false) {
    super.close(destroy);
    this.zspSource.close(destroy);
    // @ts-ignore
    forEach(this.chanSources, (s) => s.close(destroy));
  }
}
