// @ts-check

import { bindAll, forEach, get, transform } from 'lodash';
import { DicXmlDataSet } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';
import { DaqListDaqMap } from '../../interfaces/eacs';
import { parseDataSet } from '../../interfaces';
import DIMSource from './DIMSource';

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index';
*/

/**
 * @typedef {import('../../interfaces/types').EACS.DaqListInfo} DaqListInfo
 * @typedef {import('../../interfaces/types').EACS.DaqListCardInfo} DaqListCardInfo
 */

/**
 * @brief this source is not always active, thus separated from EACSSource
 */
export default class EACSDaqListSource extends DIMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    super(store);
    this.services = [ 'daqList' ];

    bindAll(this, [ 'onError', 'onDaqList' ]);
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    this.close();

    if (dns) {
      this.daqList = new DicXmlDataSet('EACS/Daq/List',
        { proxy: currentUrl() }, dnsUrl(dns));
      this.daqList.on('error', this.onError);
      this.daqList.on('value', this.onDaqList);
      this.daqList.promise().catch(this.onError);
    }
    else {
      this.onDaqList(null);
    }
  }

  /**
   * @param {any} data
   */
  onDaqList(data /*: any */) {
    const daqs = /** @type {Object<string, DaqListInfo>} */(transform(data, (ret, daqData) => {
      const daq =  /** @type DaqListInfo */(parseDataSet(daqData.value, DaqListDaqMap));
      daq.cards = get(daq.cards, 'card', []);
      // @ts-ignore
      ret[daq.name] = daq;
    }, {}));

    const cardList = /** @type {Object<string, DaqListCardInfo>} */({});
    forEach(daqs, (daqInfo) => {
      forEach(daqInfo.cards, (c) => {
        c.daq = daqInfo.name;
        cardList[c.serialNumber] = c;
      });
    });

    this.store.commit('eacs/daqList', daqs);
    this.store.commit('eacs/cardList', cardList);
  }
}
