// @ts-check
import { bindAll, get, isNull } from 'lodash';
import { currentUrl, dnsUrl } from '../../utilities';
import WorkerSource from './WorkerSource';
import d from 'debug';

const debug = d('source:hv');

// @ts-ignore
import HighVoltageWorker from '../../workers/HighVoltage.shared-worker';

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index'
*/

export default class HighVoltageSource extends WorkerSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    super(store);
    bindAll(this, [ 'onError', 'onMessage', 'setFilters' ]);
    // @ts-ignore hv is a module of state
    this.unwatchFilters = this.store.watch((state) => state.hv.filters, this.setFilters);
  }

  destroy() {
    super.destroy();
    this.unwatchFilters();
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    this.close();
    if (dns) {
      this.worker = new HighVoltageWorker();
      this.worker.port.start();
      this.worker.port.onmessage = this.onMessage;

      this.setFilters();

      super.postMessage({
        options: { proxy: currentUrl(), dns: dnsUrl(dns) }
      });

    }
    else {
      this.onMessage(null);
    }
  }

  setFilters() {
    if (!this.worker) { return; }
    const filters = get(this.store, [ 'state', 'hv', 'filters' ]);
    debug('set filters:', filters);
    super.postMessage(filters);
  }

  beforePostMessage() {
    this.store.commit('hv/loading', true);
  }

  /**
   * @param {any} msg
   */
  onMessage(msg /*: any */) {
    if (isNull(msg)) {
      this.store.commit('hv/update', { hvs: null, stats: null, boardInfo: null });
      this.store.commit('hv/loading', false);
      return;
    }
    const data = get(msg, 'data');
    if (data.data) {
      this.store.commit('hv/update', { hvs: data.data, stats: data.stats, boardInfo: data.boardInfo });
      this.store.commit('hv/loading', false);
    }
    else if (data.error) {
      this.store.commit('hv/loading', true);
      this.onError(data.error);
    }
  }
}
