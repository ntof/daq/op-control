// @ts-check
import { get } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';

/**
 * @typedef {{
  *   name?: string,
  *   message?: string
  * } | string } ErrWarn
  */

/**
 * @brief helper abstract class for DIM based sources
 */
export default class WorkerSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    /** @type V.Store<AppStore.State> */
    this.store = store;
    /** @type function */
    this.unwatch = this.store.watch((state) => state.dns,
      (dns) => this.onDnsUpdate(dns));
    /** @type {SharedWorker|null} */
    this.worker = null;
  }

  connect() {
    this.onDnsUpdate(get(this.store, [ 'state', 'dns' ]));
  }

  destroy() {
    this.close();
    this.unwatch();
  }

  /**
   * @param {?string} dns
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onDnsUpdate(dns /*: ?string */) { /* jshint unused:false */
    this.close();
  }

  close() {
    if (this.worker) {
      this.worker.port.postMessage({ destroy: true });
      this.worker.port.close();
    }
    // @ts-ignore
    this.worker = null;
  }

  /**
   * @param {ErrWarn} err
   */
  onError(err /*: any */) {
    logger.error(err);
  }

  beforePostMessage() {
    // override this if you want to do something before calling postMessage
  }

  /**
   * @param {any} msg
   */
  postMessage(msg /*: {} */) {
    if (this.worker) {
      this.beforePostMessage();
      this.worker.port.postMessage(msg);
    }
  }
}
