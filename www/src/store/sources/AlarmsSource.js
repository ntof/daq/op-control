// @ts-check
import { bindAll, get, isNil, isNull, now } from 'lodash';
import { currentUrl, dnsUrl } from '../../utilities';
import WorkerSource from './WorkerSource';

// @ts-ignore
import AlarmServerWorker from '../../workers/AlarmServer.shared-worker';

const CONNECTION_DELAY_MS = 30 * 1000;

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index'
*/

export default class AlarmsSource extends WorkerSource {
  /*::
  store: Store<any>
  unwatchFilters: () => void
  timer: ?TimeoutID
  */
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    super(store);

    bindAll(this, [ 'onError', 'onMessage', 'setFilters' ]);
    // @ts-ignore alarms is a module of state
    this.unwatchFilters = this.store.watch((state) => state.alarms.filters, this.setFilters);

    // Delayed connection
    this.timer = setTimeout(() => {
      // @ts-ignore
      delete this.timer;
      this.store.commit('alarms/fetchStamp', undefined);
      this.connect();
    }, CONNECTION_DELAY_MS);
    this.store.commit('alarms/fetchStamp', now() + CONNECTION_DELAY_MS);
  }

  connect() {
    if (!isNil(this.timer)) {
      clearTimeout(this.timer);
      this.store.commit('alarms/fetchStamp', undefined);
      // @ts-ignore
      delete this.timer;
    }
    super.connect();
  }

  destroy() {
    super.destroy();
    this.unwatchFilters();
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    // Do the connection only if timer is not active, otherwise
    // the timer will call it later
    if (!isNil(this.timer)) { return; }
    this.close();
    if (dns) {
      this.worker = new AlarmServerWorker();
      this.worker.port.start();
      this.worker.port.onmessage = this.onMessage;

      this.setFilters();

      super.postMessage({
        service: 'Alarms',
        options: { proxy: currentUrl(), dns: dnsUrl(dns) }
      });

    }
    else {
      this.onMessage(null);
    }
  }

  setFilters() {
    if (!this.worker) { return; }
    const filters = get(this.store, [ 'state', 'alarms', 'filters' ]);
    if (filters) {
      super.postMessage(filters);
    }
  }

  beforePostMessage() {
    this.store.commit('alarms/loading', true);
  }

  /**
   * @param {?any} msg
   */
  onMessage(msg /*: any */) {
    if (isNull(msg)) {
      this.store.commit('alarms/update', { alarms: null, stats: null });
      this.store.commit('alarms/loading', false);
      return;
    }
    const data = get(msg, 'data');
    if (data.data) {
      this.store.commit('alarms/update', { alarms: data.data, stats: data.stats });
      this.store.commit('alarms/loading', false);
    }
    else if (data.error) {
      this.store.commit('alarms/loading', true);
      this.onError(data.error);
    }
  }
}
