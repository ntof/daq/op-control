// @ts-check
import { filter, get, isEmpty, isNil, keyBy, toString } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import axios from 'axios';
import { currentUrl } from '../../utilities';
import { AreaList } from '../../Consts';
import DBCommentsSource from './DBCommentsSource';
import DBMaterialsSource from './DBMaterialsSource';

/**
 * @brief class for DB interactions
 */
export default class DBSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    this.store = store;
    this.unwatch = this.store.watch((state) => state.dns,
      () => { this.fetchAll(); });
    this.areaMap = keyBy(AreaList, 'dns');
    this.operYear = (new Date()).getFullYear();
    this.commentsSource = new DBCommentsSource(this.store);
    this.materialsSource = new DBMaterialsSource(this.store);
  }

  async fetchAll() {
    const dns = get(this.store, 'state.dns');
    const area = get(this.areaMap, dns);

    this.earNumber = area ? area.prefix : null;
    this.store.commit('db/setups/materials', []);
    this.store.commit('db/setups/detectors', []);

    this.fetchMaterials();
    this.fetchDetectors();
  }

  async fetchMaterials() {
    var filter = 'operYear:' + toString(this.operYear);
    if (!isNil(this.earNumber)) {
      filter = filter + ',earNumber:' + toString(this.earNumber);
    }

    await axios.get(currentUrl() + '/db/materialsSetups',
      { params: { filter, sort: 'id:desc' } })
    .then(
      (ret) => this.store.commit('db/setups/materials', ret.data),
      (err) => this.onError(err));
  }

  async fetchDetectors() {
    var filter = 'operYear:' + toString(this.operYear);
    if (!isNil(this.earNumber)) {
      filter = filter + ',earNumber:' + toString(this.earNumber);
    }

    await axios.get(currentUrl() + '/db/detectorsSetups',
      { params: { filter, sort: 'id:desc' } })
    .then(
      (ret) => this.store.commit('db/setups/detectors', ret.data),
      (err) => this.onError(err));
  }

  /**
   * @param {Array<number>} users
   * @returns {Promise<void>}
   */
  async fetchUsers(users) {
    users = filter(users,
      (u) => (get(this.store, [ 'state', 'db', 'users', u ], null) === null));

    if (isEmpty(users)) { return; }
    await axios.get(currentUrl() + '/db/users',
      { params: { query: '{"id":{"$in":' + JSON.stringify(users) + '}}' } })
    .then(
      (ret) => this.store.commit('db/usersAdd', ret.data),
      (err) => this.onError(err));
  }

  destroy() {
    this.unwatch();
    this.commentsSource.destroy();
    this.materialsSource.destroy();
  }

  /**
   * @param {any} err
   */
  onError(err /*: any */) {
    logger.error(err);
  }
}
