// @ts-check
import { bindAll, isEmpty } from 'lodash';
import { DicXmlParams } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';
import DIMSource from './DIMSource';
import { TimingParamsMap } from '../../interfaces/timing';
import { parseDataSet } from '../../interfaces';

import d from 'debug';

const debug = d('source:eacs:timing');

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index'
*/

export default class EACSTimingSource extends DIMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    super(store);
    this.services = [ 'client' ];

    bindAll(this, [ 'onError', 'onData' ]);
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    this.close();
    if (dns) {
      this.client = new DicXmlParams('EACS/Timing',
        { proxy: currentUrl() }, dnsUrl(dns));
      this.client.on('error', this.onError);
      this.client.on('value', this.onData);
      this.client.promise().catch(this.onError);
    }
    else {
      this.onData(null);
    }
  }

  /**
   * @param {any} data
   */
  onData(data /*: any */) {
    debug('timing config fetched');
    this.store.commit('eacs/timing/update', parseDataSet(data, TimingParamsMap));
  }

  async send() {
    const changes = this.store.getters['eacs/timing/getDataSetChanges'];

    if (!isEmpty(changes)) {
      debug('sending: %O', changes);
      // @ts-ignore
      return this.client.setParams(changes);
    }
  }
}
