// @ts-check

import { bindAll, find, isNil } from 'lodash';
import { DicXmlDataSet } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';
import DIMSource from './DIMSource';

/**
 * @typedef {import('../../interfaces/types').EACS.Info} Info
 */

/**
 * @brief this source is not always active, thus separated from EACSSource
 */
export default class EACSInfoSource extends DIMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    super(store);
    this.services = [ 'eacsInfo' ];

    bindAll(this, [ 'onError', 'onEacsInfo' ]);
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    this.close();
    this.onEacsInfo(null);
    if (dns) {
      this.eacsInfo = new DicXmlDataSet('EACS/Info',
        { proxy: currentUrl() }, dnsUrl(dns));
      this.eacsInfo.on('error', this.onError);
      this.eacsInfo.on('value', this.onEacsInfo);
      this.eacsInfo.promise().catch(this.onError);
    }
  }

  /**
   * @param {any} data
   */
  onEacsInfo(data /*: any */) {
    if (isNil(data)) {
      this.store.commit('eacs/info', null);
    }
    else {
      const info = /** @type Info */({});
      const filterStation = find(data, { name: 'filterStation' });
      if (filterStation) {
        info.filterStation = filterStation.value;
      }
      // TODO get also HV info and avoid call it again in HVWorker
      this.store.commit('eacs/info', info);
    }
  }
}
