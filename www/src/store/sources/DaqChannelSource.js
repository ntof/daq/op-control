// @ts-check
import { bindAll } from 'lodash';
import { DicXmlParams } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';
import DIMSource from './DIMSource';
import { ChannelParamsMap } from '../../interfaces/daq';
import { parseDataSet } from '../../interfaces';
import d from 'debug';

const debug = d('source:daq:channel');

/**
 * @typedef {import('../../interfaces/types').DAQ.DaqSourceConfig} DaqSourceConfig
 * @typedef {(import('../../interfaces/types').DAQ.DaqConfig.Channel)} Channel
 * @typedef {import('../../interfaces/types').EACS.DaqListCardInfo} DaqListCardInfo
 */

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index'
import { type DaqConfigSource$Config } from './DaqConfigSource'

import { type Eacs$DaqListCardInfo } from '../modules/eacs';
*/
export default class DaqChannelSource extends DIMSource {
  /*::
  client: typeof DicXmlParams
  id: string
  config: DaqConfigSource$Config
  card: Eacs$DaqListCardInfo
  channel: number
  store: Store<any>
  channels: Array<string>
  */
  /**
   * @param {DaqSourceConfig} config
   * @param {DaqListCardInfo} card
   * @param {number} channel
   * @param {V.Store<AppStore.State>} store
   */
  constructor(config, card, channel, store) {
    super(store);
    this.config = config;
    this.card = card;
    this.channel = channel;
    this.id = card.serialNumber + ':' + channel;
    this.services = [ 'client' ];

    bindAll(this, [ 'onError', 'onData' ]);
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    this.close();
    if (dns) {
      this.client = new DicXmlParams(
        this.config.service + '/CARD' + this.card.index + '/CHANNEL' +  this.channel,
        { proxy: currentUrl() }, dnsUrl(dns));
      this.client.on('error', this.onError);
      this.client.on('value', this.onData);
      this.client.promise().catch(this.onError);
    }
    else {
      this.onData(null);
    }
  }

  /**
   * @param {any} data
   */
  onData(data /*: any */) {
    debug('channel config fetched: ', this.id);
    const info = /** @type Channel */(parseDataSet(data, ChannelParamsMap));
    info.index = this.channel;
    info.serialNumber = this.card.serialNumber;
    info.id = this.id;
    this.store.commit(this.config.path + '/updateChannel', info);
  }

  async send() {
    return this.store.dispatch(this.config.path + '/send', this);
  }
}
