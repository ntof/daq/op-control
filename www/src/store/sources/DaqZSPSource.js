// @ts-check
import { bindAll, cloneDeep, forEach, get, isEmpty, transform } from 'lodash';
import { DicXmlParams } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';
import DIMSource from './DIMSource';
import { ZSPMap } from '../../interfaces/daq';
import { parseDataSet } from '../../interfaces';
import d from 'debug';

const debug = d('source:daq:zsp');

/**
 * @typedef {{sn: string, channel: number}} Channel
 * @typedef {import('../../interfaces/types').DAQ.DaqSourceConfig} DaqSourceConfig
 */

/**
 * @template T
 * @param {T} chan
 * @returns {T}
 */
function genId(chan) {
  // @ts-ignore
  chan.id = chan.sn + ':' + chan.channel;
  return chan;
}

/**
 * @param {{sn: string, channel: number}} chan
 * @returns {string}
 */
function getId(chan) {
  return chan.sn + ':' + chan.channel;
}

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index'
import { type DaqConfigSource$Config } from './DaqConfigSource'
*/
export default class DaqZSPSource extends DIMSource {
  /*::
  client: typeof DicXmlParams
  config: DaqConfigSource$Config
  store: Store<any>
  channels: Array<string>
  */
  /**
   * @param {DaqSourceConfig} config
   * @param {V.Store<AppStore.State>} store
   */
  constructor(config /*: DaqConfigSource$Config */, store /*: App$Store */) {
    super(store);
    this.config = config;
    this.services = [ 'client' ];

    this.channels = transform(config.daqInfo.cards,
      (/** @type {{ [key: string]: any }} */ ret, c) => {
        for (var i = 0; i < c.channelsCount; ++i) {
          ret[c.serialNumber + ':' + i] = genId({ sn: c.serialNumber, channel: i });
        }
      }, /** @type {{ [key: string]: any }} */ {});
    bindAll(this, [ 'onError', 'onData' ]);
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    this.close();
    this.store.commit(this.config.path + '/zsp/clear');
    if (dns) {
      this.client = new DicXmlParams(this.config.service + '/ZeroSuppression',
        { proxy: currentUrl() }, dnsUrl(dns));
      this.client.on('error', this.onError);
      this.client.on('value', this.onData);
      this.client.promise().catch(this.onError);
    }
    else {
      this.onData(null);
    }
  }

  /**
   * @param {any} data
   */
  onData(data /*: any */) {
    const info = parseDataSet(data, ZSPMap);
    const channels = cloneDeep(this.channels);
    forEach(get(info, 'configuration.master'), (masterData) => {
      const master = get(channels, getId(masterData));
      if (!master) {
        debug('unknown master: %o', masterData);
        return;
      }
      master.slave = [];
      forEach(masterData.slave, (slave) => master.slave.push(getId(slave)));
    });

    this.store.commit(this.config.path + '/zsp/update', { mode: get(info, 'mode'), channels });
  }

  async send() {
    const changes = this.store.getters[this.config.path + '/zsp/getDataSetChanges'];

    if (!isEmpty(changes)) {
      debug('sending: %O', changes);
      // @ts-ignore is not undefined
      return this.client.setParams(changes);
    }
  }
}
