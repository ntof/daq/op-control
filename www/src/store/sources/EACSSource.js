// @ts-check
import d from 'debug';
import { bindAll, get, keyBy, transform } from 'lodash';
import { DicXmlParams, DicXmlState } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';
import { RunConfigMap, RunInfoMap } from '../../interfaces/eacs';
import DIMSource from './DIMSource';

const debug = d('app:store:eacs');

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index';
*/

/**
 * @typedef {import('../../interfaces/types').EACS.RunInfo} RunInfo
 * @typedef {import('../../interfaces/types').EACS.RunConfig} RunConfig
 * @typedef {import('../../interfaces/types').XML.ParamDesc} ParamDesc
 */

export default class EACSSource extends DIMSource {
  /*::
  runInfo: typeof DicXmlParams
  runConfig: typeof DicXmlParams
  state: typeof DicXmlState
  */
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    super(store);
    this.services = [ 'runInfo', 'state', 'runConfig' ];

    bindAll(this, [ 'onError', 'onRunInfo', 'onRunConfig', 'onState' ]);
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    debug('connecting to EACS:', dns);
    this.close();
    if (dns) {
      this.runInfo = new DicXmlParams('EACS/RunInfo',
        { proxy: currentUrl() }, dnsUrl(dns));
      this.runInfo.on('error', this.onError);
      this.runInfo.on('value', this.onRunInfo);
      this.runInfo.promise().catch(this.onError);

      this.state = new DicXmlState('EACS/State',
        { proxy: currentUrl() }, dnsUrl(dns));
      this.state.on('error', this.onError);
      this.state.on('value', this.onState);
      this.state.promise().catch(this.onError);

      this.runConfig = new DicXmlParams('EACS/RunConfig',
        { proxy: currentUrl() }, dnsUrl(dns));
      this.runConfig.on('error', this.onError);
      this.runConfig.on('value', this.onRunConfig);
      this.runConfig.promise().catch(this.onError);
    }
    else {
      this.onRunInfo(null);
      this.onRunConfig(null);
    }
  }

  /**
   * @param {?any} data
   */
  onRunInfo(data /*: any */) {
    const indexed = keyBy(data, 'index');
    const values = /** @type RunInfo */(transform(RunInfoMap, (ret, param, name) => {
      // @ts-ignore
      ret[name] = get(indexed, [ param.index, 'value' ], null);
    }, {}));
    debug('runInfo retrieved');
    this.store.commit('eacs/runInfo/update', values);
  }

  /**
   * @param {?any} data
   */
  onRunConfig(data /*: any */) {
    const indexed = keyBy(data, 'index');
    const values = /** @type RunConfig */(transform(RunConfigMap, (ret, param, name) => {
      // @ts-ignore
      ret[name] = get(indexed, [ param.index, 'value' ], null);
    }, {}));
    this.store.commit('eacs/runConfig/update', values);
  }

  /**
   * @param {any} data
   */
  onState(data /*: any */) {
    this.store.commit('eacs/update', data);
  }
}
