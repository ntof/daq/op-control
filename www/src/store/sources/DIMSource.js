// @ts-check
import { forEach, get } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';

/**
 * @brief helper abstract class for DIM based sources
 */
export default class DIMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    this.store = store;
    this.unwatch = this.store.watch((state) => state.dns,
      (dns) => this.onDnsUpdate(dns));
    /** @type {string[]|null} */
    this.services = null;
  }

  connect() {
    this.onDnsUpdate(get(this.store, [ 'state', 'dns' ]));
  }

  destroy() {
    this.close(true);
  }

  /**
   * @param {?string} dns
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onDnsUpdate(dns /*: ?string */) { /* jshint unused:false */
    this.close();
  }

  close(destroy = false) {
    // @ts-ignore this stuff should be down in the hierarchy
    if (this[get(this, [ 'services', 0 ])]) {
      // @ts-ignore
      forEach(this.services, (srv) => {
        // @ts-ignore
        this[srv].close();
        // @ts-ignore
        this[srv] = null;
      });
    }
    if (destroy) { this.unwatch(); }
  }

  /**
   * @param {any} err
   */
  onError(err /*: any */) {
    logger.error(err);
  }
}
