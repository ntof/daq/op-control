// @ts-check
import { get, isNil, map } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import axios from 'axios';
import { currentUrl } from '../../utilities';
import { sources } from '../index';

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index';
*/

export default class DBCommentsSource {
  /*::
  services: Array<string>
  store: App$Store
  unwatch: () => void
  timerId: IntervalID

  static INTERVAL: number
  */
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    this.store = store;
    this.unwatch = this.store.watch((state) => get(state, 'eacs.runInfo.runNumber'),
      () => { this.fetchComments(); });
    this.timerId = setInterval(() => this.fetchComments(), DBCommentsSource.INTERVAL);
  }

  async fetchComments() {
    const runNumber = get(this.store.state, 'eacs.runInfo.runNumber');

    if (isNil(runNumber)) {
      this.store.commit('db/comments', null);
      return;
    }

    await axios.get(currentUrl() + '/db/comments', {
      params: { filter: 'runNumber:' + runNumber, sort: 'date:desc' }
    })
    .then((ret) => {
      this.store.commit('db/comments', ret.data);
      return sources.db.fetchUsers(map(ret.data, 'userId'));
    })
    .catch((err) => this.onError(err));
  }

  /**
   * @param {string} comments
   * @returns {Promise<any>}
   */
  async sendComment(comments /*: string */) {
    const runNumber = get(this.store.state, 'eacs.runInfo.runNumber');

    if (isNil(runNumber)) {
      throw 'Failed to send comment, no active runs';
    }
    return await axios.post(currentUrl() + '/db/comments',
      { runNumber, comments })
    .then(() => this.fetchComments());
  }

  destroy() {
    this.unwatch();
    clearInterval(this.timerId);
  }

  /**
   * @param {any} err
   */
  onError(err /*: any */) {
    logger.error(err);
  }
}
DBCommentsSource.INTERVAL = 15000;
