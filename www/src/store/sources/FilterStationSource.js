// @ts-check
import { bindAll, get } from 'lodash';
import { DicXmlParams } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';
import DIMSource from './DIMSource';
import { FilterStationParamsMap } from '../../interfaces/filterStation';
import { parseDataSet } from '../../interfaces';

import d from 'debug';

const debug = d('source:filterStation');

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index'
*/

export default class FilterStationSource extends DIMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    super(store);
    this.unwatch();
    /** @type {() => void} */
    this.unwatch =
      this.store.watch((state) => get(state, 'eacs.info.filterStation'),
        (/** @type string */ dns) => this.onDnsUpdate(dns));
    /** @type {Array<string>} */
    this.services = [ 'client' ];

    bindAll(this, [ 'onError', 'onData' ]);
  }

  /**
   * @param {?string} dns
   */
  onDnsUpdate(dns /*: ?string */) {
    this.close();
    if (dns) {
      /** @type {string} */
      const filterStation = get(this.store.state, 'eacs.info.filterStation');
      debug('connecting filterStation: %s', filterStation);
      this.client = new DicXmlParams(
        filterStation,
        { proxy: currentUrl() }, dnsUrl(dns));
      this.client.on('error', this.onError);
      this.client.on('value', this.onData);
      this.client.promise().catch(this.onError);
    }
    else {
      this.onData(null);
    }
  }

  /**
   * @typedef {{
   *   airPressure: number,
   *   filter1: number,
   *   filter2: number,
   *   filter3: number,
   *   filter4: number,
   *   filter5: number,
   *   filter6: number,
   *   filter7: number,
   *   filter8: number
   * }} FilterStationData
   */

  /**
   * @param {any} data
   */
  onData(data /*: any */) {
    const parseData = /** @type FilterStationData */(parseDataSet(data, FilterStationParamsMap));

    debug('filterStation config fetched');
    this.store.commit('filterStation/update', {
      airPressure: parseData.airPressure, filters: [
        parseData.filter1, parseData.filter2, parseData.filter3,
        parseData.filter4, parseData.filter5, parseData.filter6,
        parseData.filter7, parseData.filter8
      ]
    });
  }
}
