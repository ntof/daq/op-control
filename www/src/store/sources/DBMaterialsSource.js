// @ts-check
import { forEach, get, isNil, map } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import axios from 'axios';
import { currentUrl } from '../../utilities';

/*::
import { type Store } from 'vuex'
import { type App$Store } from '../index';
*/

export default class DBMaterialsSource {
  /*::
  services: Array<string>
  store: App$Store
  unwatch: Array<() => void>

  static INTERVAL: number
  */
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    this.store = store;
    this.unwatch = [
      this.store.watch((state) => get(state, 'eacs.runInfo.detectorsSetupId'),
        () => this.fetchDetectorsSetup(true)),
      this.store.watch((state) => get(state, 'eacs.runInfo.materialsSetupId'),
        () => this.fetchMaterialsSetup(true)),
      this.store.watch((state) => get(state, 'eacs.runConfig.detectorsSetupId'),
        () => this.fetchDetectorsSetup(false)),
      this.store.watch((state) => get(state, 'eacs.runConfig.materialsSetupId'),
        () => this.fetchMaterialsSetup(false))
    ];
  }

  /**
   * @param {boolean} current
   * @returns {Promise<void>}
   */
  async fetchDetectorsSetup(current /*: boolean */) {
    const setupId = get(this.store.state, current ?
      'eacs.runInfo.detectorsSetupId' : 'eacs.runConfig.detectorsSetupId');
    const storePath = `db/materials/${current ? 'current' : 'next'}DetectorsSet`;

    if (isNil(setupId) || setupId < 0) {
      this.store.commit(storePath, null);
      return;
    }

    try {
      const ret = await axios.get(`${currentUrl()}/db/detectorsSetups/item/${setupId}/related/detectorsSetupInfo`, undefined);
      this.store.commit(storePath, ret.data);

      const detRet = await axios.get(`${currentUrl()}/db/detectors`,
        { params: { query: '{"id":{"$in":' + JSON.stringify(map(ret.data, 'detectorId')) + '}}' } });
      this.store.commit('db/materials/detectorsAdd', detRet.data);

      const contRet = await axios.get(`${currentUrl()}/db/containers`,
        { params: { query: '{"id":{"$in":' + JSON.stringify(map(ret.data, 'containerId')) + '}}' } });
      this.store.commit('db/materials/containersAdd', contRet.data);
    }
    catch (err) {
      this.onError(err);
    }
  }

  /**
   * @param {boolean} current
   * @returns {Promise<void>}
   */
  async fetchMaterialsSetup(current /*: boolean */) {
    const setupId = get(this.store.state, current ?
      'eacs.runInfo.materialsSetupId' : 'eacs.runConfig.materialsSetupId');
    const storePath = `db/materials/${current ? 'current' : 'next'}MaterialsSet`;

    if (isNil(setupId) || setupId < 0) {
      this.store.commit(storePath, null);
      return;
    }

    try {
      const ret = await axios.get(`${currentUrl()}/db/materialsSetups/item/${setupId}/related/materialsSetupInfo`, undefined);
      this.store.commit(storePath, ret.data);

      const materialsRet = await axios.get(`${currentUrl()}/db/materials`,
        { params: { query: '{"id":{"$in":' + JSON.stringify(map(ret.data, 'materialId')) + '}}' } });
      this.store.commit('db/materials/materialsAdd', materialsRet.data);
    }
    catch (err) {
      this.onError(err);
    }
  }

  destroy() {
    forEach(this.unwatch, (u) => u());
  }

  /**
   * @param {any} err
   */
  onError(err /*: any */) {
    logger.error(err);
  }
}
