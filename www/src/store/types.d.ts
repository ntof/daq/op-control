import { DaqState, AcqState } from '../interfaces/daq'
import {AlarmServer, DAQ, Timing, DB, EACS} from "../interfaces/types";
import {BoardInfo, HvCrate} from "../interfaces/hv";
import {HighVoltageWorker} from "../workers/types";

export = AppStore
export as namespace AppStore

declare namespace AppStore {

  interface State {
    dns: string|null;
    user: { username: string, [key: string]: any } | null;
  }

  interface AlarmState {
    filters: AlarmServer.Filters,
    alarms?: Array<AlarmServer.Alarm> | null,
    stats?: AlarmServer.Stats | null,
    loading: boolean,
    fetchStamp?: number | null
  }

  interface ZSPState extends DAQ.DaqConfig.ZSP {
    edit: DAQ.DaqConfig.ZSP;
  }

  interface DaqState {
    name: string|null;
    daqState: DaqState|null;
    acqState: AcqState|null;
  }

  interface DaqConfigState {
    channels: { [key: string]: DaqConfig.Channel },
    edit: { [key: string]: DaqConfig.Channel }
  }

  interface EacsState {
    strValue?: string;
    value?: number;
    errors?: Array<{ code: number, message: string } & Object<string, any>>;
    warnings?: Array<{ code: number, message: string } & Object<string, any>>;
    runInfo: EACS.RunInfo;
    runConfig: EACS.RunConfig;
    daqList: Object<string, EACS.DaqListInfo>;
    cardList: Object<string, EACS.DaqListCardInfo>;
    info: EACS.Info;
    timing: Timing.TimingData & { edit: Timing.TimingData };
  }

  interface MaterialsState {
    filters: Array<DB.Material>,
    samples: Array<DB.Material>,
    sources: Array<DB.Material>,
    detectors: Array<DB.Detector>,
    containers: Array<DB.Detector>,

    current: {
      materialsSetup: Array<DB.SetupMaterial>,
      detectorsSetup: Array<DB.SetupDetector>
    },
    next: {
      materialsSetup: Array<DB.SetupMaterial>,
      detectorsSetup: Array<DB.SetupDetector>
    },
  }

  interface DBUserState  {
    users: Array<DB.User>;
    comments: Array<DB.Comment>;
  }

  interface DBSetupState {
    materials: {};
    detectors: {};
  }

  interface UiState {
    showKeyHints: boolean
  }

  interface HvState {
    filters: HighVoltageWorker.Filters,
    hvs?: Array<ChannelInfo> | null,
    stats?: HighVoltageWorker.Stats | null,
    boardInfo?: Array<BoardInfo>,
    loading: boolean
  }

  interface FilterStationState {
    airPressure: number;
    filters: Array<number>;
  }


}

