// @ts-check

import { get, merge } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';

import eacs from './modules/eacs';
import db from './modules/db';
import filterStation from './modules/filterStation';
import alarms from './modules/alarms';
import hv from './modules/hv';
import HighVoltageSource from "./sources/HighVoltageSource";
import {
  sources as baseSources,
  createStore,
  storeOptions } from '@cern/base-vue';

import EACSSource from './sources/EACSSource';
import DBSource from './sources/DBSource';
import EACSDaqListSource from './sources/EACSDaqListSource';
import EACSTimingSource from './sources/EACSTimingSource';
import AlarmsSource from './sources/AlarmsSource';

Vue.use(Vuex);

/*::
import { type Eacs$StoreState } from './modules/eacs'
import { type Eacs$DBState } from './modules/db'
import { type Alarms$StoreState } from './modules/alarms'
import { type HV$StoreState } from './modules/hv'

export type App$StoreState = {
  dns: ?string,
  user: ?{ username: string, ... },
  eacs: Eacs$StoreState,
  db: Eacs$DBState,
  filterStation: { airPressure: number, filters: Array<number> },
  alarms: Alarms$StoreState,
  hv: HV$StoreState
}

export type App$Store = Vuex.Store<App$StoreState>;
*/


merge(storeOptions, /** @type {V.StoreOptions<AppStore.State>} */ ({
  state: {
    dns: null,
    user: null
  },
  mutations: {
    queryChange(state, query) {
      state.dns = get(query, 'dns', null);
    },
    user(state /*: App$StoreState */, user /*: ?{ username: string, ... } */) {
      state.user = user;
    }
  },
  modules: {
    eacs, db, filterStation, alarms, hv
  }
}));

const store = /** @type {V.Store<AppStore.State>} */ (createStore());
export default store;

export const sources = merge(baseSources, {
  eacs: new EACSSource(store),
  db: new DBSource(store),
  daqList: new EACSDaqListSource(store),
  timing: new EACSTimingSource(store),
  alarms: new AlarmsSource(store),
  hv: new HighVoltageSource(store)
});

/**
 * @param {string} name
 * @param {any} defaultValue
 * @returns {function(any): any}
 */
export function getUiState(name, defaultValue = null) {
  return /** @this {Vue} */ function(state) {
    if (state[name] === undefined) {
      Vue.set(state, name, defaultValue);
      this.$store.commit('ui/update', { [name]: defaultValue });
    }
    return state[name];
  };
}
