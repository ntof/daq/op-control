// @ts-check

import { forEach, get, transform } from 'lodash';
import Vue from 'vue';
import d from 'debug';

const debug = d('store:materials');

/*::

export type Eacs$MaterialType = "sample"|"filter"|"source"

export type Eacs$Material = {
  id: number,
  title: string,
  type: Eacs$MaterialType
}

export type Eacs$SetupMaterial = {
  materialId: number,
  position: number,
  status: number
}

export type Eacs$SetupDetector = {
  detectorId: number,
  containerId: number
}

export type Eacs$Detector = {
  name: string
}

export type Eacs$Container = {
  name: string
}

export type Eacs$DBMaterials = {
  filters: { [number]: Eacs$Material },
  samples: { [number]: Eacs$Material },
  sources: { [number]: Eacs$Material },
  detectors: { [number]: Eacs$Detector },
  containers: { [number]: Eacs$Container },

  current: {
    materialsSetup: Array<Eacs$SetupMaterial>,
    detectorsSetup: Array<Eacs$SetupDetector>
  },
  next: {
    materialsSetup: Array<Eacs$SetupMaterial>,
    detectorsSetup: Array<Eacs$SetupDetector>
  },
}

*/

/**
 * @typedef {(import('../../interfaces/types').DB.Material)} Material
 * @typedef {(import('../../interfaces/types').DB.SetupMaterial)} SetupMaterial
 * @typedef {(import('../../interfaces/types').DB.Container)} Container
 * @typedef {(import('../../interfaces/types').DB.Detector)} Detector
 *
 * @typedef {{
 *   sources: Array<SetupMaterial>,
 *   filters: Array<SetupMaterial>,
 *   samples: Array<SetupMaterial>
 * }} SplitMaterialSetup
 */


/**
 * @param {AppStore.MaterialsState} state
 * @param {boolean} current
 * @returns {SplitMaterialSetup}
 */
function splitMaterialsSetup(state /*: Eacs$DBMaterials */, current /*: boolean */) {
  const ret = /** @type SplitMaterialSetup */ transform(get(state,
    [ (current ? 'current' : 'next'), 'materialsSetup' ]),
  (/** @type SplitMaterialSetup */ ret, /** @type SetupMaterial */ matSetup) => {
    const matId = matSetup.materialId;
    if (state.filters[matId]) {
      ret.filters.push(matSetup);
    }
    else if (state.samples[matId]) {
      ret.samples.push(matSetup);
    }
    else if (state.sources[matId]) {
      ret.sources.push(matSetup);
    }
    else {
      debug('unknown material in setup %o', matSetup);
    }
  }, { filters: [], samples: [], sources: [] });
  return ret;
}

/** @type {V.Module<AppStore.MaterialsState>} */
export default {
  namespaced: true,
  state: () => ({
    filters: {}, samples: {}, sources: {},
    current: {}, next: {}, detectors: {}, containers: {}
  }),
  mutations: {
    /**
     * @param {AppStore.MaterialsState} state
     * @param {Array<Material>} materials
     */
    materialsAdd: (state, materials /*: Array<Eacs$Material> */) => {
      forEach(materials, (m /*: Eacs$Material */) => {
        if (m.type === 'sample') {
          Vue.set(state.samples, m.id, m);
        }
        else if (m.type === 'source') {
          Vue.set(state.sources, m.id, m);
        }
        else if (m.type === 'filter') {
          Vue.set(state.filters, m.id, m);
        }
        else {
          debug('unknown material type: %o', m);
        }
      });
    },
    /**
     * @param {AppStore.MaterialsState} state
     * @param {Array<Detector>} detectors
     */
    detectorsAdd: (state /*: Eacs$DBMaterials */, detectors /*: Array<Eacs$Detector> */) => {
      forEach(detectors, (d) => Vue.set(state.detectors, d.id, d));
    },
    /**
     * @param {AppStore.MaterialsState} state
     * @param {Array<Detector>} containers
     */
    containersAdd: (state /*: Eacs$DBMaterials */, containers /*: Array<Eacs$Container> */) => {
      forEach(containers, (d) => Vue.set(state.containers, d.id, d));
    },
    /**
     * @param {AppStore.MaterialsState} state
     * @param {Array<SetupMaterial>} data
     */
    currentMaterialsSet: (state /*: Eacs$DBMaterials */, data /*: Array<Eacs$SetupMaterial> */) => {
      Vue.set(state.current, 'materialsSetup', data);
    },
    /**
     * @param {AppStore.MaterialsState} state
     * @param {Array<SetupMaterial>} data
     */
    nextMaterialsSet: (state /*: Eacs$DBMaterials */, data /*: Array<Eacs$SetupMaterial> */) => {
      Vue.set(state.next, 'materialsSetup', data);
    },
    /**
     * @param {AppStore.MaterialsState} state
     * @param {Array<SetupMaterial>} data
     */
    currentDetectorsSet: (state /*: Eacs$DBMaterials */, data /*: Array<Eacs$SetupDetector> */) => {
      Vue.set(state.current, 'detectorsSetup', data);
    },
    /**
     * @param {AppStore.MaterialsState} state
     * @param {Array<SetupMaterial>} data
     */
    nextDetectorsSet: (state /*: Eacs$DBMaterials */, data /*: Array<Eacs$SetupDetector> */) => {
      Vue.set(state.next, 'detectorsSetup', data);
    }
  },
  getters: {
    /**
     * @param {AppStore.MaterialsState} state
     */
    currentMaterials: (state /*: Eacs$DBMaterials */) => splitMaterialsSetup(state, true),
    /**
     * @param {AppStore.MaterialsState} state
     */
    nextMaterials: (state  /*: Eacs$DBMaterials */) => splitMaterialsSetup(state, false)
  }
};
