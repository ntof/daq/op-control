// @ts-check

import { assign, get } from 'lodash';
import { State } from '../../interfaces/eacs';
import { TimingParamsMap } from '../../interfaces/timing';
import { paramsModule } from './utils';

const MAX_UINT32 = 4294967295;

/**
 * @typedef {import('../../interfaces/types').EACS.DaqListInfo} DaqListInfo
 * @typedef {import('../../interfaces/types').EACS.RunInfo} RunInfo
 * @typedef {import('../../interfaces/types').EACS.RunConfig} RunConfig
 * @typedef {import('../../interfaces/types').EACS.DaqListCardInfo} DaqListCardInfo
 * @typedef {import('../../interfaces/types').EACS.Info} Info
 */

/** @type {V.Module<AppStore.EacsState>} */
export default {
  namespaced: true,
  state: () => ({
    strValue: null, value: null, errors: null, warnings: null,
    daqList: null, cardList: null, info: null
  }),
  getters: {
    /**
     * @param {AppStore.EacsState} state
     * @returns {boolean}
     */
    isActive: (state) => { // eslint-disable-line complexity
      switch (state.value) {
      case State.FILTERS_INIT:
      case State.SAMPLES_INIT:
      case State.COLLIMATOR_INIT:
      case State.HIGHVOLTAGE_INIT:
      case State.DAQS_INIT:
      case State.MERGER_INIT:
      case State.STARTING:
      case State.RUNNING:
        return true;
      default: return false;
      }
    }
  },
  mutations: {
    update: assign,
    /**
     * @param {AppStore.EacsState} state
     * @param {Object<string, DaqListInfo>} daqList
     */
    daqList: (state, daqList) => {
      state.daqList = daqList;
    },
    /**
     * @param {AppStore.EacsState} state
     * @param {Object<string, DaqListCardInfo>} cardList
     */
    cardList: (state, cardList) => {
      state.cardList = cardList;
    },
    /**
     * @param {AppStore.EacsState} state
     * @param {Info} info
     */
    info: (state, info) => {
      state.info = info;
    }
  },
  modules: {
    runInfo: {
      namespaced: true,
      state: () => ({
        runNumber: null,
        eventNumber: null,
        validatedNumber: null,
        validatedProtons: null,
        title: null,
        description: null,
        area: null,
        experiment: null,
        detectorsSetupId: null,
        materialsSetupId: null
      }),
      mutations: {
        /**
         * @param  {any} state
         * @param  {Partial<RunInfo>|null} value
         */
        update: (state, value) => {
          assign(state, value);
          if (get(value, [ 'eventNumber' ], 0) >= MAX_UINT32) {
            state.eventNumber = null;
          }
          if (get(value, 'validatedNumber', 0) >= MAX_UINT32) {
            state.validatedNumber = null;
          }

        }
      }
    },
    runConfig: {
      namespaced: true,
      state: () => ({
        runNumber: null, title: null, description: null, experiment: null,
        detectorsSetupId: null, materialsSetupId: null
      }),
      mutations: {
        update: assign
      }
    },
    daq: { /* required to dynamically created daqs */
      namespaced: true,
      state: () => null
    },
    timing: paramsModule(TimingParamsMap)
  }
};
