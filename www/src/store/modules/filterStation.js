// @ts-check

import { assign } from 'lodash';

/*::
export type FilterStation$StoreState = {
  airPressure: number,
  filters: Array<number>
}
*/

/** @type {V.Module<AppStore.FilterStationState>} */
export default {
  namespaced: true,
  state: () => ({
    airPressure: null,
    filters: null
  }),
  mutations: {
    update: assign
  }
};
