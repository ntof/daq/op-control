// @ts-check

import { assign, cloneDeep, constant, get, mapValues, omit, pickBy } from 'lodash';
import { genDataSet, parseValue } from '../../interfaces';

/**
 * @typedef {import('../../interfaces/types').XML.ParamMap} ParamMap
 *
 * @typedef {V.Module<{edit: any } & {[key:string]: any}>} GenericParamModule
 */

/**
 * @param {ParamMap} paramMap
 * @returns {GenericParamModule}
 */

export function paramsModule(paramMap) {
  return {
    namespaced: true,
    state: () => ({ edit: mapValues(paramMap, constant(null)), ...mapValues(paramMap, constant(null)) }),
    mutations: {
      update: assign,
      /**
       * @param {{ edit: any, [key:string]: any }} state
       */
      resetEdit(state /*: { edit: any, ... } */) {
        state.edit = cloneDeep(omit(state, [ 'edit' ]));
      },
      /**
       * @param {{ edit: any, [key:string]: any }} state
       */
      editValue(state /*: { edit: any, ... } */, val /*: { name: string, value: any } */) {
        state.edit[val.name] = parseValue(val.value, get(paramMap, [ val.name, 'type' ]));
      },
      /**
       * @param {{ edit: any, [key:string]: any }} state
       */
      clear(state /*: { edit: any, ... } */) {
        state.edit = mapValues(paramMap, constant(null));
        assign(state, state.edit); /* we've just reset it, don't map again */
      }
    },
    getters: {
      /**
       * @param {{ edit: any, [key:string]: any }} state
       */
      getDataSetChanges(state /*: { edit: any, ... } */) {
        const values = /** @type Object<string, any> */ (pickBy(state.edit,
          (value, name) => state[name] !== value));
        return genDataSet(values, paramMap);
      }
    }
  };
}
