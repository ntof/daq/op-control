// @ts-check

import { assign, keyBy } from 'lodash';
import dbMaterials from './dbMaterials';
import dbSetups from "./dbSetups";

/*::

export type Eacs$Setup = {
  id: number,
  name: string,
  description: string,
  earNumber: number,
  operYear: number
}

export type Eacs$DBUser = {
  userId: number,
  username: string
}

export type Eacs$DBComment = {
  id: number,
  date: number,
  comments: string,
  userId: number
}

import type { Eacs$DBMaterials } from './dbMaterials'


export type Eacs$DBState = {
  users: ?{ [userId: string]: Eacs$DBUser },
  comments: ?Array<Eacs$DBComment>,
  setups: {
    materials: { [number]: Eacs$Setup },
    detectors: { [number]: Eacs$Setup },
  },
  materials: Eacs$DBMaterials,
}
*/

/**
 * @typedef {(import('../../interfaces/types').DB.User)} User
 * @typedef {(import('../../interfaces/types').DB.Comment)} Comment
 */


/** @type {V.Module<AppStore.DBUserState>} */
export default {
  namespaced: true,
  state: () => ({ users: {}, comments: null }),
  mutations: {
    /**
     * @param {AppStore.DBUserState} state
     * @param {Array<User>} users
     */
    usersAdd: (state /*: any */, users /*: Array<{ userId: number, ...}> */) => {
      assign(state.users, keyBy(users, 'id'));
    },
    /**
     * @param {AppStore.DBUserState} state
     * @param {Array<Comment>} comments
     */
    comments: (state /*: any */, comments /*: Array<{...}> */) => {
      state.comments = comments;
    }
  },
  modules: {
    materials: dbMaterials,
    setups: dbSetups
  }
};
