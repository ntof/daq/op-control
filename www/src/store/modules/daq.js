// @ts-check

import {
  assign, cloneDeep, forEach, get, isEmpty, isNil, map, set, toString,
  without } from 'lodash';
import d from 'debug';
import Vue from 'vue';
import { genDataSet, parseValue } from '../../interfaces';
import { ChannelParamsMap, ZSPMap } from '../../interfaces/daq';

const debug = d('store:daq');

/**
 * @typedef {(import('../../interfaces/types').DAQ.DaqConfig.ZSPChannel)} ZSPChannel
 * @typedef {(import('../../interfaces/types').DAQ.DaqConfig.ZSP)} ZSP
 * @typedef {(import('../../interfaces/types').DAQ.DaqConfig.Channel)} Channel
 * @typedef {(import('../../interfaces/types').DAQ.ChanId)} ChanId
 * @typedef {(import('../sources/DaqChannelSource').default)} DaqChannelSource
 */

/**
 * @param {ZSP} state
 * @param {ChanId} chanId
 * @returns {{}|{ slave: ChanId[], channel: Channel, sn: string } | { channel: Channel, sn: string }}
 */
function getChannelDataSet(state /*: DaqConfig$ZSP */, chanId /*: DaqConfig$ChanId */) {
  const channel = get(state, [ 'channels', chanId ]);
  if (!channel) {
    return {};
  }
  else if (channel.slave) {
    return {
      sn: channel.sn, channel: channel.channel,
      slave: map(channel.slave, (slaveId) => getChannelDataSet(state, slaveId))
    };
  }
  else {
    return { sn: channel.sn, channel: channel.channel };
  }
}

/** @type {V.Module<AppStore.ZSPState>} */
export const DaqZspStore = {
  namespaced: true,
  state: () => ({
    edit: {
      mode: null,
      master: {},
      channels: {},
      independent: {}
    },
    mode: null,
    master: {},
    channels: {},
    independent: {}
  }),
  mutations: {
    /**
     * @param {AppStore.ZSPState} state
     * @param {{mode: number, channels: {[key: ChanId]: Channel} }} config
     */
    update(state, config) {
      const master = {};
      const indep = {};

      state.mode = config.mode;
      forEach(config.channels, (/** @type ZSPChannel */chan) => {
        if (!isNil(chan.slave)) {
          // @ts-ignore
          master[chan.id] = chan.id;
          forEach(chan.slave, (slaveId) => {
            set(config.channels, [ slaveId, 'masterId' ], chan.id);
          });
        }
      });
      forEach(config.channels, (/** @type ZSPChannel */chan) => {
        if (isNil(chan.masterId) && isNil(chan.slave)) {
          // @ts-ignore
          indep[chan.id] = chan.id;
        }
      });
      Vue.set(state, 'channels', config.channels);
      Vue.set(state, 'master', master);
      Vue.set(state, 'independent', indep);
    },
    /**
     * @param {AppStore.ZSPState} state
     */
    resetEdit(state) {
      state.edit.mode = state.mode;
      Vue.set(state.edit, 'channels', cloneDeep(state.channels));
      Vue.set(state.edit, 'master', cloneDeep(state.master));
      Vue.set(state.edit, 'independent', cloneDeep(state.independent));
    },
    /**
     * @param {AppStore.ZSPState} state
     */
    clear(state) {
      // @ts-ignore
      assign(state, DaqZspStore.state());
    },
    /**
     * @param {AppStore.ZSPState} state
     * @param {ChanId} masterId
     */
    clearMaster(state, masterId) {
      const master = get(state.edit, [ 'channels', masterId ]);
      if (master) {
        forEach(master.slave, (slaveId) => {
          const slave = get(state.edit, [ 'channels', slaveId ]);
          Vue.delete(slave, 'masterId');
          Vue.set(state.edit.independent, slaveId, slaveId);
        });
        Vue.delete(master, 'slave');
        Vue.delete(state.edit.master, masterId);
        Vue.set(state.edit.independent, masterId, masterId);
      }
    },
    /**
     * @param {AppStore.ZSPState} state
     * @param {ZSPMode} mode
     */
    setMode(state, mode) {
      DaqZspStore.mutations.clearMasters(state);
      Vue.set(state.edit, 'mode', mode);
    },
    /**
     * @param {AppStore.ZSPState} state
     */
    clearMasters(state) {
      forEach(state.edit.master, (masterId) => {
        DaqZspStore.mutations.clearMaster(state, masterId);
      });
    },
    /**
     * @param {AppStore.ZSPState} state
     * @param {ChanId} id
     */
    setSingleMaster(state, id) {
      DaqZspStore.mutations.clearMasters(state);
      if (id) {
        Vue.set(state.edit, 'master', { [id]: id });
        Vue.delete(state.edit.independent, id);
      }
    },
    /**
     * @param {AppStore.ZSPState} state
     * @param {{id: ChanId, masterId: ChanId}} move
     */
    moveChannel( // eslint-disable-line complexity, max-statements
      state, move) {

      const slave = get(state.edit, [ 'channels', move.id ]);
      if (!slave || (slave.masterId === move.masterId) ||
          (slave.id === move.masterId) || !isNil(slave.slave)) {
        debug('not moving %o move: %o', slave, move);
        return;
      }
      if (slave.masterId) {
        const master = get(state.edit, [ 'channels', slave.masterId ]);
        if (!master) {
          debug('master not found %s', slave.masterId);
        }
        else {
          debug('removing %s from master %s', slave.id, master.id);
          master.slave = without(master.slave, slave.id);
          Vue.delete(slave, 'masterId');
          if (isEmpty(master.slave)) {
            debug('master %s moved to independent', master.id);
            Vue.delete(master, 'slave');
            Vue.delete(state.edit.master, master.id);
            Vue.set(state.edit.independent, master.id, master.id);
          }
        }
      }
      else {
        debug('removing %s from independent', slave.id);
        Vue.delete(state.edit.independent, slave.id);
      }

      if (isNil(move.masterId)) {
        debug('adding %s to independent', slave.id);
        Vue.set(state.edit.independent, slave.id, slave.id);
      }
      else {
        // @ts-ignore
        const master = state.edit.channels[move.masterId];
        if (isNil(master)) {
          debug('master not found %s', move.masterId);
          return;
        }
        else if (master.masterId) {
          debug("can't add channel on a slave %s", move.masterId);
          return;
        }
        debug('adding %s to master %s', slave.id, master.id);
        if (!master.slave) {
          Vue.set(master, 'slave', []);
        }
        // $FlowIgnore: checked the line above
        Vue.set(master.slave, master.slave.length, slave.id);
        Vue.set(slave, 'masterId', master.id);
        Vue.delete(state.edit.independent, master.id);
        Vue.set(state.edit.master, master.id, master.id);
      }
    }
  },
  getters: {
    /**
     * @param {AppStore.ZSPState} state
     */
    getDataSetChanges(state /*: DaqConfig$ZSPStoreState */) {
      return genDataSet({
        mode: state.edit.mode,
        configuration: {
          master: map(state.edit.master, (masterId) => getChannelDataSet(state.edit, masterId))
        }
      }, ZSPMap);
    }
  }
};

/** @type {V.Module<AppStore.DaqConfigState>} */
export const DaqConfigStore = {
  namespaced: true,
  modules: {
    zsp: DaqZspStore
  },
  state: () => ({
    channels: {},
    edit: {}
  }),
  mutations: {
    /**
     * @param {AppStore.DaqConfigState} state
     * @param {Channel} channel
     */
    updateChannel(state /*: DaqConfig$StoreState */, channel /*: DaqConfig$Channel */) {
      debug('channel update %s', channel.id, channel.sampleRate);
      Vue.set(state.channels, channel.id, channel);
    },
    /**
     * @param {AppStore.DaqConfigState} state
     */
    clear(state /*: DaqConfig$StoreState */) {
      debug('clearing daq');
      state.channels = {};
      state.edit = {};
    },
    /**
     * @param {AppStore.DaqConfigState} state
     */
    resetEdit(state /*: DaqConfig$StoreState */) {
      debug('daq edit reset');
      state.edit = cloneDeep(state.channels);
    },
    /**
     * @param {AppStore.DaqConfigState} state
     * @param {{id: string, name: string, value: any}} value
     */
    editChannel(state  /*: DaqConfig$StoreState */, value /*: { id: string, name: string, value: any } */) {
      const chan = state.edit[value.id];
      if (!chan) {
        debug('Failed to find channel:', value.id);
        return;
      }
      chan[value.name] = parseValue(value.value,
        get(ChannelParamsMap, [ value.name, 'type' ]));
    }
  },
  actions: {
    /**
     * @param {any} context
     * @param {DaqChannelSource} source
     * @returns {Promise<*>}
     */
    async send(context /*: ActionContext<DaqConfig$StoreState> */, source /*: DaqChannelSource */) {
      const config = get(context.state, [ 'edit', source.id ]);
      if (!get(config, 'isZSP', true)) {
        debug('disabling ZSP on channel:%s', source.id);
        const sampleSize = get(config, [ 'sampleSize' ], 0);
        const sampleRate = get(config, [ 'sampleRate' ], 1);
        config.zsStart = toString(sampleSize / sampleRate * 1e6);
      }
      const changes = genDataSet(config, ChannelParamsMap);

      /* always fully configure all channels */
      if (!isEmpty(changes)) {
        debug('sending channel params to %s', source.id);
        // @ts-ignore
        return source.client.setParams(changes);
      }
    }
  }
};
