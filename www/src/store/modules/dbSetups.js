// @ts-check

import { keyBy, map } from 'lodash';

/**
 * @typedef {(import('../../interfaces/types').DB.Material)} Material
 * @typedef {(import('../../interfaces/types').DB.SetupMaterial)} SetupMaterial
 * @typedef {(import('../../interfaces/types').DB.Container)} Container
 * @typedef {(import('../../interfaces/types').DB.Detector)} Detector
 *
 * @typedef {{
 *   sources: Array<SetupMaterial>,
 *   filters: Array<SetupMaterial>,
 *   samples: Array<SetupMaterial>
 * }} SplitMaterialSetup
 */


/**
 * @param {any} list
 * @returns {Array<any>}
 */
function makeOpts(list) {
  const setup = map(list, (m) => ({ value: m.id, text: m.name }));
  setup.unshift({ value: -1, text: 'Not Set' });
  return setup;
}

/** @type {V.Module<AppStore.DBSetupState>} */
export default {
  namespaced: true,
  state: () => ({ materials: {}, detectors: {} }),
  getters: {
    /**
     * @param {AppStore.DBSetupState} state
     * @returns {Array<any>}
     */
    detectorsOpts: (state /*: any */) => makeOpts(state.detectors),
    /**
     * @param {AppStore.DBSetupState} state
     * @returns {Array<any>}
     */
    materialsOpts: (state /*: any */) => makeOpts(state.materials)
  },
  mutations: {
    /**
     * @param {AppStore.DBSetupState} state
     * @param {Array<any>} values
     */
    materials: (state /*: any */, values /*: any */) => { state.materials = keyBy(values, 'id'); },
    /**
     * @param {AppStore.DBSetupState} state
     * @param {Array<any>} values
     */
    detectors: (state /*: any */, values /*: any */) => { state.detectors = keyBy(values, 'id'); }
  }
};
