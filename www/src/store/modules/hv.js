// @ts-check
import { assign } from "lodash";

/**
 * @typedef {import("../../workers/types").HighVoltageWorker.Stats} Stats
 * @typedef {import('../../interfaces/hv').BoardInfo} BoardInfo
 * @typedef {import("../../workers/types").HighVoltageWorker.Filters} Filters
 */

/** @type {V.Module<AppStore.HvState>} */
const hvModule = {
  namespaced: true,
  state: () => ({
    filters: {
      offset: 0,
      limit: 100,
      search: null,
      orderBy: null
    },
    hvs: null,
    stats: null,
    boardInfo: [],
    loading: false
  }),
  mutations: {
    update: assign,
    /**
     * @param {AppStore.HvState} state
     * @param {Filters} filters
     */
    filters: (state, filters) => {
      state.filters = filters;
    },
    /**
     * @param {AppStore.HvState} state
     * @param hvs
     */
    hvs: (state, hvs) => {
      state.hvs = hvs;
    },
    /**
     * @param {AppStore.HvState} state
     * @param {Stats} stats
     */
    stats: (state, stats) => {
      state.stats = stats;
    },
    /**
     * @param {AppStore.HvState} state
     * @param {Array<BoardInfo>} stats
     */
    boardInfo: (state, boardInfo) => {
      state.boardInfo = boardInfo;
    },
    /**
     * @param {AppStore.HvState} state
     * @param {boolean} isLoading
     */
    loading: (state, isLoading) => {
      state.loading = isLoading;
    }
  }
};
export default hvModule;
