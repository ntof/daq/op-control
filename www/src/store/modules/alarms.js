// @ts-check

import { assign } from 'lodash';

/**
 * @typedef {import('../../interfaces/types').AlarmServer.Filters} Filters
 */

/** @type {V.Module<AppStore.AlarmState>} */
const alarmModule = {
  namespaced: true,
  state: () => ({
    filters: {
      offset: 0,
      limit: 1000, // first 1000 alarms only
      search: null,
      orderBy: null
    },
    alarms: null,
    stats: null,
    loading: false,
    fetchStamp: null
  }),
  mutations: {
    update: assign,
    /**
     * @param {AppStore.AlarmState} state
     * @param {Filters} filters
     */
    filters: (state, filters) => {
      state.filters = filters;
    },
    /**
     * @param {AppStore.AlarmState} state
     * @param {boolean} isLoading
     */
    loading: (state /*: Alarms$StoreState */, isLoading /*: boolean */) => {
      state.loading = isLoading;
    },
    /**
     * @param {AppStore.AlarmState} state
     * @param {number?=} stamp
     */
    fetchStamp: (state /*: Alarms$StoreState */, stamp /*: ?number */) => {
      state.fetchStamp = stamp;
    }
  }
};
export default alarmModule;
