// @ts-check
import { DicXmlCmd } from '@ntof/redim-client';
import { UrlUtilitiesMixin, errorPrefix } from '../utilities';
import Vue from "vue";

const wrapError = errorPrefix.bind(null, 'EACS: ');

/**
 * @typedef {V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */
const component = Vue.extend({
  mixins: [ UrlUtilitiesMixin ],
  methods: {
    /**
     * @this {Instance}
     * @param {string} dns
     * @return {Promise<void>}
     */
    eacsStart(dns /*: string */) {
      return DicXmlCmd.invoke('EACS',
        { command: { '$': { name: 'start' } } },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    },

    /**
     * @this {Instance}
     * @param {string} dns
     * @param {string} dataStatus
     * @return {Promise<void>}
     */
    eacsStop(dns, dataStatus) {
      return DicXmlCmd.invoke('EACS',
        { command: { '$': { name: 'stop', dataStatus } } },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    },

    /**
     * @this {Instance}
     * @param {string} dns
     * @return {Promise<void>}
     */
    eacsReset(dns /*: string */) {
      return DicXmlCmd.invoke('EACS',
        { command: { '$': { name: 'reset' } } },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    }
  }
});
export default component;
