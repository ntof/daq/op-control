// @ts-check
import { get, lowerCase } from 'lodash';
import Vue from 'vue';

/**
 * @param  {Element|null} elt
 * @param  {string} tagName
 * @return {Element|null}
 */
function findParent(elt, tagName) {
  while (elt && lowerCase(elt.tagName) !== tagName) {
    elt = elt.parentElement;
  }
  return elt;
}

/**
 * @param  {HTMLElement|null}  elt
 * @return {boolean}
 */
function isVisible(elt) {
  while (elt) {
    if (elt.hidden ||
      (elt.style &&
       (elt.style.visibility === 'hidden' ||
        elt.style.display === 'none'))) {
      return false;
    }
    elt = get(elt, 'parentElement');
  }
  return true;
}

/**
 * @param  {HTMLCollectionOf<HTMLElement>} inputs [description]
 * @return {HTMLElement|null}
 */
function findEditElement(inputs) {
  for (let i = 0; i < inputs.length; ++i) {
    const input = inputs.item(i);
    if (!get(input, 'disabled', false) && isVisible(input)) {
      return input;
    }
  }
  return null;
}

const component = Vue.extend({
  methods: {
    /**
     * @brief navigate table focus
     * @param  {Element|null} elt current element
     * @param  {number} row row movement (+1/-1)
     * @param  {number} [cell=0] cell movement (+1/-1)
     */ // eslint-disable-next-line complexity
    tableMoveFocus(elt, row, cell = 0) {
      const td = findParent(elt, 'td');
      const tr = findParent(td, 'tr');
      const table = findParent(tr, 'table');

      if (!td || !tr || !table) { return; }
      let rIndex = get(tr, 'rowIndex', 0) + row;
      let cIndex = get(td, 'cellIndex', 0) + cell;
      if (!row && !cell) {
        return get(table, [ 'rows', rIndex, 'cells', cIndex ]);
      }

      let ret;
      for (let tdElt = get(table, [ 'rows', rIndex, 'cells', cIndex ]);
        !ret && tdElt;
        tdElt = get(table, [ 'rows', rIndex, 'cells', cIndex ])) {

        ret = findEditElement(tdElt.getElementsByTagName('input')) ||
          findEditElement(tdElt.getElementsByTagName('select'));
        rIndex += row;
        cIndex += cell;
      }

      if (ret) {
        ret.focus();
      }
      return ret;
    }
  }
});
export default component;
