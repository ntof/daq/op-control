// @ts-check
import { BaseMatchMedia } from '@cern/base-vue';
import Vue from 'vue';

const component = (/** @type {'SM'|'MD'|'LG'|'XL'|'XXL'} */ media) => (Vue.extend({
  /**
   * @return {{ ["media"+media]: boolean }}
   */
  data() { return { ["media" + media]: false }; },
  mounted() {
    this.$options["mm" + media] = new BaseMatchMedia(BaseMatchMedia[media],
      (match) => Vue.set(this, "media" + media, match));
  },
  beforeDestroy() {
    this.$options["mm" + media].close();
  }
}));
export default component;
