// @ts-check
import { DicXmlCmd } from '@ntof/redim-client';
import { UrlUtilitiesMixin, errorPrefix } from '../utilities';
import Vue from 'vue';

const wrapError = errorPrefix.bind(null, 'Alarms: ');

/**
 * @typedef {V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */
const component = Vue.extend({
  mixins: [ UrlUtilitiesMixin ],
  methods: {
    /**
     * @this {Instance}
     * @param {string} cmd
     * @param {string} ident
     * @param {string} dns
     * @return {Promise<void>}
     */
    alarmCmd(cmd, ident, dns) {
      return DicXmlCmd.invoke('Alarms',
        { command: { '$': { name: cmd, ident: ident } } },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    }
  }
});
export default component;
