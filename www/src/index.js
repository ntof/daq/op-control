// @ts-check

import './public-path';

import Vue from 'vue';
import store from './store';
import VueRouter from 'vue-router';
// @ts-ignore
import App from './App';
import router from './router';
import '../static/css/style.css';
import BaseVue from '@cern/base-vue';

import VueUtils from './VueUtils';

Vue.use(/** @type Vue.PluginObject<any> */(VueUtils));
Vue.use(VueRouter);
// @ts-ignore we need to export install function in base-vue index.d.ts
Vue.use(BaseVue, { auth: true });

export default new Vue({
  el: '#dashboard',
  store,
  router,
  data: {
    keyHints: false
  },
  render: (h) => h(App)
});
