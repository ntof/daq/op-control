// @ts-check
import Vue from "vue";
import SideBarContent from './Sidebar/SideBarContent.vue';
import { VERSION } from '../Consts';

/**
 * @typedef {{
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>>} Instance
 */
const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'Dashboard',
  components: { SideBarContent },
  props: {
    dns: { type: String, default: '' }
  },
  /**
   * @return {{ VERSION: string }}
   */
  data() { return { VERSION }; }
});
export default component;
