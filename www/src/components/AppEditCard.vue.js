// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin  } from '@cern/base-vue';

/**
 * @typedef {V.Instance<typeof component> &
 *  V.Instance<ReturnType<KeyboardEventMixin>>} Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'EditCard',
  mixins: [
    KeyboardEventMixin({ local: true, checkOnInputs: true })
  ],
  props: {
    title: { type: String, default: '' },
    canEdit: { type: Boolean, default: true }
  },
  /**
   * @returns {{
   *   inEdit: boolean
   * }}
   */
  data() { return { inEdit: false }; },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ]))
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /** @param {boolean} value */
    setEdit(value) {
      this.inEdit = value;
    }
  }
});
export default component;
