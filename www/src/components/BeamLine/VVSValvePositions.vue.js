// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';
import { assign, endsWith, filter, findKey, first, forEach, get,
  isEmpty, keys, map, padStart, reverse } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import { DicDns, DicXmlDataSet } from '@ntof/redim-client';
import { UrlUtilitiesMixin } from '../../utilities';

/**
  * @typedef {V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

class VVSData {
  /**
   * @param {string} name
   */
  constructor(name) {
    this.name = name;
    /** @type {number | null} */
    this.state = null;
    /** @type {number | null} */
    this.position = null;
  }
}

const StateBits = {
  OPEN: 0x02,
  CLOSED: 0x04
};

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'VVSValvePositions',
  mixins: [
    UrlUtilitiesMixin
  ],
  /**
   * @returns {{
    *   clients: Array<DicXmlDataSet>
    *   data: {[key: string]: VVSData}
    *   loading: boolean
    * }}
    */
  data() { return { clients: [], data: {}, loading: true }; },
  computed: {
    ...mapState([ 'dns' ])
  },
  watch: {
    dns() {
      this.release();
      this.monitor();
    }
  },
  /** @this {Instance} */
  mounted() {
    if (isEmpty(this.dns)) { return; }
    this.monitor();
  },
  /** @this {Instance} */
  beforeDestroy() {
    this.release();
  },
  methods: {
    /** @this {Instance} */
    release() {
      forEach(this.clients, (client) => client.close());
      // @ts-ignore reset data
      assign(this, this.$options.data());
    },
    /** @this {Instance} */
    async monitor() {
      this.loading = true;
      const services = await DicDns.serviceInfo('*.VVS*',
        { proxy: this.getProxy() }, this.dnsUrl(this.dns))
      .then((res) => filter(keys(res), (svc) => endsWith(svc, '.State')));

      this.clients = map(services, (svc) => {
        // @ts-ignore
        const client =  new DicXmlDataSet(svc,
          { proxy: this.getProxy() }, this.dnsUrl(this.dns));
        client.promise().catch((err) => logger.error(err));
        client.on('value', (value) => this.onValue(value, svc));
        return client;
      });
      this.loading = false;
    },
    /**
     * @this {Instance}
     * @param {any} value
     * @param {any} service
     */
    onValue(value, service) {
      // service example: Valves/TOF.VVS0.State or Valves/TOF.VVS0 for position
      // also could happen that "Valves/" is not there
      // Remove "Valves/"
      service = service.replace(/^Valves\//, '');
      let vvsName = service;
      if (endsWith(vvsName, '.State')) {
        vvsName = vvsName.slice(0, -6);
      }
      const vvs = get(this.data, [ vvsName ]) || new VVSData(vvsName);
      const val = first(value).value;

      // TODO: do state value to string here.
      vvs.state = val;
      // @ts-ignore
      Vue.set(this.data, [ vvsName ], vvs);
    },
    /**
     * @this {Instance}
     * @param {number} value
     * @return {string}
     */
    getStateFromBits(value) {
      const status = findKey(StateBits, (bit) => ((value & bit) === bit));
      return !status ? "UNKNOWN" : status;
    },
    /**
     * @this {Instance}
     * @param {number} value
     * @return {boolean}
     */
    isStateValid(value) {
      return (value % 2) === 1; // Bit0 = 1 -> Valid
    },
    /**
     * @this {Instance}
     * @param {number} value
     * @return {string}
     */
    fillTitle(value) {
      if (!value) { return ''; }
      let bStr = padStart(value.toString(2), 3, '0');
      bStr = reverse(bStr.split("")).join("");
      return `Bit0 [Validity] = ${bStr[0]}\n` +
        `Bit1 [${keys(StateBits)[0]}] = ${bStr[1]}\n` +
        `Bit2 [${keys(StateBits)[1]}] = ${bStr[2]}`;
    }
  }
});
export default component;
