// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';
import VacuumAdministrationLink from './VacuumAdministrationLink.vue';
import VGRVacuumPressure from './VGRVacuumPressure.vue';
import VPPVacuumPumpState from './VPPVacuumPumpState.vue';
import VVSValvePositions from './VVSValvePositions.vue';
/**
  * @typedef {V.Instance<typeof component>} Instance
    */

const component = Vue.extend({
  name: 'BeamLine',
  components: { VacuumAdministrationLink, VGRVacuumPressure,
    VPPVacuumPumpState, VVSValvePositions },
  computed: {
    ...mapState([ 'dns' ])
  }
});
export default component;
