// @ts-check

import Vue from "vue";
/**
  * @typedef {V.Instance<typeof component>} Instance
    */

const component = Vue.extend({
  name: 'VacuumAdministrationLink'
});
export default component;
