// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';
import { assign, first, forEach, get, isEmpty,
  keys, map, padStart, reverse } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import { DicDns, DicXmlDataSet } from '@ntof/redim-client';
import { UrlUtilitiesMixin } from '../../utilities';

/**
  * @typedef {V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const StateBits = {
  ON: 0x02,
  OFF: 0x04
};

class VGRData {
  /**
   * @param {string} name
   */
  constructor(name) {
    this.name = name;
    /** @type {number | null} */
    this.state = null;
    /** @type {number | null} */
    this.pressure = null;
  }
}

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'VGRVacuumPressure',
  mixins: [
    UrlUtilitiesMixin
  ],
  /**
   * @returns {{
    *   clients: Array<DicXmlDataSet>
    *   data: {[key: string]: VGRData}
    *   loading: boolean
    * }}
    */
  data() { return { clients: [], data: {}, loading: true }; },
  computed: {
    ...mapState([ 'dns' ])
  },
  watch: {
    dns() {
      this.release();
      this.monitor();
    }
  },
  /** @this {Instance} */
  mounted() {
    if (isEmpty(this.dns)) { return; }
    this.monitor();
  },
  /** @this {Instance} */
  beforeDestroy() {
    this.release();
  },
  methods: {
    /** @this {Instance} */
    release() {
      forEach(this.clients, (client) => client.close());
      // @ts-ignore reset data
      assign(this, this.$options.data());
    },
    /** @this {Instance} */
    async monitor() {
      this.loading = true;
      const services = await DicDns.serviceInfo('*_VGR*',
        { proxy: this.getProxy() }, this.dnsUrl(this.dns))
      .then((res) => keys(res));

      this.clients = map(services, (svc) => {
        // @ts-ignore
        const client =  new DicXmlDataSet(svc,
          { proxy: this.getProxy() }, this.dnsUrl(this.dns));
        client.promise().catch((err) => logger.error(err));
        client.on('value', (value) => this.onValue(value, svc));
        return client;
      });
      this.loading = false;
    },
    /**
     * @this {Instance}
     * @param {any} value
     * @param {any} service
     */
    onValue(value, service) {
      // service example: TOF_VGR0.State or TOF_VGR0.PR
      const [ vgrName, svcType ] = service.split(".");
      const vgr = get(this.data, vgrName, new VGRData(vgrName));
      const val = first(value).value;
      if (svcType === "State") {
        // TODO: do state value to string here.
        vgr.state = val;
      }
      else if (svcType === "PR") {
        vgr.pressure = val;
      }
      else {
        return;
      }
      Vue.set(this.data, vgrName, vgr);
    },
    /**
     * @this {Instance}
     * @param {number} value
     * @return {string}
     */
    getStateFromBits(value) {
      let status = '';
      forEach(StateBits, (bit, key) => {
        if ((value & bit) === bit) { status = key; }
      });
      if (isEmpty(status)) { status = "UNKNOWN"; }
      return status;
    },
    /**
     * @this {Instance}
     * @param {number} value
     * @return {boolean}
     */
    isStateValid(value) {
      return (value % 2) === 1; // Bit0 = 1 -> Valid
    },
    /**
     * @this {Instance}
     * @param {number} value
     * @return {string}
     */
    fillTitle(value) {
      if (!value) { return ''; }
      let bStr = padStart(value.toString(2), 3, '0');
      bStr = reverse(bStr.split("")).join("");
      return `Bit0 [Validity] = ${bStr[0]}\n` +
        `Bit1 [${keys(StateBits)[0]}] = ${bStr[1]}\n` +
        `Bit2 [${keys(StateBits)[1]}] = ${bStr[2]}`;
    }
  }
});
export default component;
