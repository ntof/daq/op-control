// @ts-check
import Vue from "vue";

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  name: "SideBarBadge",
  props: {
    errors: { type: Number, default: 0 },
    warnings: { type: Number, default: 0 },
    loading: { type: Boolean, default: false }
  }
});
export default component;
