// @ts-check

import { get, isEmpty, keyBy, noop, omit, pick, size } from 'lodash';
import Vue from "vue";
import * as Consts from '../../Consts';
import { UrlUtilitiesMixin } from '../../utilities';
import SideBarBadge from './SideBarBadge.vue';
import AlarmStatsBadge from './AlarmBadge.vue';

/**
 * @typedef {{
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>>&
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const options = {
  /** @type  {{[index: string]: any} | null}  */
  areaMap: (keyBy(Consts.AreaList, 'dns'))
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'Sidebar',
  ...options,
  components: { AlarmStatsBadge, SideBarBadge },
  mixins: [ UrlUtilitiesMixin ],
  props: {
    dns: { type: String, default: '' }
  },
  /**
   * @return {{
   *  dnsInput: string
   *}}
   */
  data() { return { dnsInput: '' }; },
  computed: {
    /**
     * @this {Instance}
     * @return {string}
     */
    serverName() {
      if (this.dnsInput === '') {
        return "Select server";
      }
      return get(this.$options.areaMap, [ this.dnsInput, 'name' ], this.dnsInput);
    },
    /**
     * @this {Instance}
     * @return {Array<{path: string, component?: any}>}
     */
    routes() {
      return get(this.$router, [ 'options', 'routes', 0, 'children' ]);
    },
    /**
     * @this {Instance}
     * @return {string?=}
     */
    routeName() { return this.$route.name; },
    /** @return {boolean} */
    hasDns() { return !isEmpty(this.dns); }
  },
  watch: {
    dns(value /*: string */) {
      this.dnsInput = value;
    }
  },
  /** @this {Instance} */
  mounted() {
    this.dnsInput = this.dns;
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} value
     */
    selectDns(value /*: ?string */) {
      value = value || this.dnsInput;
      var query = omit(this.$route.query, [ 'dns' ]);
      if (!isEmpty(value)) {
        query.dns = value;
      }
      this.$router.push({ query }).catch(noop);
    },
    /**
     * @this {Instance}
     * @param {string} name
     */
    selectView(name /*: string */) {
      /* preserve only dns */
      const query = pick(this.$route.query, [ 'dns' ]);
      this.$router.push({ path: '/' + name, query }).catch(noop);
    },
    size(value) {
      return size(value);
    }
  }
});
export default component;
