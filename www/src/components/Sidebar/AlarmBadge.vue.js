// @ts-check
import { sum } from 'lodash';
import Vue from "vue";
import { mapState } from "vuex";
import SideBarBadge from './SideBarBadge.vue';

/**
 * @typedef {import('../../interfaces/types').AlarmServer.Stats} Stats
 *
 * @typedef {{
 *   stats(): Stats | null,
 *   fetchStamp(): number | null,
 *   loading(): boolean
 * }} AlarmStateMap
 */

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  name: "AlarmBadge",
  components: { SideBarBadge },
  computed: {
    .../** @type AlarmStateMap */(mapState('alarms', [ 'stats', 'fetchStamp', 'loading' ])),
    /**
     * @this {Instance}
     * @returns {number}
     */
    criticalAndErrorAlarms() {
      if (this.stats) {
        return sum([ this.stats.critical, this.stats.error ]);
      }
      return 0;
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    warningAlarms() {
      if (this.stats) {
        return this.stats.warning;
      }
      return 0;
    }
  }
});
export default component;
