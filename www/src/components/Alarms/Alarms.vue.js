// @ts-check

import Vue from 'vue';
import { get, indexOf, isEmpty, sum, values, xor } from 'lodash';
import MatchMedia from '../BaseMatchMedia';
import { mapState } from 'vuex';
import { UrlUtilitiesMixin } from '../../utilities';
import AlarmsCmdMixin from '../../mixins/AlarmsCmdMixin';
import { sources } from "../../store";
import { BaseLogger as logger } from '@cern/base-vue';
import EditCard from '../AppEditCard.vue';

/**
 * @typedef {{
 *   $and: Array<any>,
 *   $or: Array<any>
 * }} MongoDBFilter
 */

/** @type Object<string, {val: number, badgeClass: string}> */
const SeverityUtilityMap = {
  NONE: { val: 0, badgeClass: "dark" },
  INFO: { val: 1, badgeClass: "info" },
  WARNING: { val: 2, badgeClass: "warning" },
  ERROR: { val: 3, badgeClass: "danger" },
  CRITICAL: { val: 4, badgeClass: "danger active" }
};

const SortDir = {
  ASC: 'asc',
  DESC: 'desc'
};

/**
 * @typedef {V.Instance<typeof component> &
 *   V.Instance<typeof AlarmsCmdMixin> &
 *   V.Instance<typeof UrlUtilitiesMixin> } Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'Alarms',
  components: { EditCard },
  mixins: [
    AlarmsCmdMixin,
    UrlUtilitiesMixin
  ],
  /**
   * @returns {{
   *  media?: ?MatchMedia,
   *  searchSeverity: Array<number>,
   *  searchMasked: boolean,
   *  currentSort: string,
   *  isSendingCommand: boolean,
   *  regExpFilter: null,
   *  currentSortDir: string,
   *  searchDisabled: boolean,
   *  isScreenLG: boolean
   *  }}
   */
  data() {
    return {
      media: null, isScreenLG: false, isSendingCommand: false,
      currentSort: 'date', currentSortDir: SortDir.ASC,
      searchSeverity: [
        SeverityUtilityMap.CRITICAL.val,
        SeverityUtilityMap.ERROR.val,
        SeverityUtilityMap.WARNING.val
      ],
      searchDisabled: false, searchMasked: false, regExpFilter: null
    };
  },
  computed: {
    ...mapState([ 'dns' ]),
    ...mapState('alarms', [ 'filters', 'alarms', 'stats', 'loading' ]),
    /**
     * @returns {number}
     */
    sumAllStats() {
      return sum(values(this.stats));
    }
  },
  watch: {
    /** @this {Instance} */
    regExpFilter() { this.updateWorker(); }
  },
  /** @this {Instance} */
  beforeDestroy() {
    /** @type {MatchMedia} */ (this.media).close();
  },
  /** @this {Instance} */
  mounted() {
    this.media = new MatchMedia(MatchMedia.LG,
      (value) => { this.isScreenLG = value; });
    this.updateWorker();

    // Connect the the worker
    sources.alarms.connect();
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} field
     */
    setSorting(field) {
      if (this.currentSort === field) {
        this.currentSortDir = (this.currentSortDir === SortDir.ASC) ?
          SortDir.DESC : SortDir.ASC;
      }
      else {
        this.currentSort = field;
        this.currentSortDir = SortDir.ASC;
      }
      this.updateWorker();
    },
    /**
     * @this {Instance}
     * @param {string} field
     */
    getSortingClass(field) {
      if (this.currentSort !== field) {
        return [ "fa-sort", "i-deactivated" ];
      }
      else if (this.currentSortDir === SortDir.ASC) {
        return [ "fa-sort-down" ];
      }
      return [ "fa-sort-up" ];
    },
    /**
     * @this {Instance}
     * @param {string} severityStr
     */
    getButtonFilterClass(severityStr) {
      if (indexOf(this.searchSeverity,
        SeverityUtilityMap[severityStr].val) !== -1) {
        return [ "btn-" + SeverityUtilityMap[severityStr].badgeClass ];
      }
      else {
        return [ "btn-light", "text-muted" ];
      }
    },
    /** @this {Instance} */
    toggleMaskedFilter() {
      this.searchMasked = !this.searchMasked;
      this.searchDisabled = false;
      this.updateWorker();
    },
    /** @this {Instance} */
    toggleDisabledFilter() {
      this.searchDisabled = !this.searchDisabled;
      this.searchMasked = false;
      this.updateWorker();
    },
    /**
     * @this {Instance}
     * @param {string} severityStr
     */
    toggleSeverityFilter(severityStr /*: string */) {
      // using xor to toggle item in the array
      this.searchSeverity = xor(this.searchSeverity, [ SeverityUtilityMap[severityStr].val ]);
      this.updateWorker();
    },
    /**
     * @this {Instance}
     * @param {string} severityStr
     */
    getSeverityBadgeClass(severityStr /*: string */) {
      return "badge-" + get(SeverityUtilityMap, [ severityStr, "badgeClass" ], "light");
    },
    /** @this {Instance} */
    updateWorker() {
      const mfilter =  /** @type MongoDBFilter */({ $and: [
        { severity: { $in: this.searchSeverity } }
      ] });
      mfilter.$and.push({ masked: { $eq: this.searchMasked } });
      mfilter.$and.push({ disabled: { $eq: this.searchDisabled } });

      if (!isEmpty(this.regExpFilter)) {
        const rf = {
          $or: [
            { system: { $regex: this.regExpFilter } },
            { ident: { $regex: this.regExpFilter } },
            { message: { $regex: this.regExpFilter } }
          ]
        };
        mfilter.$and.push(rf);
      }

      const filters = {
        offset: 0,
        limit: 1000, // first 1000 alarms only
        search: mfilter,
        orderBy: this.currentSort +
          ((this.currentSortDir === SortDir.ASC) ? ":asc" : ":desc")
      };

      this.$store.commit(`alarms/filters`, filters);
    },
    /**
     * @this {Instance}
     * @param {function(string, string, string): Promise<void>} cmd
     * @param {any} args
     */
    wrapCmd(cmd /*: function */, ...args /*: any */) {
      this.isSendingCommand = true;
      // @ts-ignore
      cmd.call(this, ...args)
      .catch((/** @type any */err) => logger.error(err))
      .finally(() => {
        this.isSendingCommand = false;
      });
    },
    /**
     * @this {Instance}
     * @param {string} cmd
     * @param {string} ident
     */
    changeAlarm(cmd /*: string */, ident /*: string */) {
      this.wrapCmd(this.alarmCmd, cmd, ident, this.dns);
    }
  }
});
export default component;
