// @ts-check
import Vue from "vue";

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  name: 'DetectorInfo',
  props: {
    container: { type: Object, default: () => ({}) },
    detector: { type: Object, default: () => ({}) }
  }
});
export default component;
