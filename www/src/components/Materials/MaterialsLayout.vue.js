// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';
import { invoke } from 'lodash';
import FilterStation from './FilterStation.vue';
import MaterialSetup from './MaterialSetupCard.vue';
import DetectorSetup from './DetectorSetupCard.vue';

import EACSInfoSource from '../../store/sources/EACSInfoSource';

/**
  * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, any>>} Instance
  */

const options = {
  /** @type  EACSInfoSource | null */
  source: null
};

const component = /** @type {V.Constructor<typeof options, any>} */ (Vue).extend({
  name: 'MaterialsLayout',
  ...options,
  components: { FilterStation, MaterialSetup, DetectorSetup },
  computed: {
    ...mapState([ 'dns' ]),
    ...mapState('eacs', [ 'info' ])
  },
  /** @this {Instance} */
  mounted() {
    this.$options.source = new EACSInfoSource(this.$store);
    this.$options.source.connect();
  },
  /** @this {Instance} */
  beforeDestroy() {
    invoke(this.$options.source, 'destroy');
  }
});
export default component;
