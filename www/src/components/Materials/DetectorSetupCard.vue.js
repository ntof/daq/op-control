// @ts-check

import Vue from "vue";
import { get } from 'lodash';
import { mapState } from 'vuex';

import SetupInfo from './DetectorSetupInfo.vue';
import EditCard from '../AppEditCard.vue';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const component = Vue.extend({
  name: 'DetectorSetupCard',
  components: { EditCard, SetupInfo },
  /**
   * @returns {{
   *   inEdit: boolean
   * }}
   */
  data() {
    return { inEdit: false };
  },
  computed: {
    ...mapState('eacs/runInfo', [ 'detectorsSetupId' ]),
    ...mapState('eacs/runConfig', {
      nextDetectorsSetupId: 'detectorsSetupId'
    }),
    ...mapState('db/materials', {
      currentDetectorsSetup: (state) => get(state, 'current.detectorsSetup'),
      nextDetectorsSetup: (state) => get(state, 'next.detectorsSetup')
    }),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    showNext() {
      return this.nextDetectorsSetupId !== this.detectorsSetupId;
    }
  }
});
export default component;
