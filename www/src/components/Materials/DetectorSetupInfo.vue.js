// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';
import { get, size } from 'lodash';
import { BaseParamBase as BaseParam } from '@cern/base-vue';
import DetectorInfo from './DetectorSetupDetectorInfo.vue';

/**
  * @typedef {V.Instance<typeof component>} Instance
  */

const component = Vue.extend({
  name: 'DetectorSetupInfo',
  components: { BaseParam, DetectorInfo },
  props: {
    detectorsSetupId: { type: Number, default: -1 },
    detectorsSetup: { type: Array, default: () => [] }
  },
  computed: {
    ...mapState('db/setups', { dbDetectorsSetup: 'detectors' }),
    ...mapState('db/materials', { dbDetectors: 'detectors', dbContainers: 'containers' }),
    /**
     * @this {Instance}
     * @returns {any}
     */
    info() { return get(this.dbDetectorsSetup, this.detectorsSetupId, {}); }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {Array<string>} path
     * @returns {any}
     */
    getDbDetectors(path /*: Array<string> */) {
      return get(this.dbDetectors, path);
    },
    /**
     * @this {Instance}
     * @param {Array<string>} path
     * @returns {any}
     */
    getDbContainers(path /*: Array<string> */) {
      return get(this.dbContainers, path);
    },
    /**
     * @this {Instance}
     * @param {any} data
     * @returns {number}
     */
    getSize(data /*: any */) {
      return size(data);
    }
  }
});
export default component;
