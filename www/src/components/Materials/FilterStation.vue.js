// @ts-check

import Vue from "vue";
import { mapGetters, mapState } from 'vuex';
import { find, get, invert, invoke } from 'lodash';
import FilterStationSource from '../../store/sources/FilterStationSource';
import { FilterPosition } from '../../interfaces/filterStation';
import EditCard from '../AppEditCard.vue';

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, any>>} Instance
 */

const options = {
  /** @type  FilterStationSource | null */
  source: null,
  /** @type  {{[key: number]: string}} */
  filterPosName: invert(FilterPosition)
};

const component = /** @type {V.Constructor<typeof options, any>} */ (Vue).extend({
  name: 'FilterStation',
  ...options,
  components: { EditCard },
  data: () => ({ nextFilters: null }),
  computed: {
    ...mapState('filterStation', [ 'airPressure', 'filters' ]),
    ...mapGetters('db/materials', [ 'currentMaterials', 'nextMaterials' ]),
    ...mapState('db/materials', {
      dbFilters: 'filters'
    })
    // ...mapState('db/materials', [ 'filters', 'next' ])
  },
  /** @this {Instance} */
  mounted() {
    this.$options.source = new FilterStationSource(this.$store);
    this.$options.source.connect();
  },
  /** @this {Instance} */
  beforeDestroy() {
    invoke(this.$options.source, 'destroy');
  },
  methods: {
    /**
     * @this {Instance}
     * @param {number} state
     */
    stateName(state /*: number */) {
      return get(this.$options.filterPosName, state);
    },
    /**
     * @this {Instance}
     * @param {number} state
     */
    stateClass(state /*: number */) {
      if (state === FilterPosition.IN || state === FilterPosition.IN_INTERLOCKED) {
        return [ "badge-primary" ];
      }
      else {
        return [ "badge-secondary" ];
      }
    },
    /**
     * @this {Instance}
     * @param {number} idx
     */
    filterName(idx /*: number */) {
      const matSetup = find(get(this.currentMaterials, 'filters'),
        { position: idx + 1 });
      if (matSetup) {
        return get(this.dbFilters, [ matSetup.materialId, 'title' ], '');
      }
      return '';
    },
    /**
     * @this {Instance}
     * @param {number} idx
     */
    nextState(idx /*: number */) {
      const matSetup = find(get(this.nextMaterials, 'filters'),
        { position: idx + 1 });
      return (matSetup && matSetup.status) ? FilterPosition.IN : FilterPosition.OUT;
    }
  }
});
export default component;
