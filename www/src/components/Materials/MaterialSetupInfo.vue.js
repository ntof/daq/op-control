// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';
import { get, size } from 'lodash';
import { BaseParamBase as BaseParam } from '@cern/base-vue';
import Material from './MaterialSetupMaterialInfo.vue';

/**
  * @typedef {V.Instance<typeof component>} Instance
  */

const component = Vue.extend({
  name: 'MaterialSetupInfo',
  components: { BaseParam, Material },
  props: {
    materialsSetupId: { type: Number, default: -1 },
    materials: { type: Object, default: null }
  },
  computed: {
    ...mapState('db/setups', { dbMaterialsSetup: 'materials' }),
    ...mapState('db/materials', {
      dbFilters: 'filters',   dbSamples: 'samples', dbSources: 'sources' }),
    /**
     * @this {Instance}
     * @returns {any}
     */
    materialsSetup() { return get(this.dbMaterialsSetup, this.materialsSetupId, {}); }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} db
     * @param {Array<string>} path
     * @returns {any}
     */
    get(db /*: string */, path /*: Array<string> */) {
      /** @type {any} */
      const src = get(this, db);
      return get(src, path);
    },
    /**
     * @this {Instance}
     * @param {any} data
     * @returns {number}
     */
    getSize(data /*: any */) {
      return size(data);
    }
  }
});
export default component;
