// @ts-check

import Vue from "vue";
import { mapGetters, mapState } from 'vuex';
import EditCard from '../AppEditCard.vue';
import SetupInfo from './MaterialSetupInfo.vue';

/**
  * @typedef {V.Instance<typeof component>} Instance
  */
const component = Vue.extend({
  name: 'MaterialSetupCard',
  components: { EditCard, SetupInfo },
  computed: {
    ...mapState('eacs/runInfo', [ 'materialsSetupId' ]),
    ...mapState('eacs/runConfig', {
      nextMaterialsSetupId: 'materialsSetupId'
    }),
    ...mapGetters('db/materials', [ 'currentMaterials', 'nextMaterials' ]),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    showNext() {
      return this.nextMaterialsSetupId !== this.materialsSetupId;
    }
  }
});
export default component;
