// @ts-check
import Vue from "vue";

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  name: 'MaterialInfo',
  props: {
    title: { type: String, default: '' },
    description: { type: String, default: '' }
  },
  computed: {
    /**
     * @this {Instance}
     * @returns {string}
     */
    prepared() {
      // FIXME: old database has escaped html content
      return (this.description || "")
      .replace(/&lt;/g, '<')
      .replace(/&gt;/g, '>')
      .replace(/&amp;/g, '&')
      .replace(/&apos;/g, "'")
      .replace(/&quot;/g, '"');
    }
  }
});
export default component;
