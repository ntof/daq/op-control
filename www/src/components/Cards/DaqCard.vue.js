// @ts-check
import Vue from "vue";
import d from 'debug';
import { every, forEach, get, map, some, toNumber } from 'lodash';
import { mapState } from 'vuex';
import Channel from './Channel.vue';

import { StoreWatchMixin } from '../../utilities';
import { CardLimits } from '../../Consts';
import { getDaqUiState } from './Daq.vue.js';

const debug = d('daq:card');

/**
 * @typedef {import('../../interfaces/types').DAQ.DaqConfig.Channel} Channel
 * @typedef {import('../../interfaces/types').EACS.DaqListCardInfo} DaqListCardInfo
 * @typedef {typeof import('@cern/base-vue').BaseParamInput} Param
 * @typedef {typeof import('@cern/base-vue').BaseParamReadonly} ParamReadonly
 * @typedef {typeof import('@cern/base-vue').BaseParamList} ParamList
 */

/**
 * @typedef {{
 *   sampleSize: V.Instance<ParamReadonly>,
 *   timeWindow: V.Instance<Param>,
 *   sampleRate: V.Instance<ParamList>,
 *   delay: V.Instance<Param>,
  *  Channels: Array<V.Instance<typeof Channel>>,
  * }} Refs
  * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> &
  *  V.Instance<typeof StoreWatchMixin>} Instance
  */

const options = {
  /** @type  function | null */
  storeWatch: null
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'DaqCard',
  ...options,
  components: { Channel },
  mixins: [ StoreWatchMixin ],
  props: {
    daq: { type: Object, default: null }, // Eacs$DaqListInfo
    card: { type: Object, default: null }, // Eacs$DaqListCardInfo
    source: { type: Object, default: null }, // DaqConfigSource
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @returns {{
   *   sampleRate: number ,
   *   sampleSize: number ,
   *   delay: number ,
   *   maxEditTimeWindow: number,
   *   isLoading: boolean,
   *   isUsed: boolean
   * }}
   */
  data() {
    return {
      sampleRate: 0,
      sampleSize: 0,
      delay: 0,
      maxEditTimeWindow: 0,
      isLoading: true,
      isUsed: false
    };
  },
  computed: {
    .../** @type {{ acqExpanded(): string|null, zspExpanded(): string|null }} */(mapState('ui', {
      acqExpanded: getDaqUiState('acqExpanded'),
      zspExpanded: getDaqUiState('zspExpanded')
    })),
    /** @return {boolean} */
    showAcq() {
      return (this.acqExpanded === "all") || (!!this.acqExpanded && this.isUsed);
    },
    /** @return {boolean} */
    showZsp() {
      return (this.zspExpanded === "all") || (!!this.zspExpanded && this.isUsed);
    },
    /**
     * @this {Instance}
     * @returns {string}
     */
    cardId() { return get(this.card, 'name', 'invalid'); },
    /**
     * @this {Instance}
     * @returns {number}
     */
    timeWindow() {
      /* parseFloat will remove trailing zeroes */
      return parseFloat((this.sampleSize / this.sampleRate).toFixed(3));
    },
    /**
     * @this {Instance}
     * @returns {Array<{ value: number, text: number }>}
     */
    SampleRateValues() {
      const arr = /** @type number[] */ get(CardLimits, [ this.card.type, 'SampleRateLimits', 'values' ]);
      return map(arr, function(el) { return { value: el, text: el }; });
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    minSampleSize() {
      return get(CardLimits, [ this.card.type, 'SampleSizeLimits', 'min' ]);
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    maxSampleSize() {
      return get(CardLimits, [ this.card.type, 'SampleSizeLimits', 'max' ]);
    }
  },
  /** @this {Instance} */
  mounted() {
    this.$options.storeWatch = this.storeWatch(
      [ 'eacs', 'daq', this.daq.name, 'channels', this.card.serialNumber + ':0' ],
      (/** @type Channel */ config) => {
        this.sampleRate = config.sampleRate;
        this.sampleSize = config.sampleSize;
        this.delay = config.delay;
      }
    );
  },
  /** @this {Instance} */
  beforeDestroy() {
    if (this.$options.storeWatch) {
      this.$options.storeWatch();
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} name
     * @param {any} value
     */
    pushEditValue(name /*: string */, value /*: any */) {
      /* card level parameters apply to all cards */
      for (let channel = 0; channel < this.card.channelsCount; ++channel) {
        this.$store.commit('eacs/daq/' + this.daq.name + '/editChannel',
          { id: `${this.card.serialNumber}:${channel}`, name, value });
      }
    },
    /** @this {Instance} */
    onChannelLoaded() {
      if (every(this.$refs.Channels, { 'isLoading': false })) {
        this.isLoading = false;
        this.$emit('loaded');
        debug('Card loaded %s', this.card.sn);
        this.isUsed = some(this.$refs.Channels, 'enabled');
      }
    },
    /** @this {Instance} */
    updateEditSampleSize() {
      this.$refs.sampleSize.editValue = Math.ceil(
        toNumber(this.$refs.timeWindow.editValue) *
        toNumber(this.$refs.sampleRate.editValue)).toString();
      const sampleRate = toNumber(this.$refs.sampleRate.editValue);
      this.maxEditTimeWindow = (sampleRate !== 0) ?
        parseFloat((this.maxSampleSize / sampleRate).toFixed(3)) : 0;

      this.$refs.sampleRate.checkValidity();
      this.$refs.timeWindow.checkValidity();
    },
    /** @this {Instance} */
    async configure() {
      let isUsed = false;
      forEach(this.$refs.Channels, (chan) => chan.setSending(true));
      for (const chan of this.$refs.Channels) {
        await (chan).configure();
        isUsed = isUsed || chan.enabled;
      }
      this.isUsed = isUsed;
    }
  }
});
export default component;
