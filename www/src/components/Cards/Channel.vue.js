// @ts-check

import Vue from "vue";
import {
  assign, constant, get, isFunction, map, mapValues, toNumber, toString
} from 'lodash';
import { mapState } from 'vuex';
import d from 'debug';

import { StoreWatchMixin } from '../../utilities';
import { ChannelParamsMap as ParamsMap } from '../../interfaces/daq';
import { CardLimits } from '../../Consts';
import { getDaqUiState } from './Daq.vue.js';

var debug = d('daq:chan');

/**
 * @typedef {import('../../interfaces/types').DAQ.DaqConfig.Channel} Channel
 * @typedef {import('../../interfaces/types').EACS.DaqListCardInfo} DaqListCardInfo
 * @typedef {typeof import('@cern/base-vue').BaseParamInput} Param
 * @typedef {typeof import('@cern/base-vue').BaseParamReadonly} ParamReadonly
 * @typedef {typeof import('@cern/base-vue').BaseParamList} ParamList
 */

/**
 * @typedef {{
 *  detectorType: V.Instance<BaseVue.Param>,
 *  detectorId: V.Instance<Param>,
 *  fullScale: V.Instance<Param>,
 *  offset: V.Instance<ParamReadonly>,
 *  lowerLimit: V.Instance<Param>,
 *  impedance: V.Instance<ParamList>,
 *  thresholdSign: V.Instance<ParamList>,
 *  threshold: V.Instance<Param>,
 *  zsStart: V.Instance<Param>,
 *  preSamples: V.Instance<Param>,
 *  postSamples: V.Instance<Param>,
 *  enabled: V.Instance<Param>,
 *  isZSP: V.Instance<Param>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> &
 *  V.Instance<typeof StoreWatchMixin>} Instance
 */

const options = {
  /** @type  function | null */
  storeWatch: null
};


const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'Channel',
  ...options,
  mixins: [ StoreWatchMixin ],
  props: {
    daq: { type: Object, default: null },
    card: { type: Object, default: null }, /* Eacs$DaqListCardInfo */
    channel: { type: Number, default: null },
    source: { type: Object, default: null }, // DaqConfigSource
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @returns {{
   *   isLoading: boolean,
   *   isSending: boolean,
   *   isZSPEdit: boolean
   * } & {[p in keyof ParamsMap]: any}}
   */
  data() {
    return assign({ isLoading: true, isSending: false, isZSPEdit: false },
      mapValues(ParamsMap, constant(null)));
  },
  computed: {
    .../** @type {{ acqExpanded(): string|null, zspExpanded(): string|null }} */(mapState('ui', {
      acqExpanded: getDaqUiState('acqExpanded'),
      zspExpanded: getDaqUiState('zspExpanded')
    })),
    /** @return {boolean} */
    showAcq() {
      return !!((this.acqExpanded === "all") ||
        (this.acqExpanded && this.currentEnabled));
    },
    /** @return {boolean} */
    showZsp() {
      return !!((this.zspExpanded === "all") ||
        (this.zspExpanded && this.currentEnabled));
    },
    /**
     * @this {Instance}
     * @returns {string}
     */
    channelId() {
      return this.card.serialNumber + ':' + this.channel;
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    lowerLimit() {
      return -(this.fullScale / 2) - this.offset;
    },
    /**
     * @this {Instance}
     * @returns {Array<{ value: number, text: string }>}
     */
    ImpedanceValues() {
      const arr = get(CardLimits,
        [ this.card.type, 'ImpedanceLimits', 'values' ]);
      return map(arr, function(el) { return { value: el, text: el };});
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    minOffset() {
      return get(CardLimits, [ this.card.type, 'OffsetLimits', 'min' ]);
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    maxOffset() {
      return get(CardLimits, [ this.card.type, 'OffsetLimits', 'max' ]);
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    OffsetLimits() {
      const offset = get(CardLimits, [ this.card.type, 'OffsetLimit' ]);
      if (isFunction(offset)) {
        return offset(this.fullScale);
      }
      return offset;
    },
    /**
     * @this {Instance}
     * @return {Array<{value: number, text: string }>}
     */
    FullScaleValues() {
      return get(CardLimits, [ this.card.type, 'FullScaleLimits', 'values' ]);
    },
    /** @return {boolean} */
    currentEnabled() {
      return this.inEdit ? this.edit.enabled : this.enabled;
    },
    /** @return {number} */
    timeWindow() {
      if (!this.sampleRate) { return 0; }
      /* parseFloat will remove trailing zeroes */
      return parseFloat((this.sampleSize / this.sampleRate).toFixed(3));
    },
    /** @return {boolean} */
    isZSP() {
      return (this.zsStart || 0) < (this.timeWindow * 1e6);
    },
    /** @return {{[p in keyof ParamsMap]: any}}} */
    edit() {
      return get(this.$store, [ 'state', 'eacs', 'daq', this.daq.name, 'edit', this.channelId ], {});
    }
  },
  /** @this {Instance} */
  mounted() {
    this.$options.storeWatch = this.storeWatch(
      [ 'eacs', 'daq', this.daq.name, 'channels', this.channelId ],
      (/** @type Channel */ config) => {
        assign(this, config);
        this.isLoading = false;
        this.$emit('loaded', this.$data, this.channel);
        debug('channel update', this.channel);
      }
    );
  },
  /** @this {Instance} */
  beforeDestroy() {
    if (this.$options.storeWatch) {
      this.$options.storeWatch();
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} name
     * @param {any} value
     */
    pushEditValue(name, value) {
      this.$store.commit(`eacs/daq/${this.daq.name}/editChannel`,
        { id: this.channelId, name, value });
    },
    /** @this {Instance} */
    updateEditOffset() {
      this.$refs.offset.editValue =
        (-(toNumber(this.$refs.fullScale.editValue) / 2) -
        toNumber(this.$refs.lowerLimit.editValue)).toString();
      this.$refs.fullScale.checkValidity();
    },
    /** @this {Instance} */
    checkOffsetWarning() {
      this.$refs.offset.removeWarning('exceeded');
      const offsetLimits = this.OffsetLimits;
      if (toNumber(this.$refs.offset.editValue) > offsetLimits ||
          toNumber(this.$refs.offset.editValue) < -offsetLimits) {
        this.$refs.offset.addWarning('exceeded', 'offset should be below ' +
          offsetLimits + ' and above ' + (-offsetLimits));
      }
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setSending(value) {
      this.isSending = value;
    },
    /** @this {Instance} */
    async configure() {
      this.isSending = true;
      await this.source.chanSources[this.channelId].send();
      this.isSending = false;
    },
    /** @this {Instance} */
    onZSPEdit(value) {
      this.isZSPEdit = value;
      if (!this.inEdit) { return; }
      this.$refs.zsStart.editValue = toString(this.isZSPEdit ?
        0 : (this.timeWindow * 1e6));

      this.pushEditValue('isZSP', this.isZSPEdit);
    }
  }
});
export default component;
