// @ts-check

import Vue from "vue";
import {
  assign, constant, get, isFunction, isNil, map, mapValues, toNumber, toString
} from 'lodash';
import MatchMediaMixin from '../../mixins/BaseMatchMediaMixin';
import { StoreWatchMixin } from '../../utilities';
import { ChannelParamsMap as ParamsMap } from '../../interfaces/daq';
import { CardLimits } from '../../Consts';

/**
 * @typedef {import('../../interfaces/types').DAQ.DaqConfig.Channel} Channel
 */

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, any>> &
 *  V.Instance<typeof StoreWatchMixin>} Instance
 */

const options = {
  /** @type  function | null */
  storeWatch: null
};


const component = /** @type {V.Constructor<typeof options, any>} */ (Vue).extend({
  name: 'DaqTableChannel',
  ...options,
  mixins: [ StoreWatchMixin, MatchMediaMixin('XL') ],
  props: {
    showDisabled: { type: Boolean, default: true },
    daq: { type: Object, default: null },
    card: { type: Object, default: null },
    channel: { type: Number, default: null },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{ [p in keyof ParamsMap]: any } &
   *  { maxEditTimeWindow: number, selected: boolean, isZSPEdit: boolean }
   * }
   */
  data() {
    return assign({ maxEditTimeWindow: 0, selected: false, isZSPEdit: false },
      mapValues(ParamsMap, constant(null)));
  },
  computed: {
    /**
     * @this {Instance}
     * @return {string}
     */
    channelId() {
      return this.card.serialNumber + ':' + this.channel;
    },
    /**
     * @this {Instance}
     * @return {number}
     */
    lowerLimit() {
      return -(this.fullScale / 2) - this.offset;
    },
    /**
     * @return {number}
     */
    timeWindow() {
      if (!this.sampleRate) { return 0; }
      /* parseFloat will remove trailing zeroes */
      return parseFloat((this.sampleSize / this.sampleRate).toFixed(3));
    },
    /**
     * @this {Instance}
     * @returns {Array<{ value: number, text: number }>}
     */
    SampleRateValues() {
      const arr = /** @type number[] */ get(CardLimits, [ this.card.type, 'SampleRateLimits', 'values' ]);
      return map(arr, function(el) { return { value: el, text: el }; });
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    minSampleSize() {
      return get(CardLimits, [ this.card.type, 'SampleSizeLimits', 'min' ]);
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    maxSampleSize() {
      return get(CardLimits, [ this.card.type, 'SampleSizeLimits', 'max' ]);
    },
    /**
     * @this {Instance}
     * @returns {Array<{ value: number, text: string }>}
     */
    ImpedanceValues() /*: Array<{ value: number, text: string }> */ {
      const arr = get(CardLimits,
        [ this.card.type, 'ImpedanceLimits', 'values' ]);
      return map(arr, function(el) { return { value: el, text: el };});
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    minOffset() /*: number */ {
      return get(CardLimits, [ this.card.type, 'OffsetLimits', 'min' ]);
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    maxOffset() /*: number */ {
      return get(CardLimits, [ this.card.type, 'OffsetLimits', 'max' ]);
    },
    /**
     * @this {Instance}
     * @returns {number}
     */
    OffsetLimits() {
      const offset = get(CardLimits, [ this.card.type, 'OffsetLimit' ]);
      if (isFunction(offset)) {
        return offset(this.fullScale);
      }
      return offset;
    },
    /**
     * @this {Instance}
     * @return {Array<{value: number, text: string }>}
     */
    FullScaleValues() {
      return get(CardLimits, [ this.card.type, 'FullScaleLimits', 'values' ]);
    },
    /**
     * @return {boolean}
     */
    isZSP() {
      return (this.zsStart || 0) < (this.timeWindow * 1e6);
    }
  },
  watch: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    inEdit(value /*: boolean */) {
      if (value) {
        this.$store.commit('eacs/daq/' + this.daq.name + '/resetEdit');
        this.$store.commit('eacs/daq/' + this.daq.name + '/zsp/resetEdit');
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    this.$options.storeWatch = this.storeWatch(
      [ 'eacs', 'daq', this.daq.name, 'channels', this.channelId ],
      (/** @type Channel */ config) => assign(this, config)
    );
  },
  /** @this {Instance} */
  beforeDestroy() {
    if (this.$options.storeWatch) {
      this.$options.storeWatch();
    }
  },
  methods: {
    focusChannel() {
      const el = document.getElementById(this.channelId);
      const currentState = get(this.$store.state,
        [ 'ui', `${this.daq.name}:acqExpanded` ]);

      if (el && (isNil(currentState) ||
          (currentState === "active" && !this.enabled))) {
        this.$store.commit('ui/update', { [`${this.daq.name}:acqExpanded`]:
          ((this.enabled) ? "active" : "all")
        });
        setTimeout(() => el.scrollIntoView({ behavior: 'smooth' }), 500);
      }
      else if (el) {
        el.scrollIntoView({ behavior: 'smooth' });
      }
    },
    /**
     * @this {Instance}
     * @param {string} name
     * @param {any} value
     * @param {boolean} [emit=true]
     */
    pushValue(name, value, emit = true) {
      const evt = { id: this.channelId, name, value };
      this.$store.commit(`eacs/daq/${this.daq.name}/editChannel`, evt);
      if (emit) {
        this.$emit('push-value', evt);
      }
    },
    /**
     * @this {Instance}
     * @param {string} name
     * @param {any} value
     * @param {boolean} [emit=true]
     */
    pushCardValue(name, value, emit = true) {
      /* card level parameters apply to all cards */
      for (let channel = 0; channel < this.card.channelsCount; ++channel) {
        this.$store.commit('eacs/daq/' + this.daq.name + '/editChannel',
          { id: `${this.card.serialNumber}:${channel}`, name, value });
      }
      if (emit) {
        this.$emit('push-value', { id: this.channelId, name, value });
      }
    },
    /**
     * @this {Instance}
     * @param {string} name
     * @param {any} value
     */
    emitPushValue(name, value) {
      this.$emit('push-value', { id: this.channelId, name, value });
    },
    /** @this {Instance} */
    updateEditSampleSize() {
      this.$refs.sampleSize.editValue = Math.ceil(
        toNumber(this.$refs.timeWindow.editValue) *
        toNumber(this.$refs.sampleRate.editValue)).toString();
      const sampleRate = toNumber(this.$refs.sampleRate.editValue);
      this.maxEditTimeWindow = (sampleRate !== 0) ?
        parseFloat((this.maxSampleSize / sampleRate).toFixed(3)) : 0;

      this.$refs.sampleRate.checkValidity();
      this.$refs.timeWindow.checkValidity();
    },
    /** @this {Instance} */
    updateEditOffset() {
      this.$refs.offset.editValue =
        (-(toNumber(this.$refs.fullScale.editValue) / 2) -
        toNumber(this.$refs.lowerLimit.editValue)).toString();
      this.$refs.fullScale.checkValidity();
    },
    /** @this {Instance} */
    checkOffsetWarning() {
      this.$refs.offset.removeWarning('exceeded');
      const offsetLimits = this.OffsetLimits;
      if (toNumber(this.$refs.offset.editValue) > offsetLimits || toNumber(this.$refs.offset.editValue) < -offsetLimits) {
        this.$refs.offset.addWarning('exceeded', 'offset should be below ' + offsetLimits + ' and above ' + (-offsetLimits));
      }
    },
    /**
     * @this {Instance}
     * @param  {boolean} value
     */
    onZSPEdit(value) {
      this.isZSPEdit = value;
      if (!this.inEdit) { return; }
      // not using timeWindow editValue since ref exist only on first channel
      // (uses "-" on others)
      this.$refs.zsStart.editValue = toString(this.isZSPEdit ?
        0 : (this.timeWindow * 1e6));
      this.pushValue('isZSP', this.isZSPEdit);
    }
  }
});
export default component;
