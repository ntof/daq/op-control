// @ts-check

import Vue from "vue";
import d from 'debug';
import { filter, find, findKey, get, isEmpty, orderBy, transform } from 'lodash';
import { mapState } from 'vuex';
import {
  BaseKeepFocusMixin as KeepFocusMixin } from '@cern/base-vue';
import { ZSPMode } from '../../interfaces/daq';
import { StoreWatchMixin } from '../../utilities';
import { getDaqUiState } from './Daq.vue.js';

const debug = d('daq:zsp');

/**
 * @typedef {import('../../interfaces/types').DAQ.DaqConfig.ZSP} ZSP
 * @typedef {import('../../interfaces/types').EACS.DaqListCardInfo} DaqListCardInfo
 * @typedef {import('../../store/types').ZSPState} ZSPState
 * @typedef {typeof import('@cern/base-vue').BaseCollapsible} Collapsible
 * @typedef {typeof import('@cern/base-vue').BaseParamList} ParamList
 */

/**
 * @typedef {{
  *  collapsible: V.Instance<Collapsible>,
  *  mode: V.Instance<ParamList>
  * }} Refs
  * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> &
  *  V.Instance<typeof StoreWatchMixin> &
  *  V.Instance<typeof KeepFocusMixin>} Instance
  */

const ZSPModeMap = [
  { value: ZSPMode.INDEPENDENT, text: 'Independent' },
  { value: ZSPMode.SINGLE_MASTER, text: 'Single Master' },
  { value: ZSPMode.MASTER, text: 'Multi Master' }
];

const options = {
  /** @type  function | null */
  storeWatch: null,
  /** @type  typeof ZSPModeMap */
  modes: ZSPModeMap
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'ZeroSuppression',
  ...options,
  mixins: [ StoreWatchMixin, KeepFocusMixin ],
  props: {
    daq: { type: Object, default: null },
    source: { type: Object, default: null }, // DaqConfigSource
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @returns {{
    *   isLoading: boolean,
    *   zsp: ZSPState | null,
    *   zspEdit: ZSP | null,
    *   ZSPMode: typeof ZSPMode
    * }}
    */
  data() {
    return {
      isLoading: true,
      zsp: null,
      zspEdit: null,
      ZSPMode: ZSPMode
    };
  },
  computed: {
    .../** @type {{ zspExpanded(): string|null }} */(mapState('ui', {
      zspExpanded: getDaqUiState('zspExpanded')
    })),
    /**
     * @this {Instance}
     * @returns {ZSP | null}
     */
    activeZsp() {
      return this.inEdit ? this.zspEdit : this.zsp;
    },
    /**
     * @this {Instance}
     * @returns { string | undefined}
     */
    firstMaster() {
      // find with no arguments returns first value of object
      return findKey(get(this.inEdit ? this.zspEdit : this.zsp, 'master'));
    },
    /**
     * @this {Instance}
     * @returns { Array<{value: string, text: string}>}
     */
    masterChoiceList() {
      var ret = [ { value: '', text: '' } ];
      transform(get(this.activeZsp, 'master') || {},
        (ret, m) => ret.push(this.getChanOpt(m)), ret);
      transform(get(this.activeZsp, 'independent') || {},
        (ret, m) => ret.push(this.getChanOpt(m)), ret);
      ret = orderBy(ret, 'text');
      return ret;
    }
  },
  watch: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    inEdit(value /*: boolean */) {
      if (value) {
        this.$store.commit('eacs/daq/' + this.daq.name + '/zsp/resetEdit');
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    this.$options.storeWatch = this.storeWatch(
      [ 'eacs', 'daq', this.daq.name, 'zsp' ],
      (/** @type ZSPState */config) => {
        this.zsp = config;
        this.zspEdit = config.edit;
        this.isLoading = false;
        this.$emit('loaded');
        debug('zsp fetched', this.daq.name);
      }
    );
  },
  /** @this {Instance} */
  beforeDestroy() {
    if (this.$options.storeWatch) {
      this.$options.storeWatch();
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} chanId
     * @returns {string}
     */
    getChan(chanId /*: ChanId */) {
      return get(this.activeZsp, [ 'channels', chanId ]);
    },
    /**
     * @this {Instance}
     * @param {string} chanId
     * @returns {string}
     */
    getSlaves(chanId /*: ChanId */) {
      return get(this.activeZsp, [ 'channels', chanId, 'slave' ]);
    },
    /**
     * @this {Instance}
     * @param {string} chanId
     * @returns {string}
     */
    getChanName(chanId /*: ChanId */) { // FIXME: use edit store
      const channels = get(this.$store.state,
        [ 'eacs', 'daq', this.daq.name, this.inEdit ? 'edit' : 'channels' ]);
      var name = get(channels, [ chanId, 'detectorType' ]);
      return (name) ? name : chanId;
    },
    /**
     * @this {Instance}
     * @param {string} chanId
     * @returns {string}
     */
    getChanInfo(chanId /*: ChanId */) {
      const chan = get(this.zsp, [ 'channels', chanId ]);
      if (!chan) { return ''; }
      const cardInfo/*: Eacs$DaqListCardInfo */ =
        get(this.$store.state, [ 'eacs', 'cardList', chan.sn ]);
      return cardInfo ? `[${cardInfo.index}:${chan.channel}]` : '';
    },
    /**
     * @this {Instance}
     * @param {string} id
     * @returns {{value: string, text: string}}
     */
    getChanOpt(id /*: ChanId */) {
      return { value: id, text: `${this.getChanName(id)} ${this.getChanInfo(id)}` };
    },
    /**
     * @this {Instance}
     * @param {number} mode
     * @returns {number}
     */
    getModeText(mode /*: number */) {
      /** @type {{value: number, text: string} | undefined}  */
      const findRes = find(this.$options.modes, { value: mode });
      return /** @type number*/(get(findRes, 'text', mode));
    },
    /**
     * @this {Instance}
     * @param {string} chanId
     * @returns {Array<{value: string, text: string}>}
     */
    filteredMasterChoice(chanId /*: ChanId */) {
      return filter(this.masterChoiceList,
        (opt) => (opt.value !== chanId));
    },
    /** @this {Instance} */
    async configure() {
      debug('configuring zsp');
      await this.source.zspSource.send();
    },
    /**
     * @this {Instance}
     * @param {string} mode
     */
    setMode(mode /*: $Values<typeof ZSPMode> */) {
      this.$store.commit('eacs/daq/' + this.daq.name + '/zsp/setMode', mode);
      this.setFocusOnNextTick();
    },
    /**
     * @this {Instance}
     * @param {string} masterId
     */
    setSingleMaster(masterId /*: ChanId */) {
      this.$store.commit('eacs/daq/' + this.daq.name + '/zsp/setSingleMaster', masterId);
      this.setFocusOnNextTick();
    },
    /**
     * @this {Instance}
     * @param {string} chanId
     * @param {string} masterId
     */
    moveSlave(chanId /*: ChanId */, masterId /*: ?ChanId */) {
      this.$store.commit('eacs/daq/' + this.daq.name + '/zsp/moveChannel',
        (isEmpty(masterId)) ? { id: chanId } : { id: chanId, masterId });
      this.setFocusOnNextTick();
    }
  }
});
export default component;
