// @ts-check

import { forEach, mapValues } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import DaqTable from './DaqTable.vue';
import Daq from './Daq.vue';
import DaqConfigSource from '../../store/sources/DaqConfigSource';

/**
 * @typedef {import('../../interfaces/types').EACS.DaqListInfo} DaqListInfo
 */

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  name: 'CardsLayout',
  components: { Daq, DaqTable },
  /** @return {{ sources: { [name: string]: DaqConfigSource } }} */
  data() {
    return { sources: {} };
  },
  computed: {
    ...mapState([ 'dns' ]),
    .../** @type {{ daqList(): { [name: string]: DaqListInfo }}} */(
      mapState('eacs', [ 'daqList' ]))
  },
  watch: {
    daqList: {
      immediate: true,
      handler() {
        forEach(this.sources, (s) => s.destroy());
        this.sources = mapValues(this.daqList,
          (/** @type {DaqListInfo} */ d) => (new DaqConfigSource(true, d, this.$store)));
      }
    }
  },
  /** @this {Instance} */
  beforeDestroy() {
    forEach(this.sources, (s) => s.destroy());
  }
});
export default component;
