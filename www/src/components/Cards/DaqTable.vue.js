// @ts-check

import { get, set, toNumber, toString, transform } from 'lodash';
import Vue from "vue";
import { mapState } from 'vuex';
import DaqTableChannel from './DaqTableChannel.vue';
import { getUiState } from '../../store';
import {
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import TableMixin from '../../mixins/TableMixin';
import MatchMediaMixin from '../../mixins/BaseMatchMediaMixin';
import ImportExportDropDownTools from './ImportExportDropDownTools.vue';

/**
 * @typedef {typeof import('@cern/base-vue').BaseInput} BaseInput
 * @typedef {{
 *   mainCard: Vue
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<null, Refs>> &
 *  V.Instance<ReturnType<KeyboardEventMixin>>} Instance
 */

const component = /** @type {V.Constructor<null, Refs>} */ (Vue).extend({
  name: 'DaqTable',
  components: { DaqTableChannel, ImportExportDropDownTools },
  mixins: [
    KeyboardEventMixin({ local: true }),
    TableMixin, MatchMediaMixin('XL')
  ],
  props: {
    daqList: { type: Object, default: null },
    sources: { type: Object, default: null } // DaqConfigSource
  },
  /**
   * @return {{ inEdit: boolean, isConfiguring: boolean, isSelecting: boolean }}
   */
  data() {
    return {
      inEdit: false, isConfiguring: false,
      isSelecting: false
    };
  },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', {
      showKeyHints: (state) => { return get(state, 'showKeyHints');}
    })),
    .../** @type {{ showDisabled(): boolean }} */(mapState('ui', {
      showDisabled: getUiState('cards:showDisabledCards', true)
    })),
    /**
     * @return {{ daq:any, card:any, channel:number, id:string }[]}
     */
    chanList() {
      return transform(this.daqList, (ret, daq) => transform(daq.cards, (ret, card) => {
        for (let channel = 0; channel < card.channelsCount; ++channel) {
          ret.push({
            daq, card, channel,
            id: `${card.serialNumber}:${channel}`
          });
        }
      }, ret),
      /** @type {{ daq:any, card:any, channel:number, id:string }[]} */ ([]));
    }
  },
  watch: {
    /** @this {Instance} */
    inEdit() {
      this.chanList.forEach(
        // @ts-ignore: set silently ignores null
        (chan) => set(this.getChan(chan.id), [ 'selected' ], false));
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.onKey('ctrl-s-keydown', this.configure);
  },
  methods: {
    /**
     * @param  {string} chanId
     * @return {V.Instance<DaqTableChannel>|null}
     */
    getChan(chanId) {
      return get(this.$refs, [ chanId, 0 ]) || get(this.$refs, [ chanId ]);
    },
    /**
     * @this {Instance}
     * @param  {MouseEvent} event
     * @param  {string} chanId
     */
    onMouseSelect(event, chanId) {
      if (!this.inEdit) {
        return;
      }
      else if (event.type === 'mousedown') {
        this.isSelecting = true;
      }
      else if (!this.isSelecting) {
        return;
      }

      const chan = this.getChan(chanId);
      if (!chan) { return; }
      chan.selected = event.shiftKey ? false : true;
    },
    /**
     * @this {Instance}
     * @param  {{name:string, value: any, id: string }} args
     */
    onPushValue({ name, value, id }) {
      if (!this.inEdit || (name === 'detectorId')) { return; }

      const current = this.getChan(id);
      if (!current || !current.selected) { return; }

      this.chanList.forEach((chan) => {
        const sibling = this.getChan(chan.id);
        if (sibling && sibling.selected && (sibling !== current)) {
          const elt = /** @type {V.Instance<BaseInput>} */ (sibling.$refs[name]);
          if (elt && (elt.editValue !== value)) {
            elt.editValue = value;
          }
        }
      });
    },
    /**
     * @this {Instance}
     * @param  {string} chanId
     */
    onGenDetectorId(chanId) {
      if (!this.inEdit) { return; }
      const current = this.getChan(chanId);
      if (!current || !current.selected) { return; }

      let detectorId = toNumber(get(current.$refs, [ 'detectorId', 'editValue' ], 0));

      this.chanList.forEach((chan) => {
        const sibling = this.getChan(chan.id);
        if (sibling && sibling.selected && (sibling !== current)) {
          const elt = /** @type {V.Instance<BaseInput>} */ (sibling.$refs['detectorId']);
          if (elt) {
            elt.editValue = toString(++detectorId);
          }
        }
      });
    },
    toggleShowDisabled() {
      this.$store.commit('ui/update',
        { 'cards:showDisabledCards': !this.showDisabled });
    },
    /** @this {Instance} */
    cancel() {
      this.setEdit(false);
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value/*: any */) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent | null} event
     */
    async configure(event /*: KeyboardEvent */) {
      if (event) {
        event.preventDefault();
        if (!this.inEdit) { return; } // Keyboard Shortcut Guard
      }

      this.isConfiguring = true;
      try {
        for (const card of Object.values(this.sources)) {
          for (const chan of Object.values(card.chanSources)) {
            await chan.send();
          }
          await card.zspSource.send();
        }

        this.inEdit = false;
      }
      catch (err) {
        logger.error(err);
      }
      this.isConfiguring = false;
    }
  }
});
export default component;
