// @ts-check

import Vue from "vue";
import { every, get } from 'lodash';
import { mapState } from 'vuex';
import ZeroSuppression from './ZeroSuppression.vue';
import {
  BaseKeepFocusMixin as KeepFocusMixin,
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import DaqCard from './DaqCard.vue';

/**
 * @typedef {{
 *  mainCard: V.Instance<typeof Vue>,
 *  zsp: V.Instance<typeof ZeroSuppression>,
 *  Cards: Array<V.Instance<typeof DaqCard>>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *  V.Instance<ReturnType<KeyboardEventMixin>>} Instance
 */

/**
 * @param {string} name
 * @param {any} defaultValue
 * @returns {function(any): any}
 */
export function getDaqUiState(name, defaultValue = null) {
  return /** @this {Instance} */ function(state) {
    const prop = `${this.daq.name}:${name}`;
    if (state[prop] === undefined) {
      Vue.set(state, prop, defaultValue);
      this.$store.commit('ui/update', { [prop]: defaultValue });
    }
    return state[prop];
  };
}

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'Daq',
  components: { ZeroSuppression, DaqCard },
  mixins: [
    KeyboardEventMixin({ local: true }), KeepFocusMixin
  ],
  props: {
    daq: { type: Object, default: null },
    source: { type: Object, default: null } // DaqConfigSource
  },
  /**
   * @returns {{
   *   inEdit: boolean,
   *   cardsLoading: boolean,
   *   zspLoading: boolean
   * }}
   */
  data() {
    return { inEdit: false, cardsLoading: true, zspLoading: true };
  },
  computed: {
    .../** @type {{ acqExpanded(): string|null, zspExpanded(): string|null, showKeyHints(): boolean }} */(mapState('ui', {
      acqExpanded: getDaqUiState('acqExpanded'),
      zspExpanded: getDaqUiState('zspExpanded'),
      showKeyHints: (state) => { return get(state, 'showKeyHints');}
    })),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isLoading() { return this.cardsLoading || this.zspLoading; }
  },
  watch: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    inEdit(value /*: boolean */) {
      if (value) {
        this.$store.commit('eacs/daq/' + this.daq.name + '/resetEdit');
        this.$store.commit('eacs/daq/' + this.daq.name + '/zsp/resetEdit');
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.onKey('ctrl-s-keydown', this.configure);
  },
  methods: {
    /** @this {Instance} */
    cancel() {
      this.setEdit(false);
      this.setFocusOnNextTick();
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value/*: any */) {
      this.inEdit = value;
    },
    /** @this {Instance} */
    toggleAcq() {
      let next = null;
      if (this.acqExpanded === null) {
        next = "active";
      }
      else if (this.acqExpanded === "active") {
        next = "all";
      }
      this.$store.commit('ui/update',
        { [`${this.daq.name}:acqExpanded`]: next });
    },
    /** @this {Instance} */
    toggleZsp() {
      let next = null;
      if (this.zspExpanded === null) {
        next = "active";
      }
      else if (this.zspExpanded === "active") {
        next = "all";
      }
      this.$store.commit('ui/update',
        { [`${this.daq.name}:zspExpanded`]: next });
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent | null} event
     */
    async configure(event /*: KeyboardEvent */) {
      if (event) {
        event.preventDefault();
        if (!this.inEdit || this.isLoading) { return; } // Keyboard Shortcut Guard
      }

      try {
        for (const card of this.$refs.Cards) {
          await card.configure();
        }
        await this.$refs.zsp.configure();

        this.inEdit = false;
        this.setFocusOnNextTick();
      }
      catch (err) {
        logger.error(err);
      }
    },
    /** @this {Instance} */
    onCardLoaded() {
      if (every(this.$refs.Cards, { isLoading: false })) {
        this.cardsLoading = false;
        if (!this.zspLoading) {
          this.$emit('loaded');
        }
      }
    },
    /** @this {Instance} */
    onZSPLoaded() {
      this.zspLoading = false;
      if (!this.cardsLoading) {
        this.$emit('loaded');
      }
    }
  }
});
export default component;
