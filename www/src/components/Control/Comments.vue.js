// @ts-check

import Vue from "vue";
import { get, invoke, isEmpty, set, size } from 'lodash';
import {
  BaseKeepFocusMixin as KeepFocusMixin,
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { mapGetters, mapState } from 'vuex';
import { UrlUtilitiesMixin } from '../../utilities';
import { sources } from '../../store';

/**
 * @typedef {import('../../interfaces/types').DB.User} DBUser
 * @typedef {import('../../interfaces/types').DB.Comment} DBComment
 * @typedef {import('../../interfaces/types').EACS.RunInfo} RunInfo
 * @typedef {typeof import('@cern/base-vue').BaseMarkdownWidget} MarkdownWidget
 */

/**
 * @typedef {{
  *  comment: V.Instance<MarkdownWidget>,
  * }} Refs
  * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
  *   V.Instance<typeof UrlUtilitiesMixin> &
  *   V.Instance<ReturnType<KeyboardEventMixin>>} Instance
  */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'Comments',
  mixins: [
    KeyboardEventMixin({ local: true }),
    UrlUtilitiesMixin,
    KeepFocusMixin
  ],
  /**
   * @returns {{
    *   inEdit: boolean,
    *   canSend: boolean
    * }}
    */
  data() {
    return { inEdit: false, canSend: true };
  },
  computed: {
    ...mapState([ 'dns' ]),
    ...mapState('db', [ 'comments', 'users' ]),
    ...mapState('eacs/runInfo', [ 'runNumber' ]),
    ...mapGetters([ 'username' ]),
    ...mapState('ui', [ 'showKeyHints' ]),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isComments() { return this.comments !== null; },
    /**
     * @this {Instance}
     * @returns {number}
     */
    commentsCount() { return size(this.comments); }
  },
  watch: {
    inEdit(value) {
      if (value) {
        /* wait for the Collapsible animation */
        setTimeout(() => {
          invoke(this, [ '$refs', 'comment', 'refreshEditor' ]);
          invoke(this, [ '$refs', 'comment', 'focus' ]);
        }, 250);
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.onKey('ctrl-s-keydown', this.sendComment);
  },
  methods: {
    /**
     * @this {Instance}
     * @param {number} id
     */
    getUserName(id /*: number */) {
      return get(this.users, [ id, 'login' ], id);
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value/*: any */) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent | null} event
     */
    async sendComment(event /*: ?KeyboardEvent */) {
      if (event) {
        event.preventDefault(); /* prevent ctrl-s to open dialog */
        if (!this.inEdit) { return; } // Keyboard Shortcut Guard
      }
      const comment = this.$refs.comment.editValue || '';
      if (!this.canSend || isEmpty(comment)) { return; }

      this.canSend = false;
      return sources.db.commentsSource.sendComment(comment)
      .then(
        () => {
          this.inEdit = false;
          set(this, [ '$refs', 'comment', 'editValue' ], '');
        },
        (err) => logger.error(err))
      .finally(() => {
        this.canSend = true;
        this.setFocusOnNextTick();
      });
    }
  }
});
export default component;
