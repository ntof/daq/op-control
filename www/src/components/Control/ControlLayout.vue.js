// @ts-check

import Vue from "vue";
import Control from './Control.vue';
import Timing from './Timing.vue';
import RunInfo from './RunInfo.vue';
import Events from './Events.vue';
import Comments from './Comments.vue';
import { get, isEmpty } from 'lodash';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  name: 'ControlLayout',
  components: { Control, Timing, RunInfo, Events, Comments },
  computed: {
    hasDns() { return !isEmpty(get(this.$store, [ 'state', 'dns' ])); }
  }
});
export default component;
