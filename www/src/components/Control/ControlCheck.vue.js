// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';

import { TimingMode } from '../../interfaces/timing';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const component = Vue.extend({
  name: 'ControlCheck',
  /**
   * @returns {{
   *   warnings: {timing?: string}
   * }}
   */
  data() {
    return { warnings: {} };
  },
  computed: {
    ...mapState('eacs/timing', { timingMode: 'mode' })
  },
  watch: {
    /** @this {Instance} */
    timingMode() {
      this.timingCheck();
    }
  },
  /** @this {Instance} */
  mounted() {
    this.timingCheck();
  },
  methods: {
    /** @this {Instance} */
    timingCheck() {
      if (this.timingMode === TimingMode.DISABLED) {
        this.$set(this.warnings, "timing", "Timing is disabled");
      }
      else {
        this.$delete(this.warnings, "timing");
      }
    }
  }
});
export default component;
