// @ts-check

import Vue from "vue";
import { DicXmlDataSet } from '@ntof/redim-client';
import { bindAll, get, isEmpty, keyBy, map, reverse, transform } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import { mapState } from 'vuex';
import { UrlUtilitiesMixin } from '../../utilities';
import { EventsBeamTypes, EventsEventMap } from '../../interfaces/eacs';
import MatchMedia from '../BaseMatchMedia';

/**
  * @typedef {V.Instance<typeof component> &
 *  { media: MatchMedia } &
  *  V.Instance<typeof UrlUtilitiesMixin>} Instance
  */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'Events',
  mixins: [ UrlUtilitiesMixin ],
  /**
   * @returns {{
   *   events: any | null,
   *   isScreenLG: boolean,
   *   client: DicXmlDataSet | null
   * }}
   */
  data() {
    return { events: null, isScreenLG: false, client: null };
  },
  computed: {
    ...mapState([ 'dns' ]),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isEvents() { return this.events !== null; }
  },
  watch: {
    dns() { this.monitor(); }
  },
  /** @this {Instance} */
  mounted() {
    bindAll(this, [ 'onValue' ]);
    this.monitor();
    this.media = new MatchMedia(MatchMedia.LG,
      (value) => { this.isScreenLG = value; });
  },
  /** @this {Instance} */
  beforeDestroy() {
    this.close();
    /** @type {MatchMedia} */ (this.media).close();
  },
  methods: {
    /** @this {Instance} */
    close() {
      if (this.client) {
        this.client.removeListener('value', this.onValue);
        this.client.close();
      }
      this.client = null;
    },
    /** @this {Instance} */
    monitor() {
      this.close();
      this.events = null;

      if (!isEmpty(this.dns)) {
        this.client = new DicXmlDataSet('EACS/Events',
          { proxy: this.getProxy() }, this.dnsUrl(this.dns));
        this.client.promise().catch((err) => logger.error(err));
        this.client.on('value', this.onValue);
      }
    },
    /**
     * @this {Instance}
     * @param {any} value
     */
    onValue(value) {
      this.events = reverse(map(value, (data) => {
        const indexed = keyBy(data.value, 'index');
        return transform(EventsEventMap, (ret, param, name) => {
          /** @type {{[key: string]: string | null}}*/(ret)[name] = get(indexed, [ /** @type number*/(param.index), 'value' ], null);
        }, {});
      }));
    },
    /**
     * @param {any} value
     */
    getBeamTypeName(value) {
      return get(EventsBeamTypes, [ value ], value);
    }
  }
});
export default component;
