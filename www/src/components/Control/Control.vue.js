// @ts-check

import Vue from "vue";
import { mapGetters, mapState } from 'vuex';

import {
  BaseKeepFocusMixin as KeepFocusMixin,
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { BaseDimStateAlert as StateAlert } from '@ntof/ntof-vue-widgets';

import { UrlUtilitiesMixin } from '../../utilities';
import { DataStatus, State } from '../../interfaces/eacs';
import EACSCmdMixin from '../../mixins/EACSCmdMixin';
import ControlCheck from './ControlCheck.vue';

/**
 * @typedef {{
 *   dataStatusDialog: V.Instance<typeof BaseVue.BaseDialog>
 *   dataStatus: V.Instance<typeof BaseVue.BaseParamInput>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<typeof UrlUtilitiesMixin> &
 *   V.Instance<typeof EACSCmdMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>>} Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'Control',
  components: { State: StateAlert, ControlCheck },
  mixins: [
    KeyboardEventMixin({ local: true, checkOnInputs: true }),
    UrlUtilitiesMixin,
    EACSCmdMixin,
    KeepFocusMixin
  ],
  data() {
    return { inEdit: false, isSendingCommand: false, dataStatus: DataStatus.UNKNOWN };
  },
  computed: {
    ...mapState([ 'dns' ]),
    ...mapState('eacs', { runInfo: 'runInfo', state: 'value' }),
    ...mapGetters('eacs', [ 'isActive' ]),
    ...mapState('ui', [ 'showKeyHints' ]),
    /**
     * @this {Instance}
     * @returns {void}
     */
    // eslint-disable-next-line vue/return-in-computed-property
    stateClass() {
      //      return (this.acqState > AcqState.WAITING_TO_START) ?
      //        'alert-success' : 'alert-primary';
      // return '';
    },
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isRunInfo() {
      return this.dns && this.runInfo.area;
    },
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isIdle() {
      return this.state === State.IDLE;
    },
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isLoading() {
      return this.state === State.LOADING;
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.onKey('enter', this.cmdStart);
    this.onKey('x', this.cmdStop);
    this.onKey('r', this.cmdReset);
  },
  methods: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value/*: any */) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     * @param {string} value
     */
    setDataStatus(value) {
      this.dataStatus = value;
      if (this.dataStatus === DataStatus.UNKNOWN)
        this.$refs.dataStatus.addWarning('unknown', "Please select Ok or Bad state.");
      else
        this.$refs.dataStatus.removeWarning('unknown');
    },
    /** @this {Instance} */
    async openStopDialog() {
      try {
        this.setDataStatus(DataStatus.UNKNOWN);
        if (await this.$refs.dataStatusDialog.request())
          this.cmdStop();
      }
      catch (err) { /* do nothing */}
    },
    /**
     * @this {Instance}
     * @param {any} err
     */
    handleError(err/*: any */) {
      logger.error(err);
    },
    /**
     * @this {Instance}
     * @param {function(string, ...any): Promise<void>} cmd
     * @param {string} dns
     * @param {...any} args
     */
    wrapCmd(cmd, dns, ...args) {
      this.isSendingCommand = true;
      cmd.call(this, dns, ...args)
      .catch((err) => logger.error(err))
      .finally(() => {
        this.isSendingCommand = false;
        this.setFocusOnNextTick();
      });
    },
    /** @this {Instance} */
    cmdStart() { this.wrapCmd(this.eacsStart, this.dns); },
    /** @this {Instance} */
    cmdStop() {
      this.wrapCmd(this.eacsStop, this.dns, this.dataStatus);
    },
    /** @this {Instance} */
    cmdReset() { this.wrapCmd(this.eacsReset, this.dns); }
  }
});
export default component;
