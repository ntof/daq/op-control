// @ts-check

import Vue from "vue";
import { isEmpty } from 'lodash';
import {
  BaseKeepFocusMixin as KeepFocusMixin,
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { BaseDimStateAlert as State } from '@ntof/ntof-vue-widgets';
import { UrlUtilitiesMixin } from '../../utilities';
import { mapGetters, mapState } from 'vuex';
import RunConfig from './RunConfig.vue';
import { RunInfoMap } from '../../interfaces/eacs';
import { sources } from '../../store';
import { ParamBuilderMixin } from '../../interfaces';

/**
 * @typedef {{
 *  runConfig: V.Instance<typeof RunConfig>,
 * }} Refs
  * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
    *   V.Instance<typeof UrlUtilitiesMixin> &
    *   V.Instance<typeof ParamBuilderMixin> &
    *   V.Instance<ReturnType<KeyboardEventMixin>>} Instance
    */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'RunInfo',
  components: { State, RunConfig },
  mixins: [
    KeyboardEventMixin({ local: true }),
    UrlUtilitiesMixin,
    ParamBuilderMixin,
    KeepFocusMixin
  ],
  /**
   * @returns {{
    *   inEdit: boolean,
  *   }}
    */
  data() {
    return { inEdit: false };
  },
  computed: {
    ...mapState([ 'dns' ]),
    ...mapState('eacs', [ 'runInfo' ]),
    ...mapState('ui', [ 'showKeyHints' ]),
    ...mapGetters('eacs', [ 'isActive' ]),
    ...mapGetters('db/setups', [ 'detectorsOpts', 'materialsOpts' ]),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isRunInfo() {
      return this.dns && this.runInfo.area;
    },
    /**
     * @this {Instance}
     * @returns {Array<{value: number, text: string}>}
     */
    materialsSetupOpts() {
      return [
        { value: -1, text: 'Not Set' },
        { value: 0, text: 'sample setup' }
      ];
    },
    /**
     * @this {Instance}
     * @returns {Array<{value: number, text: string}>}
     */
    detectorsSetupOpts() {
      return [
        { value: -1, text: 'Not Set' },
        { value: 0, text: 'sample setup' }
      ];
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.onKey('ctrl-s-keydown', this.submitAll);
  },
  methods: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value/*: any */) {
      this.inEdit = value;
    },
    /** @this {Instance} */
    async submit() {
      /** @type {Array<any>} */
      var params = [];
      this.paramBuilder('title', this.runInfo.title, RunInfoMap.title, params);
      this.paramBuilder('description', this.runInfo.description, RunInfoMap.description, params);
      if (!isEmpty(params)) {
        if (sources.eacs.runInfo) {
          await sources.eacs.runInfo.setParams(params)
          .then(
            () => this.onSubmit(),
            (err) => logger.error(err));
        }
      }
      else {
        this.onSubmit();
      }
    },
    /** @this {Instance} */
    onSubmit() {
      this.setEdit(false);
      this.setFocusOnNextTick();
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent | null} event
     */
    submitAll(event /*: KeyboardEvent */) {
      if (event) {
        event.preventDefault(); /* prevent ctrl-s to open dialog */
        if (!this.inEdit) { return; } // Keyboard Shortcut Guard
      }
      if (this.isActive) {
        this.submit();
      }
      this.$refs.runConfig.submit();
    }
  }
});
export default component;
