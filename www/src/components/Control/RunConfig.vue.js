// @ts-check

import Vue from "vue";
import { isEmpty, transform } from 'lodash';
import {
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { UrlUtilitiesMixin } from '../../utilities';
import { mapGetters, mapState } from 'vuex';
import { RunConfigMap } from '../../interfaces/eacs';
import { ParamBuilderMixin } from '../../interfaces';
import { sources } from '../../store';

/**
  * @typedef {V.Instance<typeof component> &
  *   V.Instance<typeof UrlUtilitiesMixin> &
  *   V.Instance<typeof ParamBuilderMixin> &
  *   V.Instance<ReturnType<KeyboardEventMixin>>} Instance
  */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'RunConfig',
  mixins: [
    KeyboardEventMixin({ local: true, checkOnInputs: true }),
    UrlUtilitiesMixin,
    ParamBuilderMixin
  ],
  props: {
    inEdit: { type: Boolean, default: false }
  },
  computed: {
    ...mapState([ 'dns' ]),
    ...mapState('eacs', [ 'runConfig' ]),
    ...mapGetters('db/setups', [ 'detectorsOpts', 'materialsOpts' ]),
    ...mapState('ui', [ 'showKeyHints' ]),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isRunConfig() {
      return this.dns && (this.runConfig.runNumber !== null);
    }
  },
  methods: {
    /** @this {Instance} */
    async submit() {
      /** @type {Array<any>} */
      const params = transform(
        [ 'title', 'description', 'experiment', 'detectorsSetupId', 'materialsSetupId' ],
        (ret, name) => this.paramBuilder(name, this.runConfig[name], RunConfigMap[name], ret), []);
      if (!isEmpty(params)) {
        if (sources.eacs.runConfig) {
          await sources.eacs.runConfig.setParams(params)
          .then(
            () => this.$emit('submit'),
            (err) => logger.error(err));
        }
      }
      else {
        this.$emit('submit');
      }
    }
  }
});
export default component;
