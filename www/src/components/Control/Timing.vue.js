// @ts-check
import {
  BaseKeepFocusMixin as KeepFocusMixin,
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import Vue from "vue";

import { TimingMode, TimingParamsMap } from '../../interfaces/timing';
import { get, invert, isEmpty, isNil, keys } from 'lodash';
import { mapState } from 'vuex';
import { sources } from '../../store';

/**
 * @typedef {{
  *   mode(): number,
  *   triggerRepeat(): number,
  *   triggerPeriod(): number,
  *   triggerPause(): number,
  *   eventNumber(): number,
  *   calibOut(): number,
  *   parasiticOut(): number,
  *   primaryOut(): number
  * }} TimingStateMap
  */

/**
 * @typedef {{
  *  mainCard: V.Instance<BaseVue.BaseCard>,
  * }} Refs
  * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> &
  *  V.Instance<ReturnType<KeyboardEventMixin>>} Instance
  */

const options = {
  /** @type  {{[key: number]: string}} */
  modeNames: invert(TimingMode)
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'Timing',
  ...options,
  mixins: [
    KeyboardEventMixin({ local: true }), KeepFocusMixin
  ],
  /**
   * @returns {{
    *   inEdit: boolean,
    *   calibHelpVisible: boolean,
    *   editMode: any | null
    * }}
    */
  data() {
    return {
      inEdit: false,
      calibHelpVisible: false,
      editMode: null
    };
  },
  computed: {
    ...mapState([ 'dns' ]),
    .../** @type TimingStateMap */(mapState('eacs/timing', keys(TimingParamsMap))),
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    /**
     * @this {Instance}
     * @returns {Array<string>}
     */
    alertClass() {
      return [ isNil(this.mode) ? 'alert-warning' : 'alert-primary' ];
    },
    /**
     * @this {Instance}
     * @returns {string}
     */
    defaultString() {
      return isEmpty(this.dns) ?
        'Please select a server...' : 'Waiting for service...';
    },
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isCalib() {
      const mode = this.inEdit ? this.editMode : this.mode;
      return this.getModeName(mode) === "CALIBRATION";
    }
  },
  watch: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    inEdit(value /*: boolean */) {
      if (value) {
        this.editMode = this.mode;
        this.$store.commit('eacs/timing/resetEdit');
      }
    },
    /**
     * @this {Instance}
     * @param {any} value
     */
    editMode: function(value /*: any */) {
      this.pushEditValue('mode', value);
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.onKey('ctrl-s-keydown', this.submitEdit);
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} name
     * @param {any} value
     */
    pushEditValue(name /*: string */, value /*: any */) {
      this.$store.commit('eacs/timing/editValue', { name, value });
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     * @param {string} mode
     * @returns {string}
     */
    getModeName(mode /*: number */) {
      return get(this.$options.modeNames, mode, 'UNKNOWN');
    },
    /** @this {Instance} */
    cancel() {
      this.inEdit = false;
      this.setFocusOnNextTick();
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent?} event
     */
    submitEdit(event /*: KeyboardEvent */) {
      if (event) {
        event.preventDefault();
        if (!this.inEdit) { return; } // Keyboard Shortcut Guard
      }

      return sources.timing.send()
      .then(
        () => {
          this.inEdit = false;
          this.setFocusOnNextTick();
        },
        (err) => logger.error(err));
    }
  }
});
export default component;
