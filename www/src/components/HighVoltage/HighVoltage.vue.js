// @ts-check
import Vue from "vue";
import { mapState } from 'vuex';
import { forEach, isEmpty, join } from 'lodash';
import MatchMedia from '../BaseMatchMedia';
import { UrlUtilitiesMixin } from '../../utilities';

const HV_PW = [ 'Unknown', 'On', 'Off' ];

const StatusBitField = {
  ON: 0x01,
  RUP: 0x02,
  RDOWN: 0x04,
  OVERCURRENT: 0x08,
  OVERVOLTAGE: 0x10,
  UNDERVOLTAGE: 0x20,
  EXTERNAL_TRIP: 0x40,
  MAX_V: 0x80,
  EXTERNAL_DISABLE: 0x100,
  INTERNAL_TRIP: 0x200,
  CALIB_ERROR: 0x400,
  UNPLUGGED: 0x800,
  OVERVOLTAGE_PROTECT: 0x2000,
  POWER_FAIL: 0x4000,
  TEMPERATURE_ERROR: 0x8000
};

const SortDir = {
  ASC: 'asc',
  DESC: 'desc'
};

/**
 * @typedef {{
 *   $and: Array<any>
 * }} MongoDBFilter
 *
 * @typedef {V.Instance<typeof component> &
 *  { mediaLG: MatchMedia, mediaXL: MatchMedia }} Instance
 */
const component = Vue.extend({
  name: 'HighVoltage',
  mixins: [
    UrlUtilitiesMixin
  ],
  /**
   * @returns {{
   *   mediaLG?: ?MatchMedia,
   *   mediaXL?: ?MatchMedia,
   *   client: null,
   *   isScreenLG: boolean,
   *   isScreenXL: boolean,
   *   currentSort: string,
   *   currentSortDir: string,
   *   errorOnlyFilter: boolean,
   *   activeOnlyFilter: boolean,
   *   regExpFilter: string | null
   * }}
   */
  data() {
    return {
      isScreenLG: false, isScreenXL: false, client: null,
      currentSort: 'name', currentSortDir: SortDir.ASC,
      errorOnlyFilter: false, activeOnlyFilter: true, regExpFilter: null
    };
  },
  computed: {
    ...mapState([ 'dns' ]),
    ...mapState('hv', [ 'filters', 'hvs', 'boardInfo', 'loading' ])
  },
  watch: {
    /** @this {Instance} */
    regExpFilter() { this.updateWorker(); }
  },
  beforeDestroy() {
    /** @type {MatchMedia} */ (this.mediaLG).close();
    /** @type {MatchMedia} */ (this.mediaXL).close();
  },
  mounted() {
    this.mediaLG = new MatchMedia(MatchMedia.LG,
      (/** @type {boolean} */value) => { this.isScreenLG = value; });
    this.mediaXL = new MatchMedia(MatchMedia.XL,
      (/** @type {boolean} */value) => { this.isScreenXL = value; });
    this.updateWorker();
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} field
     */
    setSorting(field) {
      if (this.currentSort === field) {
        this.currentSortDir = (this.currentSortDir === SortDir.ASC) ?
          SortDir.DESC : SortDir.ASC;
      }
      else {
        this.currentSort = field;
        this.currentSortDir = SortDir.ASC;
      }
      this.updateWorker();
    },
    /**
     * @this {Instance}
     * @param {string} field
     */
    getSortingClass(field) {
      if (this.currentSort !== field) {
        return [ "fa-sort", "i-deactivated" ];
      }
      else if (this.currentSortDir === SortDir.ASC) {
        return [ "fa-sort-down" ];
      }
      return [ "fa-sort-up" ];
    },
    /** @this {Instance} */
    setAllFilter() {
      this.errorOnlyFilter = false;
      this.activeOnlyFilter = false;
      this.updateWorker();
    },
    /** @this {Instance} */
    toggleErrorFilter() {
      this.errorOnlyFilter = !this.errorOnlyFilter;
      this.updateWorker();
    },
    /** @this {Instance} */
    toggleActiveFilter() {
      this.activeOnlyFilter = !this.activeOnlyFilter;
      this.updateWorker();
    },
    /** @this {Instance} */
    updateWorker() {
      const mfilter =  /** @type MongoDBFilter */({ $and: [] });
      if (this.activeOnlyFilter) {
        mfilter.$and.push({ pw: { $eq: 1 } });
      }
      if (this.errorOnlyFilter) {
        mfilter.$and.push({ caenErrorCode: { $gt: 0 } });
      }

      if (!isEmpty(this.regExpFilter)) {
        const rf = {
          $or: [
            { name: { $regex: this.regExpFilter } },
            { channelName: { $regex: this.regExpFilter } },
            { caenErrorCode: { $regex: this.regExpFilter } },
            { caenErrorMessage: { $regex: this.regExpFilter } }
          ]
        };
        mfilter.$and.push(rf);
      }

      const filters = {
        offset: 0,
        limit: 1000, // 1000 channels is enough
        search: mfilter,
        orderBy: this.currentSort +
          ((this.currentSortDir === SortDir.ASC) ? ":asc" : ":desc")
      };

      this.$store.commit(`hv/filters`, filters);
    },
    /**
     * @brief simply transform nanoseconds to milliseconds
     * @this {Instance}
     * @param {number} ns
     * @return {number}
     */
    NsToMs(ns) {
      return ns / 1000 / 1000;
    },
    /**
     * @this {Instance}
     * @param {number} value
     * @return {string}
     */
    getPW(value /*: number */) {
      return HV_PW[value];
    },
    /**
     * @this {Instance}
     * @param {number} value
     * @return {string}
     */
    getStatusFromBitfield(value /*: number */) {
      /** @type Array<string> */
      const fields = [];
      forEach(StatusBitField, (bit, key) => {
        if ((value & bit) === bit) { fields.push(key); }
      });
      return join(fields, '\n');
    },
    /**
     * @this {Instance}
     * @param {number} timestamp ms
     * @return {boolean}
     */
    isToday(timestamp /*: number */) {
      const today = new Date();
      const date = new Date(timestamp);
      return date.setHours(0, 0, 0, 0) === today.setHours(0, 0, 0, 0);
    }
  }
});
export default component;
