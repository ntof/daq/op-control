// @ts-check
import { EventEmitter } from "events";
import {
  assign, flatten, forEach, get, isEqual, isNil, map,
  set, startsWith, sumBy, toNumber, values
} from "lodash";
import { DicXmlDataSet } from '@ntof/redim-client';
import { parseDataSet, parseValue } from "../interfaces";
import {
  AcqParamMapGeneral,
  BoardInfoParamMap,
  HvCrate
} from "../interfaces/hv";

/**
 * @typedef {import('./HighVoltage.shared-worker').HighVoltageSharedWorker} HighVoltageSharedWorker
 *
 * @typedef {import('./types').HighVoltageWorker.Clients} Clients
 * @typedef {import('./types').HighVoltageWorker.ClientsArray} ClientsArray
 * @typedef {import('./types').HighVoltageWorker.Stats} Stats
 * @typedef {import('./types').HighVoltageWorker.HVCrateArray} HVCrateArray
 *
 * @typedef {import('../interfaces/hv').BoardInfo} BoardInfo
 * @typedef {import('../interfaces/hv').ChanInfo} ChanInfo
 * @typedef {import('../interfaces/hv').ChannelInfo} ChannelInfo
 *
 * @typedef {import('@ntof/redim-client').XmlData.Data } Data
 */

export class HVManager extends EventEmitter {
  /**
   * @param {HighVoltageSharedWorker} hvWorker
   */
  constructor(hvWorker) {
    super();
    this.worker = hvWorker;
    /** @type {Array<string>} */
    this.hvNames = [];
    /** @type {ClientsArray} */
    this.clients = {};
    /** @type {HVCrateArray} */
    this.data = {};
    /** @type {Stats} */
    this.stats = {
      allChannelsInError: 0,
      activeChannelsInError: 0
    };
    /** @type {?any} */
    this.options = null;
  }

  /**
   * @brief set dns informations
   * @param {?any} options
   */
  setOptions(options) {
    if (!isEqual(this.options, options)) {
      this.release();
      this.options = options;
    }
  }

  /**
   * @brief set array of hv services
   * @param {Array<string>} services
   */
  setServices(services) {
    if (isEqual(this.hvNames, services)) {
      // EACS/Info changed but HV services are the same.
      // Nothing to do.
      return;
    }
    this.release();
    this.hvNames = services;
    this.connect();
  }

  /**
   * @brief release listeners on clients
   */
  release() {
    forEach(this.clients, (client) => {
      client.acq.removeListener('value', this.handleValueAcq);
      client.acq.removeListener('error', this.worker.handleError);
      client.acq.close();
      client.board.removeListener('value', this.handleValueBoardInfo);
      client.board.removeListener('error', this.worker.handleError);
      client.board.close();
    });
    this.clear();
  }

  /**
   * @brief reset status
   */
  clear() {
    this.hvNames = [];
    this.clients = {};
    this.data = {};
    this.stats = {
      allChannelsInError: 0,
      activeChannelsInError: 0
    };
    this.publish(true);
  }

  connect() {
    if (isNil(get(this.options, 'dns', null))) {
      this.worker.handleError("Can't connect to High Voltage Services. Missing dns.");
      return;
    }
    forEach(this.hvNames, (name) => {
      // Connecting to HV/Acquisition service
      const acqService = new DicXmlDataSet(name + "/Acquisition",
        this.options, get(this.options, 'dns'));
      acqService.on('error', this.worker.handleError);
      acqService.on('value', (value) => this.handleValueAcq(name, value));
      acqService.promise().catch(this.worker.handleError);

      const boardService = new DicXmlDataSet(name + "/BoardInformation",
        this.options, get(this.options, 'dns'));
      boardService.on('error', this.worker.handleError);
      boardService.on('value', (value) => this.handleValueBoardInfo(name, value));
      boardService.promise().catch(this.worker.handleError);

      this.clients[name] = {
        acq: acqService,
        board: boardService
      };
    });
  }

  /**
   * @param {string} name
   * @returns {HvCrate}
   */
  getCrateObj(name) {
    if (!this.data[name]) {
      this.data[name] = new HvCrate(name);
    }
    return this.data[name];
  }

  /**
   * @brief handle XmlData coming from Acq service
   * @param {string} name
   * @param {Array<redim.XmlData.Data>} value
   */
  handleValueAcq(name, value) {
    if (!value) {
      return;
    }
    // Retrieve the related HV Crate
    const crate = this.getCrateObj(name);

    forEach(value, (/** @type Data */ dataItem) => {
      // Get fields
      const dataName = get(dataItem, "name", '');
      const dataValue = get(dataItem, "value");
      const dataType = toNumber(get(dataItem, "type"));

      if (!startsWith(dataName, 'chan')) {
        // Global Field
        const fieldName = get(AcqParamMapGeneral, [ dataName, 'field' ], null);
        if (!fieldName) { return; } // Unhandled parameter
        set(crate.chanInfo, fieldName, parseValue(dataValue, dataType));
      }
      else {
        // Channel Field
        // Do regex group matching
        const AcqFieldRegex = /chan(\d+)(.+$)/i;
        const matches = dataName.match(AcqFieldRegex); // example of result ["chan19field", "19", "field"];
        if (!matches || (matches && matches.length !== 3)) { return; } // Unhandled or bad formatted field
        const index = toNumber(matches[1]);
        const field = matches[2];
        // Now get the channel object and add the data on it
        const channel = crate.getChannel(index);
        set(channel, field, parseValue(dataValue, dataType));
      }
    });

    // update stats
    crate.updateStats();

    this.publish();
  }

  /**
   * @brief handle XmlData coming from Board service
   * @param {string} name
   * @param {Array<redim.XmlData.Data>} value
   */
  handleValueBoardInfo(name, value) {
    if (!value) {
      return;
    }
    const boardInfo = /** @type BoardInfo */ (parseDataSet(value, BoardInfoParamMap));
    const crate = this.getCrateObj(name);
    assign(crate.boardInfo, boardInfo);
    this.publish();
  }

  publish(fromClear = false) {
    if (fromClear) {
      this.emit("publish", { data: null, stats: null, boardInfo: null });
      return;
    }
    this.stats.allChannelsInError = sumBy(values(this.data), (crate) => crate.allChannelsInError);
    this.stats.activeChannelsInError = sumBy(values(this.data), (crate) => crate.activeChannelsInError);
    const channels = flatten(map(values(this.data), (crate) => crate.channels));
    const boardInfo = map(values(this.data), (crate) => crate.boardInfo);
    this.emit("publish", channels, this.stats, boardInfo);
  }
}
