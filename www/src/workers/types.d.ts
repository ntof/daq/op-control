import {DicXmlDataSet} from '@ntof/redim-client';
import {HvCrate} from "../interfaces/hv";

/**
 * High Voltage Worker
 */
declare namespace HighVoltageWorker {

  interface Filters {
    offset: number;
    limit: number;
    search?: { [key: string]: string } | null;
    orderBy?: string | null; // suffix with :asc/:desc
  }

  export interface Clients {
    acq: DicXmlDataSet,
    board: DicXmlDataSet
  }

  export interface ClientsArray {
    [key: string]: Clients
  }

  export interface Stats {
    allChannelsInError: number,
    activeChannelsInError: number
  }

  export interface HVCrateArray {
    [key: string]: HvCrate
  }
}


