// @ts-check
import { bindAll, find, get, isArray, map, unset } from 'lodash';
import { BaseCollectionWorker } from '@cern/base-web-workers';
import { DicXmlValue, xml } from '@ntof/redim-client';
import { HVManager } from "./HighVoltageManager";

/**
 * @typedef {import('./types').HighVoltageWorker.Stats} Stats
 * @typedef {import('../interfaces/hv').BoardInfo} BoardInfo
 * @typedef {import('../interfaces/hv').ChannelInfo} ChannelInfo
 */


/**
 * @typedef {Partial<{
 *  options: any,
 *  args: any
 * }>} PartMessageIn
 * @typedef {Partial<{
 *  stats: Stats
 *  boardInfo: Array<BoardInfo>
 * }>} PartMessageOut
 * @typedef {BaseWorker.BaseCollectionWorker.MessageIn & PartMessageIn } MessageIn
 * @typedef {BaseWorker.BaseCollectionWorker.MessageOut & PartMessageOut  } MessageOut
 */
export class HighVoltageSharedWorker extends BaseCollectionWorker {
  /**
   * @param {(msg: MessageOut) => any} post
   */
  constructor(post) {
    super(post);
    bindAll(this, [ 'handleError', 'handleValueEacsInfo' ]);

    /** @type {?DicXmlValue} */
    this.clientEacsInfo = null;
    /** @type {MessageOut} */
    this.out = {};

    this.hvManager = new HVManager(this);
    this.hvManager.on("publish", this.publishData.bind(this));
  }

  destroy() {
    this.close();
    this.hvManager.removeListener("publish", this.publishData);
    super.destroy();
  }

  /**
   * @param  {MessageIn} msg
   * @return {void}
   */
  process(msg) {
    if (!msg) { return; }

    if (msg.options) {
      this.hvManager.setOptions(msg.options);
      // prevents filtering to automatically restart
      this.in.data = null;
      unset(this.out, 'stats');
      unset(this.out, 'boardInfo');
    }
    super.process(msg);

    if (msg.options) { // It's coming from vue instance
      this.close();
      this.in.data = null;
      // Connecting to EACS/Info service
      this.clientEacsInfo = new DicXmlValue("EACS/Info", msg.options,
        get(msg.options, 'dns'));
      this.clientEacsInfo.on('error', this.handleError);
      this.clientEacsInfo.on('value', this.handleValueEacsInfo);
      this.clientEacsInfo.promise().catch(this.handleError);
    }
  }

  close() {
    this.hvManager.release();
    if (this.clientEacsInfo) {
      this.clientEacsInfo.removeListener('value', this.handleValueEacsInfo);
      this.clientEacsInfo.removeListener('error', this.handleError);
      this.clientEacsInfo.close();
      this.clientEacsInfo = null;
      this.abort();
    }
    unset(this.out, 'stats');
    unset(this.out, 'boardInfo');
  }

  /**
   * @param {any} error
   */
  handleError(error) {
    this.hvManager.release();
    this.post({ error: error });
  }

  /**
   * @param {any} value
   */
  handleValueEacsInfo(value) {
    const dataJs = xml.toJs(value);
    if (!isArray(dataJs.data)) {
      dataJs.data = [ dataJs.data ];
    }
    const hvNode = find(dataJs.data, (o) => o.$.name === 'highVoltage');
    if (!hvNode) {
      this.process({ data: null });
      return;
    }
    if (!isArray(hvNode.data)) {
      hvNode.data = [ hvNode.data ];
    }
    const hvServices = map(hvNode.data, (o) => o.$.name);
    this.hvManager.setServices(hvServices);
  }

  /**
   * @param {Array<ChannelInfo>} data
   * @param {Stats} stats
   * @param {Array<BoardInfo>} boardInfo
   */
  publishData(data, stats, boardInfo) {
    /** @type MessageOut */(this.out).stats = stats;
    /** @type MessageOut */(this.out).boardInfo = boardInfo;
    this.process({ data });
  }
}

BaseCollectionWorker.create = (p) => new HighVoltageSharedWorker(p);
