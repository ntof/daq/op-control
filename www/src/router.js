// @ts-check
import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from './components/Dashboard.vue';

import Control from './components/Control/ControlLayout.vue';
import Cards from './components/Cards/CardsLayout.vue';
import BeamLine from './components/BeamLine/BeamLineLayout.vue';
import Materials from './components/Materials/MaterialsLayout.vue';
import HighVoltage from './components/HighVoltage/HighVoltage.vue';
import Alarms from './components/Alarms/Alarms.vue';

import { currentUrl } from './utilities';
import store from './store';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/', name: 'Dashboard', component: Dashboard,
      props: (route) => ({
        dns: route.query.dns
      }),
      children: [
        { path: 'control', name: 'Control', component: Control },
        { path: 'cards', name: 'Cards', component: Cards },
        { path: 'beamline', name: 'Beam Line', component: BeamLine },
        { path: 'materials', name: 'Materials', component: Materials },
        { path: 'hv', name: 'HV Status', component: HighVoltage },
        { path: 'alarms', name: 'Alarms', component: Alarms }
      ],
      redirect: '/control'
    },
    { path: '/index.html', redirect: '/' },
    { path: '/Dashboard', redirect: '/' }
  ],
  base: currentUrl()
});
// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.afterEach((to, from) => { /* jshint unused:false */
  store.commit('queryChange', to.query);
});

export default router;
