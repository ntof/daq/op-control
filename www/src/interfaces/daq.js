// @ts-check
import { ParamType } from './index';

/**
 * @typedef {import('./types').XML.ParamMap} ParamMap
 */

/** @enum {number} */
export const DaqState = {
  HARDWARE_INIT: 0,
  NOT_CONFIGURED: 1,
  CALIBRATING: 2,
  WAITING_FOR_COMMAND: 3,
  PROCESSING_COMMAND: 4,
  STOPPING_ACQ: 8,
  STARTING_ACQ: 9,
  RESETING: 10,
  INITIALIZING_ACQ: 11,
  WAITING_FOR_START_ACQ: 12,
  SUPER_USER: 13
};

/** @enum {number} */
export const AcqState = {
  IDLE: 0,
  INITIALIZATION: 1,
  ALLOCATING_MEMORY: 2,
  WAITING_TO_START: 3,
  ARMING_CARD_TRIGGERS: 4,
  WAITING_FOR_TRIGGER: 5,
  DUMPING_CARD_MEM_BUFFERS: 6,
  QUEUING_BUFFERS_FOR_WRITER: 7
};

export const ChannelParamsMap = {
  enabled: { index: 1, type: ParamType.BOOL },
  detectorType: { index: 2, type: ParamType.STRING },
  detectorId: { index: 3, type: ParamType.UINT32 },
  chassisId: { index: 7, type: ParamType.UINT8 },
  sampleRate: { index: 9, type: ParamType.FLOAT },
  sampleSize: { index: 10, type: ParamType.UINT32 },
  fullScale: { index: 11, type: ParamType.FLOAT },
  delay: { index: 12, type: ParamType.INT32 },
  threshold: { index: 13, type: ParamType.FLOAT },
  thresholdSign: { index: 14, type: ParamType.INT32 },
  zsStart: { index: 15, type: ParamType.UINT32 },
  offset: { index: 16, type: ParamType.FLOAT },
  preSamples: { index: 17, type: ParamType.UINT32 },
  postSamples: { index: 18, type: ParamType.UINT32 },
  impedance: { index: 20, type: ParamType.FLOAT }
};

/** @type ParamMap */
export const ChannelCalibrationMap = {
  fullScale: { index: 0, type: ParamType.FLOAT },
  offset: { index: 1, type: ParamType.FLOAT },
  threshold: { index: 2, type: ParamType.FLOAT }
};

/** @type ParamMap */
export const ZSPChannelMap = {
  sn: { index: 0, type: ParamType.STRING },
  channel: { index: 1, type: ParamType.UINT32 }
};

/** @type ParamMap */
export const ZSPMasterMap = {
  sn: { index: 0, type: ParamType.STRING },
  channel: { index: 1, type: ParamType.UINT32 },
  slave: { type: ParamType.NESTED, map: ZSPChannelMap }
};

/** @enum {number} */
export const ZSPMode = {
  INDEPENDENT: 0,
  SINGLE_MASTER: 1,
  MASTER: 2
};

/** @enum {{
 * index: number,
 * type: ParamType
 * map?: {
 *   master: {
 *     type: ParamType,
 *     map: typeof ZSPMasterMap
 *   }
 * }
 * }} */
export const ZSPMap = {
  mode: { index: 1, type: ParamType.ENUM },
  configuration: { index: 2, type: ParamType.NESTED, map: {
    master: { type: ParamType.NESTED, map: ZSPMasterMap }
  } }
};
