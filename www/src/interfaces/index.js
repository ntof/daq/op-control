// @ts-check

import Vue from "vue";
import { assign, forEach, get, isNil, isObject, keyBy, toNumber, transform } from 'lodash';
import { DicXmlDataSet } from '@ntof/redim-client';

// @ts-ignore
export const ParamType = DicXmlDataSet.DataType;

/**
 * @typedef {import('./types').XML.ParamDesc} ParamDesc
 * @typedef {import('./types').XML.ParamMap} ParamMap
 */

/**
 * @param {string | number} value
 * @param {redim.XmlData.DataType} type
 * @return {boolean|number|string}
 */ // eslint-disable-next-line complexity
export function parseValue(value, type) {
  switch (type) {
  case ParamType.INT64:
  case ParamType.INT32:
  case ParamType.INT16:
  case ParamType.INT8:
  case ParamType.DOUBLE:
  case ParamType.FLOAT:
  case ParamType.ENUM:
  case ParamType.UINT32:
  case ParamType.UINT64:
  case ParamType.UINT8:
  case ParamType.UINT16:
    return toNumber(value);
  case ParamType.BOOL:
    return toNumber(value) > 0;
  default:
    return value;
  }
}

/**
 * @details this mixin can be used when ParamInput refs matches the description
 * @typedef {V.Instance<typeof ParamBuilderMixin>} Instance
 */
export const ParamBuilderMixin =  Vue.extend({
  methods: {
    /**
     * [paramBuilder description]
     * @param {string} name name of the param
     * @param {any} value current "live" value for the param
     * @param {{ type: ParamType } & Object<string, any>} desc param description
     * @param {Array<any>} params output values
     */
    paramBuilder(name, value, desc, params) {
      const editValue = parseValue(get(this.$refs, [ name, 'editValue' ]), desc.type);
      if (value !== editValue) {
        params.push(assign({ value: editValue }, desc));
      }
    }
  }
});

/**
 *
 * @param {any} value
 * @param {ParamDesc} desc
 * @returns {boolean|number|string|*}
 */
function parseParamValue(value, desc) {
  if (desc.type === ParamType.NESTED && desc.map) {
    return parseDataSet(value, desc.map);
  }
  else {
    return parseValue(value, desc.type);
  }
}

/**
 * @brief parse an incoming list of DIMData and shape it as an object
 * @param {Array<redim.XmlData.Data>} data
 * @param {ParamMap} paramMap
 * @returns Object<string, any>
 */
export function parseDataSet(data, paramMap) {
  const indexed = keyBy(data, 'index');
  return transform(paramMap,
    /**
     * @param {Object<string, any>} ret
     * @param {ParamDesc} param
     * @param {string} name
     */
    (ret, param, name) => {
      if (isNil(param.index)) {
      /* collect all using their name */ /* @ts-ignore */
        ret[name] = transform(indexed,
          /**
           * @param {Array<string>} ret
           * @param {redim.XmlData.Data} data
           */
          (ret, data) => {
            if (data.name === name) {
              const item = parseParamValue(data.value, param);
              // Some objects use index as a value
              if (isObject(item)) {
                // @ts-ignore isObject
                item.index = data.index;
              }
              ret.push(item);
            }
          }, []);
      }
      else {
        var value = get(indexed, [ param.index, 'value' ]);
        if (value !== undefined) {
          ret[name] = parseParamValue(value, param);
        }
      }
    }, {});
}

/**
 * @brief generate a list of DIMData to be sent back to a device
 * @param {Object<string, any>} values
 * @param {ParamMap} paramMap
 * @returns Array<any>
 */
export function genDataSet(values, paramMap) {
  let lastIndex = -1;

  /** @type Array<{index: number, value: any}> */
  const ret = transform(paramMap,
    /**
     * @param {Array<any>} ret
     * @param {ParamDesc} param
     * @param {string} name
     */
    (ret, param, name) => {
      if (isNil(param.index)) {
        return; // processed later
      }
      const value = get(values, name);

      if (!isNil(value)) {
        if (param.index > lastIndex) {
          lastIndex = param.index;
        }
        if (param.type === ParamType.NESTED) {
          ret.push({
            index: param.index,
            // @ts-ignore map existe because typeis NESTED
            value: genDataSet(value, param.map)
          });
        }
        else {
          ret.push({ index: param.index, value: value, type: param.type });
        }
      }
    }, []);

  return transform(paramMap, (ret, param, name) => {
    if (!isNil(param.index)) {
      return; // already processed
    }
    const value = get(values, name);

    if (!isNil(value)) {
      if (param.type === ParamType.NESTED) {
        forEach(value, (v) => {
          // @ts-ignore map existe because typeis NESTED
          ret.push({ index: ++lastIndex, value: genDataSet(v, param.map) });
        });
      }
      else {
        // @ts-ignore map existe because typeis NESTED
        ret.push({ index: ++lastIndex, value: value, type: param.type });
      }
    }
  }, ret);
}
