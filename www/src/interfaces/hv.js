/* eslint-disable camelcase */
// @ts-check
import { ParamType } from "./index";
import { nth, sumBy } from "lodash";

/**
 * @typedef {import('./types').XML.ParamMap} ParamMap
 */

/** @type ParamMap */
export const AcqParamMapGeneral = {
  acqStamp: { field: 'acqStamp', type: ParamType.INT64 },
  aqcStamp: { field: 'acqStamp', type: ParamType.INT64 }, // Typo on real service...
  cycleStamp: { field: 'cycleStamp', type: ParamType.INT64 },
  cycleName: { field: 'cycleName', type: ParamType.STRING },
  slot: { field: 'slot', type: ParamType.INT16 }
};

/** @type ParamMap */
export const BoardInfoParamMap = {
  acqStamp: { index: 0, type: ParamType.INT64 },
  cycleStamp: { index: 1, type: ParamType.INT64 },
  cycleName: { index: 2, type: ParamType.STRING },
  description: { index: 3, type: ParamType.STRING },
  firmware: { index: 4, type: ParamType.STRING },
  model: { index: 5, type: ParamType.STRING },
  serial: { index: 6, type: ParamType.INT32 }
};

/**
 * @param {Array<any>} collection
 * @param {function(any): boolean} pred
 * @returns number
 */
const count = (collection, pred) => sumBy(collection, (c) => (pred(c) ? 1 : 0));

export class BoardInfo {
  /**
   * @param {string} name
   */
  constructor(name) {
    /** @type {string} */  this.name = name;
    /** @type {number?} */ this.acqStamp = null;
    /** @type {number?} */ this.cycleStamp = null;
    /** @type {string?} */ this.cycleName = null;
    /** @type {string?} */ this.description = null;
    /** @type {string?} */ this.firmware = null;
    /** @type {string?} */ this.model = null;
    /** @type {number?} */ this.serial = null;
  }
}

export class ChanInfo {
  constructor() {
    /** @type {number?} */ this.acqStamp = null;
    /** @type {number?} */ this.cycleStamp = null;
    /** @type {string?} */ this.cycleName = null;
    /** @type {number?} */ this.slot = null;
  }
}

export class ChannelInfo {
/**
   * @details Class that store all information of a HV Channel
   * @param {string} name
   * @param {number} index
   * @param {ChanInfo} chanInfo
   */
  constructor(name, index, chanInfo) {
    /** @type {string} */  this.name = name;
    /** @type {number} */  this.index = index;
    /** @type {ChanInfo} */this.chanInfo = chanInfo; // Ref
    /** @type {number?} */ this.caenErrorCode = null;
    /** @type {string?} */ this.caenErrorMessage = null;
    /** @type {string?} */ this.channelName = null;
    /** @type {number?} */ this.channelStatusBitfield = null;
    /** @type {number?} */ this.i0set = null;
    /** @type {number?} */ this.i0set_max = null;
    /** @type {number?} */ this.i0set_min = null;
    /** @type {number?} */ this.imon = null;
    /** @type {number?} */ this.pw = null;
    /** @type {number?} */ this.rdwn = null;
    /** @type {number?} */ this.rdwn_max = null;
    /** @type {number?} */ this.rdwn_min = null;
    /** @type {number?} */ this.rup = null;
    /** @type {number?} */ this.rup_max = null;
    /** @type {number?} */ this.rup_min = null;
    /** @type {number?} */ this.svmax = null;
    /** @type {number?} */ this.svmax_max = null;
    /** @type {number?} */ this.svmax_min = null;
    /** @type {number?} */ this.trip = null;
    /** @type {number?} */ this.trip_max = null;
    /** @type {number?} */ this.trip_min = null;
    /** @type {number?} */ this.v0set = null;
    /** @type {number?} */ this.v0set_max = null;
    /** @type {number?} */ this.v0set_min = null;
    /** @type {number?} */ this.vmon = null;
  }
}

export class HvCrate {
  /**
   * @details Class that store all information of a HVCrate
   * @param {string} name
   */
  constructor(name) {
    /** @type {string} */
    this.name = name;
    /** @type {BoardInfo} */
    this.boardInfo = new BoardInfo(name);
    /** @type {ChanInfo} */
    this.chanInfo = new ChanInfo();
    /** @type {number} */
    this.allChannelsInError = 0;
    /** @type {number} */
    this.activeChannelsInError = 0;
    /** @type {Array<ChannelInfo>} */
    this.channels = [];
  }

  /**
   * @details return the ChannelInfo object at given index.
   *          It creates a new one if doesn't exist.
   * @param {number} index
   * @returns {ChannelInfo}
   */
  getChannel(index) {
    const channel = nth(this.channels, index);
    if (!channel) {
      const newChannel = new ChannelInfo(this.name, index, this.chanInfo);
      this.channels[index] = newChannel;
      return newChannel;
    }
    else {
      return channel;
    }
  }

  /**
   * @details it update internal stats.
   */
  updateStats() {
    this.allChannelsInError = count(this.channels,
      (/** @type {ChannelInfo} */x) => x.caenErrorCode !== 0);
    this.activeChannelsInError = count(this.channels,
      (/** @type {ChannelInfo} */x) => x.caenErrorCode !== 0 && x.pw === 1);
  }
}
