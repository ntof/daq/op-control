import { OutputMode, TimingMode } from "./timing";
import { Alarm as AlarmObj } from "./alarmserver";
import { XmlData } from '@ntof/redim-client';

/**
 * XmlData
 */
declare namespace XML {
  interface ParamDesc {
    index?: number,
    type: XmlData.DataType
    map?: ParamMap
    field?: string
  }

  export interface ParamMap {
    [key: string]: ParamDesc
  }
}

/**
 * Alarms Types
 */
declare namespace AlarmServer {
  type Alarm = AlarmObj;

  interface Filters {
    offset: number;
    limit: number;
    search?: { [key: string]: string } | null;
    orderBy?: string | null; // suffix with :asc/:desc
  }

  interface Stats {
    critical: number;
    error: number;
    warning: number;
    info: number;
    none: number;
    masked: number;
    disabled: number;
    active: number;
  }
}

/**
 * Timing Types
 */
declare namespace Timing {
  interface TimingData {
    mode: TimingMode;
    triggerRepeat: number;
    triggerPeriod: number;
    triggerPause: number;
    eventNumber: number;
    calibOut: OutputMode;
    parasiticOut: OutputMode;
    primaryOut: OutputMode;
  }
}

/**
 * DB
 */
declare namespace DB {
  type MaterialType = "sample"|"filter"|"source"

  interface Setup {
    id: number;
    name: string;
    description: string;
    earNumber: number;
    operYear: number;
  }

  interface User {
    userId: number;
    username: string;
  }

  interface Comment {
    id: number,
    date: number,
    comments: string,
    userId: number
  }

  interface Material {
    id: number;
    title: string;
    type: DB.MaterialType;
  }

  interface SetupMaterial {
    materialId: number;
    position: number;
    status: number;
  }

  interface SetupDetector {
    detectorId: number;
    containerId: number;
  }

  interface Detector {
    id: number;
    name: string;
  }

  interface Container {
    id: number;
    name: string;
  }
}

/**
 * EACS
 */
declare namespace EACS {
  interface DaqListCardInfo {
    channelsCount: number;
    type: string;
    serialNumber: string;
    index: number;
    used: boolean;
    daq: string;
  }

  interface DaqListInfo {
    name: string;
    used: boolean;
    cards: Array<DaqListCardInfo>;
  }

  interface HVCardInfo {
    service: string;
    cardIndex: number;
    channels: Array<number>;
  }

  interface Info {
    filterStation?: string;
    hv?: Object<string, HVCardInfo>;
  }

  interface RunInfo {
    runNumber?: number;
    eventNumber?: number;
    validatedNumber?: number;
    validatedProtons?: number;
    title?: string;
    description?: string;
    area?: string;
    experiment?: string;
    detectorsSetupId?: number;
    materialsSetupId?: number;
  }

  interface RunConfig {
    runNumber?: number;
    title?: string;
    description?: string;
    experiment?: string;
    detectorsSetupId?: number;
    materialsSetupId?: number;
  }
}

/**
 * DAQ
 */
declare namespace DAQ {
  type ChanId = string

  interface DaqSourceConfig {
    service: string;
    path: string;
    daqInfo: EACS.DaqListInfo
  }

  declare namespace DaqConfig {
    interface ZSPChannel {
      id: DaqConfig$ChanId;
      sn: string;
      channel: number;
      masterId?: ChanId;
      slave?: Array<ChanId>;
    }

    interface ZSP {
      mode: number | undefined | null;
      channels: { [key: ChanId]: ZSPChannel };
      master: { [key: ChanId]: [ChanId] };
      independent: { [key: ChanId]: [ChanId] };
    }

    interface Channel {
      enabled: boolean;
      detectorType: string;
      detectorId: number;
      chassisId: number;
      sampleRate: number;
      sampleSize: number;
      fullScale: number;
      delay: number;
      threshold: number;
      thresholdSign: number;
      zsStart: number;
      offset: number;
      preSamples: number;
      postSamples: number;
      impedance: number;

      id: string;
      serialNumber: string;
      index: number;
    }

  }
}


