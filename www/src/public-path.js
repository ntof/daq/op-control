
/* for webpack module loaders */
import { currentUrl } from './utilities';

/* eslint-disable-next-line */ /* @ts-ignore */
if (typeof __webpack_public_path__ !== 'undefined' &&
  !('__karma__' in window)) {
  /* eslint-disable-next-line */ /* @ts-ignore */
  __webpack_public_path__ = currentUrl() + '/dist/'; // jshint ignore:line
}
