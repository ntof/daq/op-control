// @ts-check

import './public-path';

const utils = {
  /**
   * @param  {string} url
   * @return {string}
   */
  publicPath: function(url) {
    /* eslint-disable-next-line camelcase */ /* @ts-ignore */
    if (typeof __webpack_public_path__ !== 'undefined') {
      /* eslint-disable-next-line camelcase */ /* @ts-ignore */
      return __webpack_public_path__ + url;
    }
    return url;
  }
};

export default {
  /** @param {InstanceType<Vue>} Vue */
  install(Vue) {
    Vue.helpers = utils;
    Vue.prototype.$utils = utils;
  }
};
