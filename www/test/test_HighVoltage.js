// @ts-check

import './karma_index';
import { waitForValue, waitForWrapper } from './utils';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import _ from 'lodash';

import HighVoltage from '../src/components/HighVoltage/HighVoltage.vue';
import store from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('HighVoltage', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        const config = {
          hv: [
            { cardId: 0, nChannels: 2 },
            { cardId: 1, nChannels: 4 }
          ]
        };
        this.env.stub = new NTOFStub(config);
        return this.env.stub.init();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      // this.env.eacs.close();
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can see HighVoltage', async function() {
    wrapper = mount(HighVoltage, { });
    const hv = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'HighVoltage' }));

    // No dns, we should have no results
    await waitForValue(wrapper, () => _.isEmpty(hv.vm.hvs), true);
    let rows = hv.findAll(".x-table-channels > tbody > tr");
    expect(rows.length).to.be.equal(0);

    // Now sets dns
    store.commit('queryChange', { dns: env.dns });
    await waitForValue(wrapper, () => wrapper.vm.loading, false);
    await waitForValue(wrapper, () => _.isEmpty(hv.vm.hvs), false);
    rows = hv.findAll(".x-table-channels > tbody > tr");
    expect(rows.length).to.be.equal(6);

    // Set wrong filter
    const input = wrapper.find('.x-hv-filters input');
    input.element.value = 'wrongFilter';
    input.trigger('input');
    await waitForValue(wrapper, () => wrapper.vm.loading, false);
    rows = hv.findAll(".x-table-channels > tbody > tr");
    expect(rows.length).to.be.equal(0);
  });
});
