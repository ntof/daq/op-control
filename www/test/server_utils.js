
const
  q = require('q'),

  Server = require('../../src/Server');

/**
 * @param  {any} env
 */
function createApp(env) {
  var def = q.defer();

  env.server = new Server({
    port: 0, basePath: '',
    db: {
      client: 'sqlite3', useNullAsDefault: true,
      connection: { filename: ':memory:' }
    }
  });

  env.server.listen(() => def.resolve());
  return def.promise;
}

module.exports = { createApp };
