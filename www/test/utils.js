// @ts-check

import { createLocalVue as tuCreateLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { defaultTo, delay, get, has, toString } from 'lodash';
import q from 'q';
import { expect } from 'chai';

import BaseVue from "@cern/base-vue";
import VueUtils from '../src/VueUtils';
import Vuex from 'vuex';

export function waitFor(
  wrapper /*: Vue$Wrapper|Vue$Component */,
  cb /*: () => boolean|any */,
  timeout /*: ?number */) /*: Promise<any> */ {

  // $FlowIgnore
  var vm/*: Vue$Component */ = (has(wrapper, 'vm')) ? wrapper.vm : wrapper;

  var def = q.defer();
  var err = new Error('timeout'); /* create this early to have stacktrace */
  var timer = delay(def.reject.bind(def, err),
    defaultTo(timeout, 1000));

  function next() {
    /* $FlowIgnore */
    vm.$nextTick(() => {
      if (!def.promise.isPending()) {
        return;
      }
      try {
        var ret = cb(); /* eslint-disable-line callback-return */
        if (ret) {
          clearTimeout(timer);
          def.resolve(ret);
        }
        else {
          delay(next, 200);
        }
      }
      catch (e) {
        clearTimeout(timer);
        def.reject(has(e, 'message') ? e : new Error(e));
      }
    });
  }

  delay(next, 200);
  return def.promise;
}

export function /*:: <T: Vue$Wrapper|Vue$WrapperArray> */ waitForWrapper(
  vm /*: Vue$Wrapper|Vue$Component */,
  test /*: () => T */,
  timeout /*: ?number */) /*: Promise<T> */ {

  return waitFor(vm, () => {
    const wrapper = test();
    return wrapper.exists() ? wrapper : false;
  }, timeout);
}

function throws(fun) {
  try {
    fun();
    return null;
  }
  catch (e) {
    return e.message;
  }
}

export function waitForValue(
  vm /*: Vue$Wrapper|Vue$Component */,
  test /*: () => bool|any */,
  value /*: any */,
  timeout /*: ?number */) /*: Promise<any> */ {

  var _val;
  return waitFor(vm, () => {
    _val = test();
    return throws(() => expect(_val).to.deep.equal(value)) === null;
  }, timeout)
  .catch((err) => {
    const msg = throws(expect(_val).to.deep.equal(value)) ||
      ("Invalid result value: " + toString(_val) + " != " + value);
    if (err instanceof Error) {
      err.message = msg;
      throw err;
    }
    throw new Error(msg);
  });
}

export function isVisible(el /*: {...} */) /*: boolean */ {
  if (has(el, 'element')) {
    el = get(el, 'element'); /* vue wrapper */
  }
  else if (has(el, '$el')) {
    el = get(el, '$el'); /* vue component */
  }
  while (el) {
    if (el.hidden ||
      (el.style &&
       (el.style.visibility === 'hidden' ||
        el.style.display === 'none'))) {
      return false;
    }
    el = get(el, 'parentElement');
  }
  return true;
}

export const TransitionStub = {
  template: `<div :is="tag"><slot></slot></div>`,
  props: { tag: { type: String, default: 'div' } }
};

export const stubs = {
  'transition-group': TransitionStub,
  'transition': TransitionStub
};

export function createLocalVue() /*: () => any */{
  const local = tuCreateLocalVue();
  local.use(VueRouter);
  local.use(BaseVue);
  local.use(VueUtils);
  local.use(Vuex);
  return local;
}
