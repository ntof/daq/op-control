// @ts-check

import './karma_index';
import { isVisible, waitFor, waitForValue, waitForWrapper } from './utils';
import { get, set } from 'lodash';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Control from '../src/components/Control/ControlLayout.vue';

// import sources, code may be elided otherwise
import { default as store } from '../src/store';
import { State } from '../src/interfaces/eacs';

/*::
declare var serverRequire: (string) => any
*/

describe('Control', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.stub = new NTOFStub({
          daqs: [ { crateId: 12, layout: [ 1 ] } ]
        });
        return this.env.stub.init();
      })
      .then(() => {
        this.env.makeParam = function makeParam(map, name, value) {
          return { index: map[name].index, type: map[name].type, value };
        };
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(async function() {
    await server.run(function() {
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
    store.commit('queryChange', { dns: null });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('displays RunInfo', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Control, { });

    const runInfo = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'RunInfo' }));

    await waitForValue(runInfo, () => get(runInfo, 'vm.$refs.title.value'), 'myExperiment');
    await waitForValue(runInfo, () => get(runInfo, 'vm.$refs.experiment.value'), 'test');

    /* make some direct service changes */
    await server.run(function() {
      const { EACSStub } = serverRequire('@ntof/ntof-stubs');
      const Params = EACSStub.EACSRunInfo.Params;

      this.env.stub.eacs.runInfo.update([
        this.env.makeParam(Params, 'runNumber', 42),
        this.env.makeParam(Params, 'title', 'new title'),
        this.env.makeParam(Params, 'experiment', 'trash')
      ]);
    });

    await waitForValue(runInfo, () => get(runInfo, 'vm.$refs.title.value'), 'new title');
    await waitForValue(runInfo, () => get(runInfo, 'vm.$refs.experiment.value'), 'trash');
    await waitForValue(runInfo, () => get(runInfo, 'vm.$refs.runNumber.value'), 42);
  });

  it('displays RunConfig when needed', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Control, { });

    const runInfo = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'RunInfo' }));

    const runConfig = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'RunConfig' }));

    await waitForValue(runConfig, () => store.state.eacs.value, State.IDLE);

    /* stub is idle, live value is not displayed */
    await waitForValue(runInfo, () => isVisible(runInfo.vm.$refs.title), false);
    await waitForValue(runConfig, () => isVisible(runConfig), true);

    await server.run(function() {
      const { EACSStub } = serverRequire('@ntof/ntof-stubs');
      this.env.stub.eacs.state.setState(EACSStub.EACSState.State.RUNNING);
    });

    await waitForValue(runInfo, () => isVisible(runInfo.vm.$refs.title), true);
    await waitForValue(runConfig, () => isVisible(runConfig), false);

    runInfo.find('.fa-cog').trigger('click');
    await waitForValue(runInfo, () => isVisible(runInfo.vm.$refs.title), true);
    await waitForValue(runConfig, () => isVisible(runConfig), true);
  });

  it('can edit RunConfig', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Control, { });

    const runInfo = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'RunInfo' }));

    const runConfig = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'RunConfig' }));

    await waitForValue(runConfig, () => store.state.eacs.value, State.IDLE);
    await server.run(function() {
      const { EACSStub } = serverRequire('@ntof/ntof-stubs');
      this.env.stub.eacs.state.setState(EACSStub.EACSState.State.RUNNING);
    });

    /* ensure RUNNING state is taken in account */
    await waitForValue(runInfo, () => isVisible(runInfo.vm.$refs.title), true);

    runInfo.find('.fa-cog').trigger('click');
    await waitForValue(runConfig, () => runConfig.vm.inEdit, true);

    set(runConfig, 'vm.$refs.title.editValue', 'another title');
    set(runConfig, 'vm.$refs.description.editValue', 'this is another desc');
    set(runConfig, 'vm.$refs.experiment.editValue', 'my awesome experiment');
    set(runConfig, 'vm.$refs.detectorsSetupId.editValue', '42');
    set(runConfig, 'vm.$refs.materialsSetupId.editValue', '44');

    set(runInfo, 'vm.$refs.title.editValue', 'current title');
    set(runInfo, 'vm.$refs.description.editValue', 'current desc');

    /* sending ctrl-s */
    runInfo.trigger('keydown', { key: 's', ctrlKey: true });
    await waitForValue(runConfig, () => runConfig.vm.inEdit, false);
    await waitForValue(runConfig, () => get(runConfig, 'vm.$refs.title.value'), 'another title');
    await waitForValue(runConfig, () => get(runConfig, 'vm.$refs.experiment.value'), 'my awesome experiment');
    await waitForValue(runConfig, () => get(runConfig, 'vm.$refs.description.value'), 'this is another desc');
    await waitForValue(runConfig, () => get(runConfig, 'vm.$refs.detectorsSetupId.value'), 42);
    await waitForValue(runConfig, () => get(runConfig, 'vm.$refs.materialsSetupId.value'), 44);

    await waitForValue(runInfo, () => get(runInfo, 'vm.$refs.title.value'), 'current title');
    await waitForValue(runInfo, () => get(runInfo, 'vm.$refs.description.value'), 'current desc');
  });

  it('displays setup information', async function() {
    wrapper = mount(Control, { });

    await server.run(function() {
      return this.env.server.db.createStub();
    });

    /* modifying the dns should awake the DBSource */
    store.commit('queryChange', { dns: env.dns });
    const runConfig = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'RunConfig' }));

    await waitFor(runConfig,
      () => (get(runConfig, 'vm.$refs.materialsSetupId.options.length', 0) > 1));
    await waitFor(runConfig,
      () => (get(runConfig, 'vm.$refs.detectorsSetupId.options.length', 0) > 1));
  });
});
