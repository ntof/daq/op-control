// @ts-check

import './karma_index';
import { waitForValue, waitForWrapper } from './utils';
import { get, size } from 'lodash';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Materials from '../src/components/Materials/MaterialsLayout.vue';

// import sources, code may be elided otherwise
import { default as store } from '../src/store';
import { RunInfoMap } from '../src/interfaces/eacs';

/*::
declare var serverRequire: (string) => any
*/

describe('Materials', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.stub = new NTOFStub({
          daqs: [ { crateId: 12, layout: [ 1 ] } ]
        });
        return this.env.stub.init();
      })
      .then(() => {
        this.env.makeParam = function makeParam(map, name, value) {
          return { index: map[name].index, type: map[name].type, value };
        };
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(async function() {
    await server.run(function() {
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
    store.commit('queryChange', { dns: null });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('displays Materials Setup Card', async function() {
    await server.run(function() {
      return this.env.server.db.createStub();
    });

    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Materials, { });

    /* make some direct service changes */
    await server.run(RunInfoMap, function(RunInfoMap) {

      this.env.stub.eacs.runInfo.update([
        { type: RunInfoMap.materialsSetupId.type, value: 681,
          index: RunInfoMap.materialsSetupId.index }
      ]);
      this.env.stub.eacs.runConfig.update([
        { type: RunInfoMap.materialsSetupId.type, value: 703,
          index: RunInfoMap.materialsSetupId.index }
      ]);
    });

    const comp = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'MaterialSetupCard' }));

    var info = await waitForWrapper(comp,
      () => comp.findComponent({ ref: 'current' }));
    await waitForValue(info, () => get(info.vm, 'materialsSetup.name'),
      '2018 EAR2 Samples: Am,Am Filters: Bi,Pb,Pb');
    await waitForValue(info, () => size(get(info.vm, 'materials.filters')), 3);

    wrapper.findAll('.fa-cog').trigger('click');
    info = await waitForWrapper(comp,
      () => comp.findComponent({ ref: 'next' }));
    await waitForValue(info, () => get(info.vm, 'materialsSetup.name'),
      '2018 EAR1 Samples: Cl -- 2018-06-18T12:32:13.000Z');
    await waitForValue(info, () => size(get(info.vm, 'materials.samples')), 1);

    wrapper.findAll('.fa-times').trigger('click');
    await waitForValue(comp,
      () => comp.findComponent({ ref: 'next' }).exists(), false);
  });

  it('displays Detectors Setup Card', async function() {
    await server.run(function() {
      return this.env.server.db.createStub();
    });

    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Materials, { });

    /* make some direct service changes */
    await server.run(RunInfoMap, function(RunInfoMap) {

      this.env.stub.eacs.runInfo.update([
        { type: RunInfoMap.detectorsSetupId.type, value: 22,
          index: RunInfoMap.detectorsSetupId.index }
      ]);
      this.env.stub.eacs.runConfig.update([
        { type: RunInfoMap.detectorsSetupId.type, value: 23,
          index: RunInfoMap.detectorsSetupId.index }
      ]);
    });

    const comp = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'DetectorSetupCard' }));

    var info = await waitForWrapper(comp,
      () => comp.findComponent({ ref: 'current' }));
    await waitForValue(info, () => get(info.vm, 'info.name'),
      '2018 EAR2 -- Unknown');
    await waitForValue(info, () => size(get(info.vm, 'detectorsSetup')), 1);

    wrapper.findAll('.fa-cog').trigger('click');
    info = await waitForWrapper(comp,
      () => comp.findComponent({ ref: 'next' }));
    await waitForValue(info, () => get(info.vm, 'info.name'),
      '2018 EAR1 -- Unknown');
    await waitForValue(info, () => size(get(info.vm, 'detectorsSetup')), 0);

    wrapper.findAll('.fa-times').trigger('click');

    await waitForValue(comp,
      () => comp.findComponent({ ref: 'next' }).exists(), false);
  });
});
