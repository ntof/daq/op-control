// @ts-check

import './karma_index';
import { waitForValue, waitForWrapper } from './utils';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import _ from 'lodash';

import Alarms from '../src/components/Alarms/Alarms.vue';
import store from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Alarms', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.stub = new NTOFStub();
        return this.env.stub.init();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      // this.env.eacs.close();
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can see Alarms', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Alarms, { });

    const alarms = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'Alarms' }));

    // Initialize filters with all alarms excluded
    await alarms.setData({ searchSeverity: [] });
    await alarms.vm.updateWorker();
    // Wait for result to be empty
    await waitForValue(alarms, () => _.isEmpty(alarms.vm.alarms), true);

    let rows = alarms.findAll("table > tbody > tr");
    expect(rows.length).to.be.eq(1); // tr with colpsan saying there are no alarms

    // Click on Critical, Error and Warning filter button
    alarms.find('#f-critical').trigger('click');
    alarms.find('#f-error').trigger('click');
    alarms.find('#f-warning').trigger('click');
    // Check alarms is now not empty
    await waitForValue(alarms, () => _.isEmpty(alarms.vm.alarms), false);

    rows = alarms.findAll("table > tbody > tr");
    expect(rows.length).to.be.greaterThan(1);
  });
});
