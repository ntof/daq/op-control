// @ts-check

import './karma_index';
import { waitFor, waitForValue, waitForWrapper } from './utils';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Control from '../src/components/Control/ControlLayout.vue';
import store from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Control', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { EACSStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.eacs = new EACSStub([], this.env.dns.url());
        this.env.eacs.register("EACS", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        // this.env.eacs.run();
        this.env.makeParam = function makeParam(map, name, value) {
          return { index: map[name].index, type: map[name].type, value };
        };
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      // this.env.eacs.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('displays and edits Comments', async function() {
    wrapper = mount(Control, { });

    await server.run(function() {
      return this.env.server.db.createStub();
    });

    /* modifying the dns should awake the DBSource (by first fetching runInfo) */
    store.commit('queryChange', { dns: env.dns });
    const comments = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'Comments' }));
    await waitForValue(comments, () => comments.vm.isComments, true);
    await waitFor(comments, () => comments.vm.commentsCount > 0);

    const count = comments.vm.commentsCount;

    comments.find('.fa-cog').trigger('click');
    await waitForValue(comments, () => comments.vm.inEdit, true);

    comments.vm.$refs['comment'].editValue = 'this is a comment';
    comments.trigger('keydown', { key: 's', ctrlKey: true });

    await waitForValue(comments, () => comments.vm.commentsCount, count + 1);
  });
});
