// @ts-check

import './karma_index';
import { waitForValue, waitForWrapper } from './utils';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Control from '../src/components/Control/ControlLayout.vue';
import store from '../src/store';
import { State } from '../src/interfaces/eacs';

/*::
declare var serverRequire: (string) => any
*/

describe('Control', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.stub = new NTOFStub({
          daqs: [
            { crateId: 12, layout: [ 4, 2 ] },
            { crateId: 11, layout: [ 1 ] }
          ]
        });
        return this.env.stub.init();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      // this.env.eacs.close();
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can Control EACS', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Control, { });

    const ctrl = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'Control' }));

    // jump to edit mode
    (await waitForWrapper(wrapper, () => ctrl.find('.fa-cog')))
    .trigger('click');

    await waitForValue(ctrl, () => ctrl.vm.state, State.IDLE);
    let button = await waitForWrapper(ctrl,
      () => ctrl.findAll('button:not(disabled)')
      .filter((b) => b.text() === 'Start'));
    button.trigger('click');
    await waitForValue(ctrl, () => ctrl.vm.state, State.RUNNING, 2000);

    button = await waitForWrapper(ctrl,
      () => ctrl.findAll('button:not(disabled)')
      .filter((b) => b.text() === 'Stop'));
    button.trigger('click');

    const stopDialog = await waitForWrapper(wrapper,
      () => ctrl.findComponent({ ref: 'dataStatusDialog' }));

    button = await waitForWrapper(stopDialog,
      () => stopDialog.findAll('button:not(disabled)')
      .filter((b) => b.text() === 'Stop Run'));
    button.trigger('click');

    await waitForValue(ctrl, () => ctrl.vm.state, State.IDLE);
  });
});
