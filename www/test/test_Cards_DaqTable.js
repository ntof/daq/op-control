// @ts-check

import './karma_index';


import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';
import { waitFor, waitForValue } from './utils';

import Cards from '../src/components/Cards/CardsLayout.vue';
import { waitDaqInit } from './test_Cards';

// jshint unused:false
import { default as store } from '../src/store';


/**
 * @typedef {typeof import('../src/components/Cards/DaqTable.vue').default} DaqTable
 * @typedef {typeof import('../src/components/Cards/DaqTableChannel.vue').default} DaqTableChannel
 * @typedef {typeof import('@cern/base-vue').BaseInput} BaseInput
 */

describe('Cards DaqTable', function() {
  /** @type {Tests.Wrapper<any>} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.stub = new NTOFStub({
          daqs: [
            { crateId: 12, layout: [ 4, 2 ] },
            { crateId: 11, layout: [ 1 ] }
          ]
        });
        return this.env.stub.init();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(async function() {
    await server.run(function() {
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
    store.commit('queryChange', { dns: null });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore: simplifies tests */
      wrapper = null;
    }
  });

  it('can disable ZSP', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Cards, { });

    const { daq } = await waitDaqInit(wrapper);
    await waitForValue(daq, () => daq.vm.isLoading, false);

    /** @type {Tests.Wrapper<V.Instance<DaqTable>>} */
    const table = wrapper.findComponent({ name: 'DaqTable' });
    expect(table.exists()).to.be.true();
    table.find('.fa-cog').trigger('click');
    await waitForValue(table, () => table.vm.inEdit, true);

    const channel =
      /** @type {Tests.Wrapper<V.Instance<DaqTableChannel>>} */ (wrapper.findComponent({ name: 'DaqTableChannel' }));
    expect(channel.exists()).to.be.true();
    expect(channel.vm.isZSP).to.be.true();

    const zsStart = /** @type {Tests.Wrapper<V.Instance<BaseInput>>} */
      (channel.findComponent({ ref: 'zsStart' }));
    expect(zsStart.exists()).to.be.true();
    await waitFor(zsStart, () => zsStart.vm.editValue === '0');

    channel.findComponent({ ref: 'isZSP' })
    .findAll('input').trigger('click');
    await waitFor(zsStart, () => zsStart.vm.editValue !== '0');

    channel.findComponent({ ref: 'isZSP' })
    .findAll('input').trigger('click');
    await waitFor(zsStart, () => zsStart.vm.editValue === '0');

    channel.findComponent({ ref: 'isZSP' })
    .findAll('input').trigger('click');
    await waitFor(zsStart, () => zsStart.vm.editValue !== '0');

    table.findAll('button')
    .filter((b) => b.text() === 'Configure')
    .trigger('click');

    await waitForValue(channel, () => channel.vm.isZSP, false);
  });
});
