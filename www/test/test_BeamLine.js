// @ts-check

import './karma_index';
import { waitForValue, waitForWrapper } from './utils';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import BeamLine from '../src/components/BeamLine/BeamLineLayout.vue';
import store from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('BeamLine', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.stub = new NTOFStub();
        return this.env.stub.init();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      // this.env.eacs.close();
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can see BeamLine', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(BeamLine, { });

    const vgr = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'VGRVacuumPressure' }));
    const vpp = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'VPPVacuumPumpState' }));
    const vvs = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'VVSValvePositions' }));

    await waitForValue(wrapper, () => vgr.findAll("table > tbody > tr").length, 2);
    await waitForValue(wrapper, () => vpp.findAll("table > tbody > tr").length, 2);
    await waitForValue(wrapper, () => vvs.findAll("table > tbody > tr").length, 2);
  });
});
