// @ts-check

import './karma_index';
import { waitForValue, waitForWrapper } from './utils';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Materials from '../src/components/Materials/MaterialsLayout.vue';

// import sources, code may be elided otherwise
import { default as store } from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Materials', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.stub = new NTOFStub({
          daqs: [ { crateId: 12, layout: [ 1 ] } ]
        });
        return this.env.stub.init();
      })
      .then(() => {
        this.env.makeParam = function makeParam(map, name, value) {
          return { index: map[name].index, type: map[name].type, value };
        };
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(async function() {
    await server.run(function() {
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
    store.commit('queryChange', { dns: null });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('displays FilterStation state', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Materials, { });

    const comp = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'FilterStation' }));

    await waitForValue(comp, () => comp.findAll('.x-current').at(0).text(), 'IN');

    /* make some direct service changes */
    await server.run(function() {
      const { XmlData } = serverRequire('@ntof/dim-xml');
      this.env.stub.filterStation.update([
        { type: XmlData.Type.ENUM, value: 4, index: 2 } // First filter in IN_INTERLOCKED
      ]);
    });
    await waitForValue(comp, () => comp.findAll('.x-current').at(0).text(), 'IN_INTERLOCKED');
  });

});
