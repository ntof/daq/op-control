// @ts-check

import './karma_index';

import { defaultTo, delay, get, has } from 'lodash';
import { expect } from 'chai';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { DaqZspStore } from '../src/store/modules/daq';
import { ZSPMode } from '../src/interfaces/daq';
import { ParamType } from '../src/interfaces';
import Vuex from 'vuex';

/*::
import { type DaqConfig$ZSPStoreState } from '../src/store/modules/daq'
*/

function genId(chan /*: { sn: string, channel: number, id?: string } */) {
  chan.id = chan.sn + ':' + chan.channel;
  return chan;
}

function storeWait(store, path /*: string | Array<string> */, test /*: function */, timeout /*: ?number */) {
  var prom = new Promise((resolve, reject) => {
    var storeValue = get(store.state, path);
    const ret = test(storeValue);
    if (ret) {
      resolve(ret);
    }
    else {
      var timer = delay(
        () => {
          reject(new Error('timeout, last value retrieved: ' + JSON.stringify(storeValue)));
        },
        defaultTo(timeout, 1000));

      const watch = store.watch(
        (state) => get(state, path),
        (ret) => {
          storeValue = ret;
          if (test(ret)) {
            watch();
            clearTimeout(timer);
            resolve(ret);
          }
        });
    }
  });

  return prom;
}

describe('DaqZspStore', function() {
  var store/*: Vuex.Store<DaqConfig$ZSPStoreState> */;
  var watch;

  beforeEach(function() {
    // $FlowIgnore
    store = new Vuex.Store/*:: <DaqConfig$ZSPStoreState> */(DaqZspStore);
  });

  afterEach(function() {
    if (watch) {
      watch();
      watch = null;
    }
    // $FlowIgnore
    store = null;
  });

  it('can move channels', async function() {
    const channels = {
      '1234:0': genId({ sn: '1234', channel: 0 }),
      '1234:1': genId({ sn: '1234', channel: 1, slave: [ '1234:2', '1234:3' ] }),
      '1234:2': genId({ sn: '1234', channel: 2 }),
      '1234:3': genId({ sn: '1234', channel: 3 }),
      '1234:4': genId({ sn: '1234', channel: 4 })
    };

    store.commit('update', { mode: ZSPMode.MASTER, channels });
    store.commit('resetEdit');

    await storeWait(store, [ 'edit', 'channels', '1234:2', 'masterId' ],
      (masterId) => masterId === '1234:1');
    var prom;

    /* do this first to ensure notifications are properly sent */
    prom = storeWait(store, [ 'edit', 'channels', '1234:2', 'masterId' ],
      (masterId) => masterId === '1234:0');
    store.commit('moveChannel', { id: '1234:2', masterId: '1234:0' });
    await prom;

    prom = storeWait(store, [ 'edit', 'independent' ], (indep) => has(indep, '1234:3'));
    store.commit('moveChannel', { id: '1234:3' });
    await prom;

    expect(get(store.state, 'edit.channels')).to.deep.equal({
      '1234:0': genId({ sn: '1234', channel: 0, slave: [ '1234:2' ] }),
      '1234:1': genId({ sn: '1234', channel: 1 }),
      '1234:2': genId({ sn: '1234', channel: 2, masterId: '1234:0' }),
      '1234:3': genId({ sn: '1234', channel: 3 }),
      '1234:4': genId({ sn: '1234', channel: 4 })
    });
  });

  it('can monitor independent', async function() {
    const channels = {
      '1234:1': genId({ sn: '1234', channel: 1, slave: [ '1234:2' ] }),
      '1234:2': genId({ sn: '1234', channel: 2 })
    };

    store.commit('update', { mode: ZSPMode.MASTER, channels });
    store.commit('resetEdit');

    await storeWait(store, [ 'edit', 'channels', '1234:2', 'masterId' ],
      (masterId) => masterId === '1234:1');
    var prom;

    expect(get(store.state, 'edit.independent')).to.be.empty();

    /* do this first to ensure notifications are properly sent */
    prom = storeWait(store, [ 'edit', 'independent' ], (indep) => has(indep, '1234:2'));
    store.commit('moveChannel', { id: '1234:2' });
    await prom;

    expect(get(store.state, 'edit.independent')).to.not.be.empty();
  });

  it('can convert to dataset', async function() {
    const channels = {
      '1234:1': genId({ sn: '1234', channel: 1, slave: [ '1234:2' ] }),
      '1234:2': genId({ sn: '1234', channel: 2 })
    };

    store.commit('update', { mode: ZSPMode.MASTER, channels });
    store.commit('resetEdit');

    await storeWait(store, [ 'edit', 'channels', '1234:2', 'masterId' ], (masterId) => masterId === '1234:1');

    expect(store.getters.getDataSetChanges).to.deep.equal([
      { index: 1, type: ParamType.ENUM, value: ZSPMode.MASTER },
      { index: 2, value: [
        { index: 0, value: [
          { index: 0, type: ParamType.STRING, value: '1234' },
          { index: 1, type: ParamType.UINT32, value: 1 },
          { index: 2, value: [
            { index: 0, type: ParamType.STRING, value: '1234' },
            { index: 1, type: ParamType.UINT32, value: 2 }
          ] }
        ] }
      ] }
    ]);
  });
});
