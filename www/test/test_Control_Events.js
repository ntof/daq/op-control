// @ts-check

import './karma_index';
import { waitForValue, waitForWrapper } from './utils';

import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Control from '../src/components/Control/ControlLayout.vue';
import store from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Control', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { EACSStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.eacs = new EACSStub([], this.env.dns.url());
        this.env.eacs.register("EACS", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        // this.env.eacs.run();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      // this.env.eacs.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('displays Events', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Control, { });

    const events = await waitForWrapper(wrapper, () => wrapper.findComponent({ name: 'Events' }));

    /* make some direct service changes */
    await server.run(function() {
      const { now } = serverRequire('lodash');
      for (var i = 0; i < 20; ++i) {
        this.env.eacs.events.addEvent({
          eventNumber: i, dest: '', dest2: '', periodNB: -1, user: '',
          name: 'CALIBRATION',
          timestamp: now() * 1000000,
          cyclestamp: now() * 1000000
        });
      }
    });

    await waitForValue(events,
      () => events.find('tbody tr td:nth-child(2)').text(), '- / 19');
    expect(events.findAll('tbody tr').length).to.equal(10);
  });
});
