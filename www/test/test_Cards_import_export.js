// @ts-check

import './karma_index';
import { waitForValue, waitForWrapper } from './utils';

import { get } from 'lodash';
import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';
import sinon from 'sinon';

import Cards from '../src/components/Cards/CardsLayout.vue';
// jshint unused:false
import { default as store } from '../src/store';

import { waitDaqInit } from './test_Cards';

/*::
declare var serverRequire: (string) => any
*/

describe('Cards', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.stub = new NTOFStub({
          daqs: [
            { crateId: 12, layout: [ 4, 2 ] },
            { crateId: 11, layout: [ 1 ] }
          ]
        });
        return this.env.stub.init();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(async function() {
    await server.run(function() {
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
    store.commit('queryChange', { dns: null });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  async function editCard(daq, card) {
    // Change
    daq.find('.fa-cog').trigger('click');
    await waitForValue(daq, () => daq.vm.inEdit, true);

    card.findComponent({ ref: 'delay' }).find('input').setValue('50');
    card.findComponent({ ref: 'timeWindow' }).find('input').setValue('100');

    const channel = card.findComponent({ name: 'Channel' });
    expect(channel.exists()).to.be.true();

    channel.findComponent({ ref: 'detectorType' }).find('input').setValue('PWET');
    channel.findComponent({ ref: 'detectorId' }).find('input').setValue(12);

    await wrapper.vm.$nextTick(); // edit signals takes a tick
    daq.findAll('button')
    .filter((b) => b.text() === 'Configure')
    .trigger('click');

    await waitForValue(daq, () => daq.vm.inEdit, false);
    await waitForValue(card,
      () => get(card.findComponent({ ref: 'delay' }).find('input'), 'element.value'), '50');
  }

  // Import / Export
  it('can export and import Daqs JSon configuration', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Cards, { });

    const { card, daq } = await waitDaqInit(wrapper);
    const overview = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'DaqTable' }));
    const daqConfTools = await waitForWrapper(wrapper,
      () => overview.findComponent({ name: 'ImportExportDropDownTools' }));

    // Save Configuration (all deactivated)
    const exportJsonBefore = daqConfTools.vm.generateJSON();

    await editCard(daq, card);

    const exportJsonAfter = daqConfTools.vm.generateJSON();
    expect(exportJsonBefore).is.not.deep.equal(exportJsonAfter);

    // Import and apply Exported config
    daqConfTools.vm.importJSON(JSON.stringify(exportJsonBefore));
    await daqConfTools.vm.applyConf();

    await wrapper.vm.$nextTick(); // edit signals takes a tick
    await wrapper.vm.$nextTick(); // edit signals takes a tick

    await waitForValue(card,
      () => get(card.findComponent({ ref: 'delay' }).find('input'), 'element.value'), '0');

    // Export again and check that is now the same as in the beginning
    const exportJsonAfterImport = daqConfTools.vm.generateJSON();
    expect(exportJsonBefore).is.deep.equal(exportJsonAfterImport);
  });

  it('can export and import Daqs CSV configuration', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Cards, { });

    const { card, daq } = await waitDaqInit(wrapper);
    const overview = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'DaqTable' }));
    const daqConfTools = await waitForWrapper(wrapper,
      () => overview.findComponent({ name: 'ImportExportDropDownTools' }));

    const fileStub = sinon.stub(daqConfTools.vm, 'downloadCsv');
    try {
      // Save Configuration (all deactivated)
      daqConfTools.vm.exportCSV();
      expect(fileStub.callCount).to.be.equal(1);

      await editCard(daq, card);

      daqConfTools.vm.exportCSV();
      expect(fileStub.callCount).to.be.equal(2);
      expect(fileStub.getCall(0).args[0])
      .is.not.deep.equal(fileStub.getCall(1).args[0]);

      // Import and apply Exported config
      daqConfTools.vm.importCSV(fileStub.getCall(0).args[0]);
      await daqConfTools.vm.applyConf();

      await waitForValue(card,
        () => get(card.findComponent({ ref: 'delay' }).find('input'), 'element.value'), '0');

      // Export again and check that is now the same as in the beginning
      daqConfTools.vm.exportCSV();
      expect(fileStub.callCount).to.be.equal(3);
      expect(fileStub.getCall(0).args[0])
      .is.deep.equal(fileStub.getCall(2).args[0]);
    }
    finally {
      fileStub.restore();
    }
  });

});
