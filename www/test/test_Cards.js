// @ts-check

import './karma_index';
import { waitFor, waitForValue, waitForWrapper } from './utils';

import { get } from 'lodash';
import * as utilities from "../src/utilities";
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import Cards from '../src/components/Cards/CardsLayout.vue';
// jshint unused:false
import { default as store, sources } from '../src/store';

/**
 * @typedef {typeof import('../src/components/Cards/Daq.vue').default} Daq
 * @typedef {typeof import('../src/components/Cards/DaqCard.vue').default} DaqCard
 */

/**
 * @param  {Tests.Wrapper} wrapper
 * @return {Promise<{
 *  daqs: Tests.WrapperArray<V.Instance<Daq>>,
 *  daq: Tests.Wrapper<V.Instance<Daq>>,
 *  card: Tests.Wrapper<V.Instance<DaqCard>>
 * }>}
 */
export async function waitDaqInit(wrapper) {
  const daqs = /** @type {Tests.WrapperArray<V.Instance<Daq>>} */ (
    await waitForWrapper(wrapper,
      () => wrapper.findAllComponents({ name: 'Daq' })));
  expect(daqs.length).to.equal(2);

  const daq = daqs.at(0);
  await waitForValue(daq, () => daq.vm.isLoading, false);

  const card = /** @type {Tests.Wrapper<V.Instance<DaqCard>>} */ (
    wrapper.findComponent({ name: 'DaqCard' }));
  expect(card.exists()).to.be.true();

  wrapper.findAllComponents({ name: 'BaseCollapsible' })
  .setData({ isExpanded: true }); /* expand everything */
  await waitForWrapper(wrapper, () => card.findComponent({ ref: 'sampleRate' }));

  return { daqs, daq, card };
}


describe('Cards', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.stub = new NTOFStub({
          daqs: [
            { crateId: 12, layout: [ 4, 2 ] },
            { crateId: 11, layout: [ 1 ] }
          ]
        });
        return this.env.stub.init();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(async function() {
    await server.run(function() {
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
    store.commit('queryChange', { dns: null });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can display Daqs', async function() {
    store.commit('queryChange', { dns: null });
    wrapper = mount(Cards, { });

    /* wait for dns=null to be taken in account */
    await waitFor(wrapper, () => !sources.daqList?.daqList);
    store.commit('queryChange', { dns: env.dns });

    const daqs = await waitForWrapper(wrapper,
      () => wrapper.findAllComponents({ name: 'Daq' }));
    expect(daqs.length).to.equal(2);

    const daq = daqs.at(0);
    await waitForValue(daq, () => daq.vm.isLoading, false);

    const cards = daq.findAllComponents({ name: 'DaqCard' });
    expect(cards.length).to.equal(2);

    const card = cards.at(0);
    expect(card.findAllComponents({ name: 'Channel' }).length).to.equal(4);
  });

  it('can configure Daqs', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Cards, { });

    const { card, daq } = await waitDaqInit(wrapper);

    daq.find('.fa-cog').trigger('click');
    await waitForValue(daq, () => daq.vm.inEdit, true);

    card.findComponent({ ref: 'sampleRate' })
    .findAll('select > option').at(0).setSelected(); // 14.0625
    card.findComponent({ ref: 'delay' }).find('input').setValue('50');
    card.findComponent({ ref: 'timeWindow' }).find('input').setValue('100');

    const channel = card.findComponent({ name: 'Channel' });
    expect(channel.exists()).to.be.true();

    channel.findComponent({ ref: 'detectorType' }).find('input').setValue('PWET');
    channel.findComponent({ ref: 'detectorId' }).find('input').setValue(12);
    channel.findComponent({ ref: 'thresholdSign' }).find('select').setValue(0);

    await wrapper.vm.$nextTick(); /* edit signals takes a tick */
    daq.findAll('button')
    .filter((b) => b.text() === 'Configure')
    .trigger('click');
    await waitForValue(daq, () => daq.vm.inEdit, false);
    await waitForValue(card,
      () => get(card.findComponent({ ref: 'sampleRate' }).find('input'), 'element.value'),
      '14.0625');

    expect(card.findComponent({ ref: 'delay' }).find('input'))
    .to.have.nested.property('element.value', '50');

    expect(channel.vm.inEdit).to.equal(false);
    expect(channel.findComponent({ ref: 'detectorType' }).find('input'))
    .to.have.nested.property('element.value', 'PWET');
    expect(channel.findComponent({ ref: 'detectorId' }).find('input'))
    .to.have.nested.property('element.value', '12');
    expect(channel.findComponent({ ref: 'thresholdSign' }).find('input'))
    .to.have.nested.property('element.value', '-');
  });

  it('updates sampleSize if timeWindow is modified', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Cards, { });

    const { card, daq } = await waitDaqInit(wrapper);

    expect(card.findComponent({ ref: 'sampleSize' }).find('input'))
    .to.have.nested.property('element.value', '128000');

    /* enter edit mode */
    daq.find('.fa-cog').trigger('click');
    await waitForValue(daq, () => daq.vm.inEdit, true);

    /* 100 ms window */
    card.findComponent({ ref: 'timeWindow' }).find('input').setValue('100');
    await wrapper.vm.$nextTick();
    /* sampleSize is updated when timeWindow is modified */
    expect(card.findComponent({ ref: 'sampleSize' }).find('input'))
    .to.have.nested.property('element.value', '180000');
  });

  it('updates sampleSize if SampleRate is modified', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Cards, { });

    const { card, daq } = await waitDaqInit(wrapper);

    expect(card.findComponent({ ref: 'sampleSize' }).find('input'))
    .to.have.nested.property('element.value', '128000');

    /* enter edit mode */
    daq.find('.fa-cog').trigger('click');
    await waitForValue(daq, () => daq.vm.inEdit, true);

    card.findComponent({ ref: 'sampleRate' }).findAll('select > option')
    .at(0).setSelected();

    await wrapper.vm.$nextTick();
    /* sampleSize is updated when SampleRate is modified */
    expect(card.findComponent({ ref: 'sampleSize' }).find('input'))
    .to.have.nested.property('element.value', '1000');
  });

  it(`displays an alert for Time Window [ms]`, async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Cards, { });

    const { card, daq } = await waitDaqInit(wrapper);

    /* enter edit mode */
    daq.find('.fa-cog').trigger('click');
    await waitForValue(daq, () => daq.vm.inEdit, true);

    const timeWindowInput = wrapper.findAll('.b-param')
    .filter((m) => m.vm.title === 'Time Window [ms]')
    .at(0).find('input');

    expect(card.find('.alert-danger').exists()).to.equal(false);

    timeWindowInput.setValue((175001 / card.vm.sampleRate).toFixed(3));
    await waitForValue(wrapper, () => wrapper.find('.alert-danger').exists(), true);

    timeWindowInput.setValue((175000 / card.vm.sampleRate).toFixed(3));
    await waitForValue(wrapper, () => wrapper.find('.alert-danger').exists(), false);

    timeWindowInput.setValue(0);
    await waitForValue(wrapper, () => wrapper.find('.alert-danger').exists(), true);
  });

  function testError(value, selector, title, isReadOnly = false) {
    it(`displays an alert for "${value}" in "${title}"`, async function() {
      store.commit('queryChange', { dns: env.dns });
      wrapper = mount(Cards, { });

      const { card, daq } = await waitDaqInit(wrapper);

      /* enter edit mode */
      daq.find('.fa-cog').trigger('click');
      await waitForValue(daq, () => daq.vm.inEdit, true);

      const params = card.findAll('.b-param');
      const param = params.filter((m) => m.vm.title === title).at(0);
      if (isReadOnly) { param.vm.$data.editValue = value; }
      else { param.find('input').setValue(value); }
      await waitForValue(wrapper, () => wrapper.find(selector).exists(), true);
    });
  }

  testError(175001, '.alert-danger', 'Sample Size [KS]', true); /* Please select a value that is no more than 175000. */
  testError(0, '.alert-danger', 'Sample Size [KS]', true); /* Please select a value that is no less than 1. */
  testError(25001, '.alert-warning', 'Offset [mV]', true); /* offset should be below 2500 and above -2500 */
});
