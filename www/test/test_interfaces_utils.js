// @ts-check

import './karma_index';

import { expect } from 'chai';
import { genDataSet, parseDataSet } from '../src/interfaces';
import { DaqListDaqMap } from '../src/interfaces/eacs';
import { describe, it } from 'mocha';

describe('interfaces', function() {
  const refData = [
    { type: 3, value: "ntofdaq-m0", index: 0, name: "name" },
    { index: 1, name: "cards", value: [
      { index: 0, name: "card",
        value: [
          { type: 11, value: 2, index: 0, name: "channelsCount" },
          { type: 9, value: false, index: 3, name: "used" }
        ]
      }
    ] },
    { type: 9, value: false, index: 2, name: "used" }
  ];

  it('can parse a DataSet', function() {
    // $FlowIgnore: type is optional
    const ret = parseDataSet(refData, DaqListDaqMap);
    expect(ret).to.deep.equal({
      name: 'ntofdaq-m0',
      cards: { card: [ { index: 0, channelsCount: 2, used: false } ] },
      used: false
    });
  });

  it('can generate a DataSet', function() {
    // $FlowIgnore: type is optional
    const ret = parseDataSet(refData, DaqListDaqMap);

    expect(genDataSet(ret, DaqListDaqMap)).to.deep.equal([
      { type: 3, value: "ntofdaq-m0", index: 0 },
      { index: 1, value: [
        { index: 0,
          value: [
            { type: 11, value: 2, index: 0 },
            { type: 9, value: false, index: 3 }
          ]
        }
      ] },
      { type: 9, value: false, index: 2 }
    ]);
  });
});
